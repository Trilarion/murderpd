/*
 * userchoicedialog.cpp
 *
 * Copyright 2012 - 2013 Chris Ohmstede
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 3
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 *
 */

#include "userchoicedialog.h"
#include "mainwindow.h"

UserChoiceDialog::UserChoiceDialog(const vector<string> &choicesVector,QWidget *parent /*= 0*/,Qt::WindowFlags f /*= 0*/) : QDialog(parent,f)
{
	m_choicesVector = choicesVector;
	setWindowTitle(USER_CHOICE_DIALOG_TITLE);
}

void UserChoiceDialog::init()
{
	m_mainVerticalLayout = new QVBoxLayout();
	m_buttonHorizontalLayout = new QHBoxLayout();

	m_choiceList = new QListWidget();
	m_choiceList->setSelectionMode(QAbstractItemView::SingleSelection);
	for(int i = 0;i < (int) m_choicesVector.size();i++)
		m_choiceList->addItem(m_choicesVector.at(i).c_str());
	m_choiceList->setCurrentRow(0,QItemSelectionModel::SelectCurrent);
	m_choiceList->setAlternatingRowColors(true);

	m_okButton = new QPushButton("OK");
	m_cancelButton = new QPushButton("Cancel");

	m_buttonHorizontalLayout->addStretch();
	m_buttonHorizontalLayout->addWidget(m_okButton);
	m_buttonHorizontalLayout->addWidget(m_cancelButton);

	m_mainVerticalLayout->addWidget(m_choiceList);
	m_mainVerticalLayout->addItem(m_buttonHorizontalLayout);

	setLayout(m_mainVerticalLayout);

	setMinimumWidth(IMAGE_MOVIE_MINIMUM_WIDTH - 20);
	setMinimumHeight(IMAGE_MOVIE_MINIMUM_HEIGHT - 120);

	connect(m_okButton,SIGNAL(clicked()),this,SLOT(accept()));
	connect(m_cancelButton,SIGNAL(clicked()),this,SLOT(reject()));
	connect(this,SIGNAL(rejected()),this,SLOT(userChoiceDialogRejected()));
	connect(this,SIGNAL(accepted()),this,SLOT(userChoiceDialogAccepted()));
	connect(m_choiceList,SIGNAL(itemDoubleClicked(QListWidgetItem*)),this,SLOT(listDoubleClicked(QListWidgetItem*)));
}

void UserChoiceDialog::userChoiceDialogRejected()
{
	setResult(-1);
}

void UserChoiceDialog::userChoiceDialogAccepted()
{
	setResult(m_choiceList->currentRow());
}

void UserChoiceDialog::listDoubleClicked(QListWidgetItem*)
{
	accept();
}
