/*
 * activesprite.h
 *
 * Copyright 2012 - 2013 Chris Ohmstede
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 3
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 *
 */

#ifndef ACTIVESPRITE_H
#define ACTIVESPRITE_H

#include "builddefs.h"
#include <QtGui/QPixmap>

#define ACTIVE_SPRITE_X_DISPLAY_BOOST						48

class ActiveSprite
{
public:
	enum
	{
		ACTIVE_SPRITE_UP,
		ACTIVE_SPRITE_DOWN
	};
	enum
	{
		ACTIVE_SPRITE_LEFT,
		ACTIVE_SPRITE_RIGHT
	};

	ActiveSprite();
	ActiveSprite(const QPixmap &sprite,
				 int spriteVertical,
				 int spriteStartingHorizontal,
				 int spriteEndingHorizontal,
				 int spriteDirection,
				 int backgroundWidth);
	ActiveSprite(const ActiveSprite &as);
	ActiveSprite &operator = (const ActiveSprite &as);

	bool slideSpriteInPainter(QPainter &painter);
private:
	QPixmap m_sprite;
	int m_spriteVertical;
	int m_spriteHorizontal;
	int m_spriteDirection;
	int m_currentOffset;
	int m_finalOffset;
	int m_spriteWidth;
	bool m_completePending;
	int m_backgroundWidth;
	int m_randVertical;
	int m_randHorizontal;
	int m_currentVerticalOffset;
	bool m_slideToRandComplete;
	int m_randVerticalDirection;
	int m_randHorizontalDirection;
	int m_finalRandOffset;
	bool m_verticalSlideToRandComplete;
	bool m_horizontalSlideToRandComplete;
};

#endif // ACTIVESPRITE_H
