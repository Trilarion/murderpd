/*
 * saveloadcontainer.cpp
 *
 * Copyright 2012 - 2013 Chris Ohmstede
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 3
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 *
 */

#include "saveloadcontainer.h"

// class SaveSpriteNode
SaveSpriteNode::SaveSpriteNode() :
		m_verticalPosition(-1),
		m_horizontalPosition(-1)
{
}

SaveSpriteNode::SaveSpriteNode(int verticalPosition,int horizontalPosition,const string &spriteName) :
		m_verticalPosition(verticalPosition),
		m_horizontalPosition(horizontalPosition),
		m_spriteName(spriteName)
{
}

SaveSpriteNode::SaveSpriteNode(const SaveSpriteNode &ssn) :
		m_verticalPosition(ssn.m_verticalPosition),
		m_horizontalPosition(ssn.m_horizontalPosition),
		m_spriteName(ssn.m_spriteName)
{
}

SaveSpriteNode &SaveSpriteNode::operator = (const SaveSpriteNode &ssn)
{
	m_verticalPosition = ssn.m_verticalPosition;
	m_horizontalPosition = ssn.m_horizontalPosition;
	m_spriteName = ssn.m_spriteName;
	return *this;
}

int SaveSpriteNode::getVerticalPosition() const
{
	return m_verticalPosition;
}

int SaveSpriteNode::getHorizontalPosition() const
{
	return m_horizontalPosition;
}

const string &SaveSpriteNode::getSpriteName() const
{
	return m_spriteName;
}





// class SaveNode
SaveNode::SaveNode() :
		m_inUse(false),
		m_time(0),
		m_scenarioOffset(-1),
		m_sceneOffset(-1)
{
}

SaveNode::SaveNode(time_t currentTime,int scenarioOffset,int sceneOffset,const string &backgroundName,const vector<SaveSpriteNode> &saveSpriteNodeVector) :
		m_inUse(true),
		m_time(currentTime),
		m_scenarioOffset(scenarioOffset),
		m_sceneOffset(sceneOffset),
		m_backgroundName(backgroundName)
{
	m_saveSpriteNodeVector = saveSpriteNodeVector;
}

SaveNode::SaveNode(const SaveNode &sn) :
		m_inUse(sn.m_inUse),
		m_time(sn.m_time),
		m_scenarioOffset(sn.m_scenarioOffset),
		m_sceneOffset(sn.m_sceneOffset),
		m_backgroundName(sn.m_backgroundName)
{
	m_saveSpriteNodeVector = sn.m_saveSpriteNodeVector;
}

SaveNode &SaveNode::operator = (const SaveNode &sn)
{
	m_inUse = sn.m_inUse;
	m_time = sn.m_time;
	m_scenarioOffset = sn.m_scenarioOffset;
	m_sceneOffset = sn.m_sceneOffset;
	m_backgroundName = sn.m_backgroundName;
	m_saveSpriteNodeVector = sn.m_saveSpriteNodeVector;
	return *this;
}

bool SaveNode::getInUse() const
{
	return m_inUse;
}

time_t SaveNode::getTime() const
{
	return m_time;
}

int SaveNode::getScenarioOffset() const
{
	return m_scenarioOffset;
}

int SaveNode::getSceneOffset() const
{
	return m_sceneOffset;
}

const string &SaveNode::getBackgroundName() const
{
	return m_backgroundName;
}

const vector<SaveSpriteNode> &SaveNode::getSaveSpriteNodeVector() const
{
	return m_saveSpriteNodeVector;
}









// class SaveLoadContainer
SaveLoadContainer::SaveLoadContainer()
{
	m_saveNodeVector.reserve(TOTAL_GAME_SAVES);
}

bool SaveLoadContainer::init(const string &homeDirectory)
{
	bool ret = true;

	SaveNode saveNode;
	for(int i = 0;i < TOTAL_GAME_SAVES;i++)
		m_saveNodeVector.push_back(saveNode);

	string gameSaveString = homeDirectory;
	gameSaveString += "/game.sav";
	m_gameSaveFile.setFileName(gameSaveString.c_str());
	if(!m_gameSaveFile.open(QIODevice::ReadWrite))
	{
		ret = false;
		fprintf(stderr,"error, could not open file: %s\n",gameSaveString.c_str());
	}
	if(ret)
	{
		if(m_gameSaveFile.seek(0))
		{
			QByteArray saveData = m_gameSaveFile.readAll();
			int length = saveData.length();
			if(length >= (int) sizeof(int))
			{
				int count = *((int*) saveData.data());  // I know we have a hard defined save total but I tend to change my mind, lets keep a count first variable so we have no issues on version upgrades
				unsigned char *dataOffset = (unsigned char*) saveData.data() + sizeof(int);
				length -= (int) sizeof(int);
				for(int i = 0;i < count;i++)
				{
					if(!parseSaveNode(dataOffset,length,i))
					{
						ret = false;
						fprintf(stderr,"error parsing SaveNode in file: %s\n",m_gameSaveFile.fileName().toStdString().c_str());
						break;
					}
				}
			}
		}
		else
			fprintf(stderr,"error, could not seek file: %s\n",m_gameSaveFile.fileName().toStdString().c_str());
	}
	return ret;
}

bool SaveLoadContainer::parseSaveNode(unsigned char *&dataOffset,int &length,int index)
{
	int ret = true;
	if(*((int*) dataOffset)) // in use
	{
		dataOffset += sizeof(int);
		length -= (int) sizeof(int);
		if(length >= (int) sizeof(time_t))
		{
			time_t currentTime = *((time_t*) dataOffset);
			dataOffset += sizeof(time_t);
			length -= (int) sizeof(time_t);
			if(length >= (int) sizeof(int))
			{
				int scenarioOffset = *((int*) dataOffset);
				dataOffset += sizeof(int);
				length -= (int) sizeof(int);
				if(length >= (int) sizeof(int))
				{
					int sceneOffset = *((int*) dataOffset);
					dataOffset += sizeof(int);
					length -= (int) sizeof(int);
					string backgroundString;
					bool bufferOverflow = false;
					bool done = false;
					while(!done)
					{
						if(length <= 0)
						{
							done = true;
							bufferOverflow = true;
							continue;
						}
						if(*dataOffset)
							backgroundString += *dataOffset;
						else
							done = true;
						++dataOffset;
						--length;
					}
					if(bufferOverflow)
					{
						fprintf(stderr,"error parsing file: %s\n",m_gameSaveFile.fileName().toStdString().c_str());
						ret = false;
					}
					else
					{
						if(length >= (int) sizeof(int))
						{
							vector<SaveSpriteNode> saveSpriteNodeVector;
							int count = *((int*) dataOffset);
							dataOffset += sizeof(int);
							length -= (int) sizeof(int);
							if(count)
							{
								for(int i = 0;i < count;i++)
								{
									if(!parseSaveSpriteNodes(dataOffset,length,saveSpriteNodeVector))
									{
										ret = false;
										fprintf(stderr,"error parsing SaveSpriteNode in file: %s\n",m_gameSaveFile.fileName().toStdString().c_str());
										break;
									}
								}
							}
							if(ret)
							{
								SaveNode saveNode(currentTime,scenarioOffset,sceneOffset,backgroundString,saveSpriteNodeVector);
								m_saveNodeVector.at(index) = saveNode;
							}
						}
						else
						{
							fprintf(stderr,"error parsing file: %s\n",m_gameSaveFile.fileName().toStdString().c_str());
							ret = false;
						}
					}
				}
				else
				{
					fprintf(stderr,"error parsing file: %s\n",m_gameSaveFile.fileName().toStdString().c_str());
					ret = false;
				}
			}
			else
			{
				fprintf(stderr,"error parsing file: %s\n",m_gameSaveFile.fileName().toStdString().c_str());
				ret = false;
			}
		}
		else
		{
			fprintf(stderr,"error parsing file: %s\n",m_gameSaveFile.fileName().toStdString().c_str());
			ret = false;
		}
	}
	else
	{
		dataOffset += sizeof(int);  // not inUse, offset inUse
		length -= (int) sizeof(int);
	}
	return ret;
}

bool SaveLoadContainer::parseSaveSpriteNodes(unsigned char *&dataOffset,int &length,vector<SaveSpriteNode> &saveSpriteNodeVector)
{
	bool ret = true;
	if(length >= (int) sizeof(int))
	{
		int verticalPosition = *((int*) dataOffset);
		dataOffset += sizeof(int);
		length -= (int) sizeof(int);
		if(length >= (int) sizeof(int))
		{
			int horizontalPosition = *((int*) dataOffset);
			dataOffset += sizeof(int);
			length -= (int) sizeof(int);
			string spriteName;
			bool bufferOverflow = false;
			bool done = false;
			while(!done)
			{
				if(length <= 0)
				{
					done = true;
					bufferOverflow = true;
					continue;
				}
				if(*dataOffset)
					spriteName += *dataOffset;
				else
					done = true;
				++dataOffset;
				--length;
			}
			if(bufferOverflow)
			{
				fprintf(stderr,"error parsing file: %s\n",m_gameSaveFile.fileName().toStdString().c_str());
				ret = false;
			}
			else
			{
				SaveSpriteNode saveSpriteNode(verticalPosition,horizontalPosition,spriteName);
				saveSpriteNodeVector.push_back(saveSpriteNode);
			}
		}
		else
		{
			fprintf(stderr,"error parsing file: %s\n",m_gameSaveFile.fileName().toStdString().c_str());
			ret = false;
		}
	}
	else
	{
		fprintf(stderr,"error parsing file: %s\n",m_gameSaveFile.fileName().toStdString().c_str());
		ret = false;
	}
	return ret;
}

time_t SaveLoadContainer::getSaveNodeTime(int index)
{
	time_t returnTime = 0;
	if(m_saveNodeVector.size())
	{
		if(index >= 0 && index < (int) m_saveNodeVector.size())
			returnTime = m_saveNodeVector.at(index).getTime();
	}
	return returnTime;
}

bool SaveLoadContainer::getSaveNodeInUse(int index)
{
	bool ret = false;
	if(m_saveNodeVector.size())
	{
		if(index >= 0 && index < (int) m_saveNodeVector.size())
			ret = m_saveNodeVector.at(index).getInUse();
	}
	return ret;
}

bool SaveLoadContainer::setSaveNode(int index,const SaveNode &sn)
{
	bool ret = true;
	if(m_saveNodeVector.size())
	{
		if(index >= 0 && index < (int) m_saveNodeVector.size())
		{
			m_saveNodeVector.at(index) = sn;
			ret = writeSaveNodesToDisk();
		}
		else
		{
			ret = false;
			fprintf(stderr,"error index: %d out of range for m_saveNodeVector\n",index);
		}
	}
	else
	{
		ret = false;
		fprintf(stderr,"error m_saveNodeVector is empty\n");
	}
	return ret;
}

bool SaveLoadContainer::getSaveNode(int index,SaveNode &sn)
{
	bool ret = true;
	if(m_saveNodeVector.size())
	{
		if(index >= 0 && index < (int) m_saveNodeVector.size())
			sn = m_saveNodeVector.at(index);
		else
		{
			ret = false;
			fprintf(stderr,"error index: %d out of range for m_saveNodeVector\n",index);
		}
	}
	else
	{
		ret = false;
		fprintf(stderr,"error m_saveNodeVector is empty\n");
	}
	return ret;
}

bool SaveLoadContainer::writeSaveNodesToDisk()
{
	bool ret = true;
	if(m_saveNodeVector.size())
	{
		vector<unsigned char> fileWriteVector;
		fileWriteVector.reserve(4096);
		int count = (int) m_saveNodeVector.size();
		for(int i = 0;i < (int) sizeof(int);i++)
			fileWriteVector.push_back(((unsigned char*) (&count))[i]);
		for(int i = 0;i < count;i++)
		{
			SaveNode saveNode = m_saveNodeVector.at(i);
			addSaveNodeToWriteVector(saveNode,fileWriteVector);
		}
		unsigned char *writeBuffer = new unsigned char[fileWriteVector.size()];
		for(int i = 0;i < (int) fileWriteVector.size();i++)
			writeBuffer[i] = fileWriteVector.at(i);
		if(m_gameSaveFile.resize(0))
		{
			count = m_gameSaveFile.write((char*) writeBuffer,fileWriteVector.size());
			if(count != (int) fileWriteVector.size())
			{
				fprintf(stderr,"error writing game save file, write size does not match buffer size\n");
				ret = false;
			}
		}
		else
		{
			fprintf(stderr,"error writing game save file, truncate failed\n");
			ret = false;
		}
		delete[] writeBuffer;
	}
	else
	{
		fprintf(stderr,"error m_saveNodeVector is empty\n");
		ret = false;
	}
	return ret;
}

void SaveLoadContainer::addSaveNodeToWriteVector(const SaveNode &saveNode,vector<unsigned char> &fileWriteVector)
{
	int inUse = 0;
	if(saveNode.getInUse())
		inUse = 1;
	for(int i = 0;i < (int) sizeof(int);i++)
		fileWriteVector.push_back(((unsigned char*) (&inUse))[i]);
	if(saveNode.getInUse())
	{
		time_t saveNodeTime = saveNode.getTime();
		for(int i = 0;i < (int) sizeof(time_t);i++)
			fileWriteVector.push_back(((unsigned char*) (&saveNodeTime))[i]);
		int scenarioOffset = saveNode.getScenarioOffset();
		for(int i = 0;i < (int) sizeof(int);i++)
			fileWriteVector.push_back(((unsigned char*) (&scenarioOffset))[i]);
		int m_sceneOffset = saveNode.getSceneOffset();
		for(int i = 0;i < (int) sizeof(int);i++)
			fileWriteVector.push_back(((unsigned char*) (&m_sceneOffset))[i]);
		string backgroundName = saveNode.getBackgroundName();
		for(int i = 0;i < (int) backgroundName.length();i++)
			fileWriteVector.push_back(backgroundName[i]);
		fileWriteVector.push_back('\0');
		vector<SaveSpriteNode> saveSpriteNodeVector = saveNode.getSaveSpriteNodeVector();
		int count = (int) saveSpriteNodeVector.size();
		for(int i = 0;i < (int) sizeof(int);i++)
			fileWriteVector.push_back(((unsigned char*) (&count))[i]);
		for(int i = 0;i < (int) saveSpriteNodeVector.size();i++)
		{
			SaveSpriteNode saveSpriteNode = saveSpriteNodeVector.at(i);
			addSaveSpriteNodeToWriteVector(saveSpriteNode,fileWriteVector);
		}
	}
}

void SaveLoadContainer::addSaveSpriteNodeToWriteVector(const SaveSpriteNode &saveSpriteNode,vector<unsigned char> &fileWriteVector)
{
	int verticalPosition = saveSpriteNode.getVerticalPosition();
	for(int i = 0;i < (int) sizeof(int);i++)
		fileWriteVector.push_back(((unsigned char*) (&verticalPosition))[i]);
	int horizontalPosition = saveSpriteNode.getHorizontalPosition();
	for(int i = 0;i < (int) sizeof(int);i++)
		fileWriteVector.push_back(((unsigned char*) (&horizontalPosition))[i]);
	string spriteName = saveSpriteNode.getSpriteName();
	for(int i = 0;i < (int) spriteName.length();i++)
		fileWriteVector.push_back(spriteName[i]);
	fileWriteVector.push_back('\0');
}

bool SaveLoadContainer::isLoadAvailable()
{
	bool ret = false;
	for(int i = 0;i < TOTAL_GAME_SAVES;i++)
	{
		if(m_saveNodeVector.at(i).getInUse())
		{
			ret = true;
			break;
		}
	}
	return ret;
}
