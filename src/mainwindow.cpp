/*
 * mainwindow.cpp
 *
 * Copyright 2012 - 2013 Chris Ohmstede
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 3
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 *
 */

#include "mainwindow.h"
#include "activesprite.h"
#include <QtCore/QFile>
#include <QtGui/QMessageBox>
#include "userchoicedialog.h"
#include <sstream>
#include <QtCore/QSettings>
#include <QtCore/QDir>
#include "saveloadverificationdialog.h"
#include <QtGui/QSplashScreen>
#include <QtGui/QApplication>
#include <QtGui/QDesktopWidget>
#include "triviadialog.h"
#ifdef LINUX_BUILD
#include "linuxconfig.h"
#endif


MainWindow::MainWindow(QWidget *parent) :
		QMainWindow(parent),
		m_conversationLabel(NULL),
		m_movieMediaObject(NULL),
		m_movieAudioOutput(NULL),
		m_soundEffectMediaObject(NULL),
		m_soundEffectAudioOutput(NULL),
#ifdef CONVERSATION_KEY_CLICK
		m_conversationTypingMediaObject(NULL),
		m_conversationTypingAudioOutput(NULL),
#endif
		m_currentSceneType(GameScene::SCENE_ERROR),
#ifdef CONVERSATION_KEY_CLICK
		m_conversationPlayEstablished(false),
#endif
		m_soundPlayEstablished(false),
		m_moviePlayEstablished(false),
		m_audioVideoWarnDisplayed(false),
		m_threadQuit(false),
		previousPlayEstablished(false)
{
	setWindowTitle(APPLICATION_TITLE);

	m_backgroundVector.reserve(BACKGROUND_MAX);
	m_spriteVector.reserve(SPRITE_MAX);
	m_movieVector.reserve(MOVIE_MAX);
	m_soundVector.reserve(SOUND_MAX);
	m_gameScenarioVector.reserve(SCENARIO_MAX);

#ifdef CONVERSATION_KEY_CLICK
	m_audioVideoMonitorThreadConversation.setParentAndType(this,AudioVideoMonitorThread::AUDIO_VIDEO_CONVERSATION);
#endif
	m_audioVideoMonitorThreadSound.setParentAndType(this,AudioVideoMonitorThread::AUDIO_VIDEO_SOUND);
	m_audioVideoMonitorThreadMovie.setParentAndType(this,AudioVideoMonitorThread::AUDIO_VIDEO_MOVIE);
}

MainWindow::~MainWindow()
{
	if(!previousPlayEstablished)
	{
		m_audioVideoMonitorThreadMutex.lock();
		m_threadQuit = true;
		m_audioVideoMonitorThreadMutex.unlock();
#ifdef CONVERSATION_KEY_CLICK
		m_audioVideoMonitorThreadConversation.wait(MONITOR_THREAD_DESTRUCTOR_WAIT);
#endif
		m_audioVideoMonitorThreadSound.wait(MONITOR_THREAD_DESTRUCTOR_WAIT);
		m_audioVideoMonitorThreadMovie.wait(MONITOR_THREAD_DESTRUCTOR_WAIT);
#ifdef CONVERSATION_KEY_CLICK
		if(m_conversationPlayEstablished &&
				m_soundPlayEstablished &&
				m_moviePlayEstablished)
#else
		if(	m_soundPlayEstablished &&
				m_moviePlayEstablished)
#endif
		{
			QFile previousPlayEstablishedFile(m_previousPlayEstablishedFileName.c_str());
			if(previousPlayEstablishedFile.open(QIODevice::ReadWrite))
			{
				previousPlayEstablishedFile.write("\x01");
				previousPlayEstablishedFile.close();
			}
		}
	}
	if(m_movieMediaObject)
		delete m_movieMediaObject;
	if(m_movieAudioOutput)
		delete m_movieAudioOutput;
	if(m_soundEffectMediaObject)
		delete m_soundEffectMediaObject;
	if(m_soundEffectAudioOutput)
		delete m_soundEffectAudioOutput;
#ifdef CONVERSATION_KEY_CLICK
	if(m_conversationTypingMediaObject)
		delete m_conversationTypingMediaObject;
	if(m_conversationTypingAudioOutput)
		delete m_conversationTypingAudioOutput;
#endif
}

bool MainWindow::init(QApplication *app)
{
	bool ret = true;
#ifdef LINUX_BUILD
	string configFile = LINUX_SYSCONF_FROM_CONFIG;
	configFile += "/Melon/MurderPD.conf";
	QSettings systemSettings(configFile.c_str(),QSettings::NativeFormat);
#endif
#ifdef WINDOWS_BUILD
	QSettings systemSettings("HKEY_LOCAL_MACHINE\\SOFTWARE\\Melon\\MurderPD",QSettings::NativeFormat);
#endif
	systemSettings.beginGroup("directories");
	string dataDirectory = systemSettings.value("data_directory").toString().toStdString();
	systemSettings.endGroup();

	string splashPixmapPath = dataDirectory;
	splashPixmapPath += "/image/bar.png";
	QPixmap splashPixmap(splashPixmapPath.c_str());
	QSplashScreen *splash = new QSplashScreen(splashPixmap);
	splash->show();
	app->processEvents();
	string firstSplashLine = "\n\n\n\n\nA Murder In The Public Domain\n\n";
	string secondSpalshLine = "Loading resources...";
	string thirdSplashLine = "\n\nPlease wait";
	string splashLine = firstSplashLine + secondSpalshLine + thirdSplashLine;
	splash->showMessage(splashLine.c_str(),Qt::AlignCenter,Qt::white);
	app->processEvents();

	ret = loadBackgrounds(dataDirectory);
	if(ret)
		ret = loadSprites(dataDirectory);
	if(ret)
		ret = loadMovies(dataDirectory);
	if(ret)
		ret = loadSounds(dataDirectory);
	secondSpalshLine = "Verifying resources...";
	splashLine = firstSplashLine + secondSpalshLine + thirdSplashLine;
	splash->showMessage(splashLine.c_str(),Qt::AlignCenter,Qt::white);
	app->processEvents();
	if(ret)
		ret = loadScenarios(dataDirectory);
	if(ret)
		ret = crossCheckBackgroundSprite();

	if(ret)
	{
		string homeDirectory;
		homeDirectory = QDir::homePath().toStdString();
		if(homeDirectory[homeDirectory.length() - 1] != '/')
			homeDirectory += '/';
		homeDirectory += ".MurderPD";
		QDir homeDir(homeDirectory.c_str());
		if(!homeDir.exists())
		{
			if(!homeDir.mkpath(homeDirectory.c_str()))
			{
				ret = false;
				fprintf(stderr,"error, could not create directory: %s\n",homeDirectory.c_str());
			}
		}
		if(ret)
		{
			m_previousPlayEstablishedFileName = homeDirectory;
			m_previousPlayEstablishedFileName += "/ppe.st";
			QFile previousPlayEstablishedFile(m_previousPlayEstablishedFileName.c_str());
			if(previousPlayEstablishedFile.exists())
				previousPlayEstablished = true;
		}
		if(ret)
		{
			if(!m_saveLoadContainer.init(homeDirectory))
				ret = false;
			if(ret)
			{
				if(!m_skipContainer.init(SCENARIO_MAX,homeDirectory))
					ret = false;
			}
		}
	}

	if(ret)
	{
		string iconString = dataDirectory;
		iconString += "image/MurderPDicon64.png";
		QIcon myWindowIcon(iconString.c_str());
		setWindowIcon(myWindowIcon);

		string aboutTextFileString = dataDirectory;
		aboutTextFileString += "text/about_text.html";
		QFile aboutTextFile(aboutTextFileString.c_str());
		if(aboutTextFile.open(QIODevice::ReadOnly))
		{
			QByteArray text = aboutTextFile.readAll();
			m_aboutText = text.data();
		}

		string instructionsTextFileString = dataDirectory;
		instructionsTextFileString += "text/instructions.html";
		QFile instructionsTextFile(instructionsTextFileString.c_str());
		if(instructionsTextFile.open(QIODevice::ReadOnly))
		{
			QByteArray text = instructionsTextFile.readAll();
			m_instructionsText = text.data();
		}

		string problemsWithAudioVideoTextFileString = dataDirectory;
		problemsWithAudioVideoTextFileString += "text/problems_with_audio_video.html";
		QFile problemsWithAudioVideoTextFile(problemsWithAudioVideoTextFileString.c_str());
		if(problemsWithAudioVideoTextFile.open(QIODevice::ReadOnly))
		{
			QByteArray text = problemsWithAudioVideoTextFile.readAll();
			m_problemsWithAudioVideoText = text.data();
		}

		loadSaveIcons();

		secondSpalshLine = "Creating display...";
		splashLine = firstSplashLine + secondSpalshLine + thirdSplashLine;
		splash->showMessage(splashLine.c_str(),Qt::AlignCenter,Qt::white);
		app->processEvents();

		m_centralWidget = new QWidget(this);
		m_centralWidget->setMinimumWidth(IMAGE_MOVIE_MINIMUM_WIDTH);

		m_mainStack = new QStackedLayout();

		m_horizontalButtonLayout = new QHBoxLayout();
		m_imageMovieHorizontalLayout = new QHBoxLayout();
		m_gamePlayVerticalLayout = new QVBoxLayout();

		m_gameCentralWidget = new QWidget();
		m_videoWidget = new GamePlayVideoWidget(this);
		m_videoWidget->setMinimumHeight(IMAGE_MOVIE_MINIMUM_HEIGHT);
		m_videoWidget->hide();

		m_pixmapDisplayLabel = new GamePlayLabel(this);
		m_pixmapDisplayLabel->setMinimumHeight(IMAGE_MOVIE_MINIMUM_HEIGHT);
		QPixmap blackPixmap(m_backgroundVector.at(BACKGROUND_BLACK).c_str());
		blackPixmap.scaled(IMAGE_MOVIE_MINIMUM_WIDTH,IMAGE_MOVIE_MINIMUM_HEIGHT);
		m_pixmapDisplayLabel->setPixmap(blackPixmap);

		m_imageMovieHorizontalLayout->addStretch();
		m_imageMovieHorizontalLayout->addWidget(m_pixmapDisplayLabel);
		m_imageMovieHorizontalLayout->addWidget(m_videoWidget);
		m_imageMovieHorizontalLayout->addStretch();

		m_gamePlayVerticalLayout->addItem(m_imageMovieHorizontalLayout);

		m_conversationLabel = new GamePlayLabelWithKeyboardGrab(this);
		m_conversationLabel->setFrameStyle(QFrame::Box | QFrame::Sunken);
		m_conversationLabel->setWordWrap(true);
		m_conversationLabel->setLineWidth(CONVERASTION_LINE_WIDTH);
		m_conversationLabel->setMidLineWidth(CONVERASTION_LINE_WIDTH);
		m_conversationLabel->setMargin(CONVERSATION_MARGIN);
		m_conversationLabel->setMinimumHeight(CONVERSATION_MINIMUM_HEIGHT);
		QFont font = m_conversationLabel->font();
		font.setPointSize(CONVERSATION_FONT_SIZE);
		m_conversationLabel->setFont(font);
		m_conversationLabel->setAlignment(Qt::AlignLeft | Qt::AlignTop);

		m_gamePlayVerticalLayout->addWidget(m_conversationLabel);

		m_saveButton = new QPushButton("Save");
		m_saveButton->setMinimumWidth(GAME_PLAY_BUTTON_WIDTH);
		m_loadButton = new QPushButton("Load");
		m_loadButton->setMinimumWidth(GAME_PLAY_BUTTON_WIDTH);
		m_skipButton = new QPushButton("Skip");
		m_skipButton->setMinimumWidth(GAME_PLAY_BUTTON_WIDTH);
		m_titleButton = new QPushButton("Title");
		m_titleButton->setMinimumWidth(GAME_PLAY_BUTTON_WIDTH);
		m_previousButton = new QPushButton("<< Previous");
		m_previousButton->setMinimumWidth(GAME_PLAY_BUTTON_WIDTH);
		m_nextButton = new QPushButton("Next >>");
		m_nextButton->setMinimumWidth(GAME_PLAY_BUTTON_WIDTH);

		m_horizontalButtonLayout->addStretch();
		m_horizontalButtonLayout->addWidget(m_saveButton);
		m_horizontalButtonLayout->addStretch();
		m_horizontalButtonLayout->addWidget(m_loadButton);
		m_horizontalButtonLayout->addStretch();
		m_horizontalButtonLayout->addWidget(m_previousButton);
		m_horizontalButtonLayout->addStretch();
		m_horizontalButtonLayout->addWidget(m_nextButton);
		m_horizontalButtonLayout->addStretch();
		m_horizontalButtonLayout->addWidget(m_skipButton);
		m_horizontalButtonLayout->addStretch();
		m_horizontalButtonLayout->addWidget(m_titleButton);
		m_horizontalButtonLayout->addStretch();

		enableGamePlayButtons(false);

		m_gamePlayVerticalLayout->addItem(m_horizontalButtonLayout);
		m_gamePlayVerticalLayout->setStretch(0,0);
		m_gamePlayVerticalLayout->setStretch(1,1);

		m_gameCentralWidget->setLayout(m_gamePlayVerticalLayout);

		m_titleHorizontalLayout = new QHBoxLayout();
		m_titleVerticalLayout = new QVBoxLayout();
		m_titleVerticalLayout->setSpacing(CONVERASTION_LINE_WIDTH);

		m_titleCentralWidget = new QWidget();

		m_titleLabel = new QLabel(APPLICATION_TITLE);
		font = m_titleLabel->font();
		font.setPointSize(TITLE_FONT_SIZE);
		m_titleLabel->setFont(font);
		m_horizontalTitleLabelLayout = new QHBoxLayout();
		m_horizontalTitleLabelLayout->addStretch();
		m_horizontalTitleLabelLayout->addWidget(m_titleLabel);
		m_horizontalTitleLabelLayout->addStretch();

		m_horizontalGamePlayButtonLayout = new QHBoxLayout();
		m_gamePlayButton = new QPushButton("Play");
		m_gamePlayButton->setMinimumWidth(DEFAULT_TITLE_BUTTON_WIDTH);
		m_horizontalGamePlayButtonLayout->addStretch();
		m_horizontalGamePlayButtonLayout->addWidget(m_gamePlayButton);
		m_horizontalGamePlayButtonLayout->addStretch();

		m_horizontalGameLoadButtonLayout = new QHBoxLayout();
		m_gameLoadButton = new QPushButton("Load");
		m_gameLoadButton->setMinimumWidth(DEFAULT_TITLE_BUTTON_WIDTH);
		if(!m_saveLoadContainer.isLoadAvailable())
			m_gameLoadButton->setEnabled(false);
		m_horizontalGameLoadButtonLayout->addStretch();
		m_horizontalGameLoadButtonLayout->addWidget(m_gameLoadButton);
		m_horizontalGameLoadButtonLayout->addStretch();

		m_horizontalInstructionsButtonLayout = new QHBoxLayout();
		m_instructionsButton = new QPushButton("Instructions");
		m_instructionsButton->setMinimumWidth(DEFAULT_TITLE_BUTTON_WIDTH);
		m_horizontalInstructionsButtonLayout->addStretch();
		m_horizontalInstructionsButtonLayout->addWidget(m_instructionsButton);
		m_horizontalInstructionsButtonLayout->addStretch();

		m_horizontalAboutButtonLayout = new QHBoxLayout();
		m_aboutButton = new QPushButton("About");
		m_aboutButton->setMinimumWidth(DEFAULT_TITLE_BUTTON_WIDTH);
		m_horizontalAboutButtonLayout->addStretch();
		m_horizontalAboutButtonLayout->addWidget(m_aboutButton);
		m_horizontalAboutButtonLayout->addStretch();

		m_horizontalproblemsWithAudioVideoButtonLayout = new QHBoxLayout();
		m_problemsWithAudioVideoButton = new QPushButton("Problems With Audio / Video");
		m_problemsWithAudioVideoButton->setMinimumWidth(DEFAULT_TITLE_BUTTON_WIDTH);
		m_horizontalproblemsWithAudioVideoButtonLayout->addStretch();
		m_horizontalproblemsWithAudioVideoButtonLayout->addWidget(m_problemsWithAudioVideoButton);
		m_horizontalproblemsWithAudioVideoButtonLayout->addStretch();

		m_horizontalAboutQtButtonLayout = new QHBoxLayout();
		m_aboutQtButton = new QPushButton("About Qt");
		m_aboutQtButton->setMinimumWidth(DEFAULT_TITLE_BUTTON_WIDTH);
		m_horizontalAboutQtButtonLayout->addStretch();
		m_horizontalAboutQtButtonLayout->addWidget(m_aboutQtButton);
		m_horizontalAboutQtButtonLayout->addStretch();

		m_horizontalQuitButtonLayout = new QHBoxLayout();
		m_quitButton = new QPushButton("Quit");
		m_quitButton->setMinimumWidth(DEFAULT_TITLE_BUTTON_WIDTH);
		m_horizontalQuitButtonLayout->addStretch();
		m_horizontalQuitButtonLayout->addWidget(m_quitButton);
		m_horizontalQuitButtonLayout->addStretch();

		m_titleTopLine = new QFrame();
		m_titleTopLine->setFrameStyle(QFrame::HLine | QFrame::Raised);
		m_titleTopLine->setLineWidth(FRAME_LINE_WIDTH);

		m_titlePixmapDisplayLabel = new QLabel();
		m_titlePixmapDisplayLabel->setMaximumSize(TITLE_PIXMAP_DISPLAY_WIDTH,TITLE_PIXMAP_DISPLAY_HEIGHT);
		QPixmap barPixmap(m_backgroundVector.at(BACKGROUND_BAR).c_str());
		m_titlePixmapDisplayLabel->setPixmap(barPixmap);

		m_horizontalTitlePixmapDisplayLabelLayout = new QHBoxLayout();
		m_horizontalTitlePixmapDisplayLabelLayout->addStretch();
		m_horizontalTitlePixmapDisplayLabelLayout->addWidget(m_titlePixmapDisplayLabel);
		m_horizontalTitlePixmapDisplayLabelLayout->addStretch();

		m_titleBottomLine = new QFrame();
		m_titleBottomLine->setFrameStyle(QFrame::HLine | QFrame::Raised);
		m_titleBottomLine->setLineWidth(FRAME_LINE_WIDTH);

		m_titleVerticalLayout->addItem(m_horizontalTitleLabelLayout);
		m_titleVerticalLayout->addStretch();
		m_titleVerticalLayout->addItem(m_horizontalGamePlayButtonLayout);
		m_titleVerticalLayout->addItem(m_horizontalGameLoadButtonLayout);
		m_titleVerticalLayout->addItem(m_horizontalInstructionsButtonLayout);
		m_titleVerticalLayout->addItem(m_horizontalproblemsWithAudioVideoButtonLayout);
		m_titleVerticalLayout->addItem(m_horizontalAboutButtonLayout);
		m_titleVerticalLayout->addItem(m_horizontalAboutQtButtonLayout);
		m_titleVerticalLayout->addItem(m_horizontalQuitButtonLayout);
		m_titleVerticalLayout->addStretch();
		m_titleVerticalLayout->addWidget(m_titleTopLine);
		m_titleVerticalLayout->addItem(m_horizontalTitlePixmapDisplayLabelLayout);
		m_titleVerticalLayout->addWidget(m_titleBottomLine);

		m_titleHorizontalLayout->addStretch();
		m_titleHorizontalLayout->addItem(m_titleVerticalLayout);
		m_titleHorizontalLayout->addStretch();

		m_titleCentralWidget->setLayout(m_titleHorizontalLayout);

		m_saveLoadVerticalLayout = new QVBoxLayout();
		m_saveLoadVerticalLayout->setSpacing(SAVE_LOAD_SPACING);
		m_saveLoadTitleHorizontalLayout = new QHBoxLayout();
		m_saveLoadPreviousNextHorizontalLayout = new QHBoxLayout();
		m_saveLoad1HorizontalLayout = new QHBoxLayout();
		m_saveLoad2HorizontalLayout = new QHBoxLayout();
		m_saveLoadCancelHorizontalLayout = new QHBoxLayout();

		m_saveLoadButton1HorizontalLayout = new QHBoxLayout();
		m_saveLoadLabel1HorizontalLayout = new QHBoxLayout();
		m_saveLoadButton1VerticalLayout = new QVBoxLayout();
		m_saveLoadButton2HorizontalLayout = new QHBoxLayout();
		m_saveLoadLabel2HorizontalLayout = new QHBoxLayout();
		m_saveLoadButton2VerticalLayout = new QVBoxLayout();
		m_saveLoadButton3HorizontalLayout = new QHBoxLayout();
		m_saveLoadLabel3HorizontalLayout = new QHBoxLayout();
		m_saveLoadButton3VerticalLayout = new QVBoxLayout();
		m_saveLoadButton4HorizontalLayout = new QHBoxLayout();
		m_saveLoadLabel4HorizontalLayout = new QHBoxLayout();
		m_saveLoadButton4VerticalLayout = new QVBoxLayout();
		m_saveLoadButton5HorizontalLayout = new QHBoxLayout();
		m_saveLoadLabel5HorizontalLayout = new QHBoxLayout();
		m_saveLoadButton5VerticalLayout = new QVBoxLayout();
		m_saveLoadButton6HorizontalLayout = new QHBoxLayout();
		m_saveLoadLabel6HorizontalLayout = new QHBoxLayout();
		m_saveLoadButton6VerticalLayout = new QVBoxLayout();

		m_saveLoadCentralWidget = new QWidget();

		m_saveLoadTitleLabel = new QLabel("Save Game");
		font = m_saveLoadTitleLabel->font();
		font.setPointSize(TITLE_FONT_SIZE);
		m_saveLoadTitleLabel->setFont(font);

		m_saveLoadTitleHorizontalLayout->addStretch();
		m_saveLoadTitleHorizontalLayout->addWidget(m_saveLoadTitleLabel);
		m_saveLoadTitleHorizontalLayout->addStretch();

		m_saveLoadPreviousButton = new QPushButton("<< Previous");
		m_saveLoadPreviousButton->setMinimumWidth(DEFAULT_SAVE_LOAD_BUTTON_WIDTH);
		m_saveLoadNextButton = new QPushButton("Next >>");
		m_saveLoadNextButton->setMinimumWidth(DEFAULT_SAVE_LOAD_BUTTON_WIDTH);

		m_saveLoadPreviousNextHorizontalLayout->addStretch();
		m_saveLoadPreviousNextHorizontalLayout->addWidget(m_saveLoadPreviousButton);
		m_saveLoadPreviousNextHorizontalLayout->addStretch();
		m_saveLoadPreviousNextHorizontalLayout->addWidget(m_saveLoadNextButton);
		m_saveLoadPreviousNextHorizontalLayout->addStretch();

		QSize iconSize(SAVE_LOAD_PIXMAP_WIDTH,SAVE_LOAD_PIXMAP_HEIGHT);

		m_saveLoad1Button = new QPushButton();
		m_saveLoad1Button->setIconSize(iconSize);
		m_saveLoad1Label = new QLabel("Open");

		m_saveLoadButton1HorizontalLayout->addStretch();
		m_saveLoadButton1HorizontalLayout->addWidget(m_saveLoad1Button);
		m_saveLoadButton1HorizontalLayout->addStretch();

		m_saveLoadLabel1HorizontalLayout->addStretch();
		m_saveLoadLabel1HorizontalLayout->addWidget(m_saveLoad1Label);
		m_saveLoadLabel1HorizontalLayout->addStretch();

		m_saveLoadButton1VerticalLayout->addItem(m_saveLoadButton1HorizontalLayout);
		m_saveLoadButton1VerticalLayout->addItem(m_saveLoadLabel1HorizontalLayout);

		m_saveLoad2Button = new QPushButton();
		m_saveLoad2Button->setIconSize(iconSize);
		m_saveLoad2Label = new QLabel("Open");

		m_saveLoadButton2HorizontalLayout->addStretch();
		m_saveLoadButton2HorizontalLayout->addWidget(m_saveLoad2Button);
		m_saveLoadButton2HorizontalLayout->addStretch();

		m_saveLoadLabel2HorizontalLayout->addStretch();
		m_saveLoadLabel2HorizontalLayout->addWidget(m_saveLoad2Label);
		m_saveLoadLabel2HorizontalLayout->addStretch();

		m_saveLoadButton2VerticalLayout->addItem(m_saveLoadButton2HorizontalLayout);
		m_saveLoadButton2VerticalLayout->addItem(m_saveLoadLabel2HorizontalLayout);

		m_saveLoad3Button = new QPushButton();
		m_saveLoad3Button->setIconSize(iconSize);
		m_saveLoad3Label = new QLabel("Open");

		m_saveLoadButton3HorizontalLayout->addStretch();
		m_saveLoadButton3HorizontalLayout->addWidget(m_saveLoad3Button);
		m_saveLoadButton3HorizontalLayout->addStretch();

		m_saveLoadLabel3HorizontalLayout->addStretch();
		m_saveLoadLabel3HorizontalLayout->addWidget(m_saveLoad3Label);
		m_saveLoadLabel3HorizontalLayout->addStretch();

		m_saveLoadButton3VerticalLayout->addItem(m_saveLoadButton3HorizontalLayout);
		m_saveLoadButton3VerticalLayout->addItem(m_saveLoadLabel3HorizontalLayout);

		m_saveLoad1HorizontalLayout->addStretch();
		m_saveLoad1HorizontalLayout->addItem(m_saveLoadButton1VerticalLayout);
		m_saveLoad1HorizontalLayout->addStretch();
		m_saveLoad1HorizontalLayout->addItem(m_saveLoadButton2VerticalLayout);
		m_saveLoad1HorizontalLayout->addStretch();
		m_saveLoad1HorizontalLayout->addItem(m_saveLoadButton3VerticalLayout);
		m_saveLoad1HorizontalLayout->addStretch();

		m_saveLoad4Button = new QPushButton();
		m_saveLoad4Button->setIconSize(iconSize);
		m_saveLoad4Label = new QLabel("Open");

		m_saveLoadButton4HorizontalLayout->addStretch();
		m_saveLoadButton4HorizontalLayout->addWidget(m_saveLoad4Button);
		m_saveLoadButton4HorizontalLayout->addStretch();

		m_saveLoadLabel4HorizontalLayout->addStretch();
		m_saveLoadLabel4HorizontalLayout->addWidget(m_saveLoad4Label);
		m_saveLoadLabel4HorizontalLayout->addStretch();

		m_saveLoadButton4VerticalLayout->addItem(m_saveLoadButton4HorizontalLayout);
		m_saveLoadButton4VerticalLayout->addItem(m_saveLoadLabel4HorizontalLayout);

		m_saveLoad5Button = new QPushButton();
		m_saveLoad5Button->setIconSize(iconSize);
		m_saveLoad5Label = new QLabel("Open");

		m_saveLoadButton5HorizontalLayout->addStretch();
		m_saveLoadButton5HorizontalLayout->addWidget(m_saveLoad5Button);
		m_saveLoadButton5HorizontalLayout->addStretch();

		m_saveLoadLabel5HorizontalLayout->addStretch();
		m_saveLoadLabel5HorizontalLayout->addWidget(m_saveLoad5Label);
		m_saveLoadLabel5HorizontalLayout->addStretch();

		m_saveLoadButton5VerticalLayout->addItem(m_saveLoadButton5HorizontalLayout);
		m_saveLoadButton5VerticalLayout->addItem(m_saveLoadLabel5HorizontalLayout);

		m_saveLoad6Button = new QPushButton();
		m_saveLoad6Button->setIconSize(iconSize);
		m_saveLoad6Label = new QLabel("Open");

		m_saveLoadButton6HorizontalLayout->addStretch();
		m_saveLoadButton6HorizontalLayout->addWidget(m_saveLoad6Button);
		m_saveLoadButton6HorizontalLayout->addStretch();

		m_saveLoadLabel6HorizontalLayout->addStretch();
		m_saveLoadLabel6HorizontalLayout->addWidget(m_saveLoad6Label);
		m_saveLoadLabel6HorizontalLayout->addStretch();

		m_saveLoadButton6VerticalLayout->addItem(m_saveLoadButton6HorizontalLayout);
		m_saveLoadButton6VerticalLayout->addItem(m_saveLoadLabel6HorizontalLayout);

		m_saveLoad2HorizontalLayout->addStretch();
		m_saveLoad2HorizontalLayout->addItem(m_saveLoadButton4VerticalLayout);
		m_saveLoad2HorizontalLayout->addStretch();
		m_saveLoad2HorizontalLayout->addItem(m_saveLoadButton5VerticalLayout);
		m_saveLoad2HorizontalLayout->addStretch();
		m_saveLoad2HorizontalLayout->addItem(m_saveLoadButton6VerticalLayout);
		m_saveLoad2HorizontalLayout->addStretch();

		m_saveLoadCancelButton = new QPushButton("Cancel");

		m_saveLoadCancelHorizontalLayout->addStretch();
		m_saveLoadCancelHorizontalLayout->addWidget(m_saveLoadCancelButton);
		m_saveLoadCancelHorizontalLayout->addStretch();

		m_saveLoadVerticalLayout->addItem(m_saveLoadTitleHorizontalLayout);
		m_saveLoadVerticalLayout->addStretch();
		m_saveLoadVerticalLayout->addItem(m_saveLoadPreviousNextHorizontalLayout);
		m_saveLoadVerticalLayout->addItem(m_saveLoad1HorizontalLayout);
		m_saveLoadVerticalLayout->addItem(m_saveLoad2HorizontalLayout);
		m_saveLoadVerticalLayout->addStretch();
		m_saveLoadVerticalLayout->addItem(m_saveLoadCancelHorizontalLayout);

		m_saveLoadCentralWidget->setLayout(m_saveLoadVerticalLayout);

		m_aboutVerticalLayout = new QVBoxLayout();
		m_horizontalAboutIconPixmapLayout = new QHBoxLayout();
		m_horizontalAboutTitleLayout = new QHBoxLayout();
		m_horizontalAboutVersionLayout = new QHBoxLayout();
		m_horizontalAboutCopyrightLayout = new QHBoxLayout();
		m_horizontalAboutTitleButtonLayout = new QHBoxLayout();

		m_aboutCentralWidget = new QWidget();

		m_aboutIconPixmapLabel = new QLabel();
		m_aboutIconPixmapLabel->setPixmap(myWindowIcon.pixmap(ABOUT_ICON_WIDTH,ABOUT_ICON_HEIGHT));

		m_horizontalAboutIconPixmapLayout->addStretch();
		m_horizontalAboutIconPixmapLayout->addWidget(m_aboutIconPixmapLabel);
		m_horizontalAboutIconPixmapLayout->addStretch();

		m_aboutTitleLabel = new QLabel(APPLICATION_TITLE);
		font = m_aboutTitleLabel->font();
		font.setPointSize(TITLE_FONT_SIZE);
		m_aboutTitleLabel->setFont(font);

		m_horizontalAboutTitleLayout->addStretch();
		m_horizontalAboutTitleLayout->addWidget(m_aboutTitleLabel);
		m_horizontalAboutTitleLayout->addStretch();

		string versionString = "Version: ";
		versionString += APPLICATION_VERSION;

		m_aboutVersionLabel = new QLabel(versionString.c_str());

		m_horizontalAboutVersionLayout->addStretch();
		m_horizontalAboutVersionLayout->addWidget(m_aboutVersionLabel);
		m_horizontalAboutVersionLayout->addStretch();

		m_copyrightLabel = new QLabel("\xA9 2012 - 2013 Chris Ohmstede");

		m_horizontalAboutCopyrightLayout->addStretch();
		m_horizontalAboutCopyrightLayout->addWidget(m_copyrightLabel);
		m_horizontalAboutCopyrightLayout->addStretch();

		m_aboutTextBrowser = new QTextBrowser(this);
		m_aboutTextBrowser->setHtml(m_aboutText.c_str());
		m_aboutTextBrowser->setOpenExternalLinks(true);

		m_aboutTitleButton = new QPushButton("Title");

		m_horizontalAboutTitleButtonLayout->addStretch();
		m_horizontalAboutTitleButtonLayout->addWidget(m_aboutTitleButton);
		m_horizontalAboutTitleButtonLayout->addStretch();

		m_aboutVerticalLayout->addItem(m_horizontalAboutIconPixmapLayout);
		m_aboutVerticalLayout->addItem(m_horizontalAboutTitleLayout);
		m_aboutVerticalLayout->addItem(m_horizontalAboutVersionLayout);
		m_aboutVerticalLayout->addItem(m_horizontalAboutCopyrightLayout);
		m_aboutVerticalLayout->addWidget(m_aboutTextBrowser);
		m_aboutVerticalLayout->addItem(m_horizontalAboutTitleButtonLayout);

		m_aboutCentralWidget->setLayout(m_aboutVerticalLayout);

		m_instructionsVerticalLayout = new QVBoxLayout();
		m_horizontalInstructionsTitleButtonLayout = new QHBoxLayout();

		m_instructionsCentralWidget = new QWidget();

		m_instructionsTextBrowser = new QTextBrowser(this);
		m_instructionsTextBrowser->setOpenExternalLinks(true);

		m_instructionsTitleButton = new QPushButton("Title");

		m_horizontalInstructionsTitleButtonLayout->addStretch();
		m_horizontalInstructionsTitleButtonLayout->addWidget(m_instructionsTitleButton);
		m_horizontalInstructionsTitleButtonLayout->addStretch();

		m_instructionsVerticalLayout->addWidget(m_instructionsTextBrowser);
		m_instructionsVerticalLayout->addItem(m_horizontalInstructionsTitleButtonLayout);

		m_instructionsCentralWidget->setLayout(m_instructionsVerticalLayout);

		m_mainStack->addWidget(m_titleCentralWidget);
		m_mainStack->addWidget(m_gameCentralWidget);
		m_mainStack->addWidget(m_saveLoadCentralWidget);
		m_mainStack->addWidget(m_aboutCentralWidget);
		m_mainStack->addWidget(m_instructionsCentralWidget);

		m_centralWidget->setLayout(m_mainStack);

		setCentralWidget(m_centralWidget);

		m_mainStack->setCurrentWidget(m_titleCentralWidget);

		m_movieMediaObject = new Phonon::MediaObject(this);
		Phonon::createPath(m_movieMediaObject,m_videoWidget);
		m_movieAudioOutput = new Phonon::AudioOutput(Phonon::VideoCategory,this);
		Phonon::createPath(m_movieMediaObject,m_movieAudioOutput);

		m_movieMediaObject->setTickInterval(MOVIE_TICK_INTERVAL);

		m_soundEffectMediaObject = new Phonon::MediaObject(this);
		m_soundEffectAudioOutput = new Phonon::AudioOutput(Phonon::MusicCategory,this);
		Phonon::createPath(m_soundEffectMediaObject,m_soundEffectAudioOutput);

#ifdef CONVERSATION_KEY_CLICK
		m_conversationTypingMediaObject = new Phonon::MediaObject(this);
		m_conversationTypingAudioOutput = new Phonon::AudioOutput(Phonon::MusicCategory,this);
		Phonon::createPath(m_conversationTypingMediaObject,m_conversationTypingAudioOutput);

		QString conversationTypingFile(m_soundVector.at(SOUND_KEY_CLICK).c_str());
		m_conversationTypingMediaObject->setCurrentSource(conversationTypingFile);
#endif

		m_spriteSlideTimer = new QTimer(this);
		m_conversationWriteTimer = new QTimer(this);
		m_conversationWriteSkipTimer = new QTimer(this);

		connect(m_titleButton,SIGNAL(clicked()),this,SLOT(titleButtonClicked()));
		connect(m_movieMediaObject,SIGNAL(stateChanged(Phonon::State,Phonon::State)),this,SLOT(movieMediaStateChanged(Phonon::State,Phonon::State)));
		connect(m_spriteSlideTimer,SIGNAL(timeout()),this,SLOT(slideSprite()));
		connect(m_movieMediaObject,SIGNAL(tick(qint64)),this,SLOT(movieRunTime(qint64)));
#ifdef LINUX_BUILD
		connect(m_soundEffectMediaObject,SIGNAL(tick(qint64)),this,SLOT(soundEffectRunTime(qint64)));
#endif
#if defined(CONVERSATION_KEY_CLICK) && defined(LINUX_BUILD)
		connect(m_conversationTypingMediaObject,SIGNAL(tick(qint64)),this,SLOT(conversationTypingRunTime(qint64)));
#endif
		connect(m_conversationWriteTimer,SIGNAL(timeout()),this,SLOT(writeConversation()));
		connect(m_conversationWriteSkipTimer,SIGNAL(timeout()),this,SLOT(writeSkipConversation()));
		connect(m_soundEffectMediaObject,SIGNAL(stateChanged(Phonon::State,Phonon::State)),this,SLOT(soundEffectMediaStateChanged(Phonon::State,Phonon::State)));
		connect(m_gamePlayButton,SIGNAL(clicked()),this,SLOT(gamePlayButtonClicked()));
		connect(m_quitButton,SIGNAL(clicked()),this,SLOT(close()));
		connect(this,SIGNAL(signalStartScenario(int)),this,SLOT(startScenario(int)));
		connect(this,SIGNAL(signalAdvanceScenario()),this,SLOT(advanceScenario()));
		connect(this,SIGNAL(signalRewindScenario()),this,SLOT(rewindScenario()));
		connect(m_pixmapDisplayLabel,SIGNAL(signalAdvanceScenario()),this,SLOT(advanceScenario()));
		connect(m_conversationLabel,SIGNAL(signalAdvanceScenario()),this,SLOT(advanceScenario()));
		connect(m_conversationLabel,SIGNAL(signalSaveButton()),this,SLOT(saveButtonClicked()));
		connect(m_conversationLabel,SIGNAL(signalLoadButton()),this,SLOT(loadButtonClicked()));
		connect(m_conversationLabel,SIGNAL(signalSkipButton()),this,SLOT(skipButtonClicked()));
		connect(m_conversationLabel,SIGNAL(signalTitleButton()),this,SLOT(titleButtonClicked()));
#ifdef CONVERSATION_KEY_CLICK
		connect(m_conversationTypingMediaObject,SIGNAL(stateChanged(Phonon::State,Phonon::State)),this,SLOT(conversationTypingMediaStateChanged(Phonon::State,Phonon::State)));
#endif
		connect(m_skipButton,SIGNAL(clicked()),this,SLOT(skipButtonClicked()));
		connect(m_gameLoadButton,SIGNAL(clicked()),this,SLOT(gameLoadButtonClicked()));
		connect(m_saveLoadCancelButton,SIGNAL(clicked()),this,SLOT(saveLoadCancelButtonClicked()));
		connect(m_loadButton,SIGNAL(clicked()),this,SLOT(loadButtonClicked()));
		connect(m_saveButton,SIGNAL(clicked()),this,SLOT(saveButtonClicked()));
		connect(m_saveLoadPreviousButton,SIGNAL(clicked()),this,SLOT(saveLoadPreviousButtonClicked()));
		connect(m_saveLoadNextButton,SIGNAL(clicked()),this,SLOT(saveLoadNextButtonClicked()));
		connect(m_saveLoad1Button,SIGNAL(clicked()),this,SLOT(saveLoad1ButtonClicked()));
		connect(m_saveLoad2Button,SIGNAL(clicked()),this,SLOT(saveLoad2ButtonClicked()));
		connect(m_saveLoad3Button,SIGNAL(clicked()),this,SLOT(saveLoad3ButtonClicked()));
		connect(m_saveLoad4Button,SIGNAL(clicked()),this,SLOT(saveLoad4ButtonClicked()));
		connect(m_saveLoad5Button,SIGNAL(clicked()),this,SLOT(saveLoad5ButtonClicked()));
		connect(m_saveLoad6Button,SIGNAL(clicked()),this,SLOT(saveLoad6ButtonClicked()));
		connect(m_aboutButton,SIGNAL(clicked()),this,SLOT(aboutButtonClicked()));
		connect(m_aboutTitleButton,SIGNAL(clicked()),this,SLOT(aboutTitleButtonClicked()));
		connect(m_instructionsButton,SIGNAL(clicked()),this,SLOT(instructionsButtonClicked()));
		connect(m_instructionsTitleButton,SIGNAL(clicked()),this,SLOT(instructionsTitleButtonClicked()));
		connect(m_problemsWithAudioVideoButton,SIGNAL(clicked()),this,SLOT(problemsWithAudioVideoButtonClicked()));
		connect(m_aboutQtButton,SIGNAL(clicked()),this,SLOT(aboutQtButtonClicked()));
#ifdef CONVERSATION_KEY_CLICK
		connect(&m_audioVideoMonitorThreadConversation,SIGNAL(signalAudioVideoWarn()),this,SLOT(audioVideoWarn()));
#endif
		connect(&m_audioVideoMonitorThreadSound,SIGNAL(signalAudioVideoWarn()),this,SLOT(audioVideoWarn()));
		connect(&m_audioVideoMonitorThreadMovie,SIGNAL(signalAudioVideoWarn()),this,SLOT(audioVideoWarn()));
#ifdef CONVERSATION_KEY_CLICK
		connect(&m_audioVideoMonitorThreadConversation,SIGNAL(signalAdvanceScenario()),this,SLOT(advanceScenario()));
#endif
		connect(&m_audioVideoMonitorThreadSound,SIGNAL(signalAdvanceScenario()),this,SLOT(advanceScenario()));
		connect(&m_audioVideoMonitorThreadMovie,SIGNAL(signalAdvanceScenario()),this,SLOT(advanceScenario()));

		connect(m_previousButton,SIGNAL(clicked()),this,SLOT(previousButtonClicked()));
		connect(m_nextButton,SIGNAL(clicked()),this,SLOT(nextButtonClicked()));

		if(!previousPlayEstablished)
		{
#ifdef CONVERSATION_KEY_CLICK
			m_audioVideoMonitorThreadConversation.start();
#endif
			m_audioVideoMonitorThreadSound.start();
			m_audioVideoMonitorThreadMovie.start();
		}

		QRect currentGeometry = geometry();
		QRect screenRect = app->desktop()->availableGeometry();
		int startx = (screenRect.width() - IMAGE_MOVIE_MINIMUM_WIDTH) / 2;
		// center the main window x
		currentGeometry.setLeft(startx);
		currentGeometry.setTop(WINDOW_TOP_POSITION);
		// width and height are set for Windows to stop unneeded expanding, the values are low but must be > 0 for Linux
		currentGeometry.setWidth(IMAGE_MOVIE_MINIMUM_WIDTH);
		currentGeometry.setHeight(IMAGE_MOVIE_MINIMUM_HEIGHT);
		setGeometry(currentGeometry);
		srand(time(NULL));
	}
	splash->finish(this);
	if(!ret)
	{
		QMessageBox mb(QMessageBox::Critical,"Resource Error","I could not load my resources.\nYou probably need to reinstall.",QMessageBox::Close);
		mb.exec();
	}
	return ret;
}

void MainWindow::movieMediaStateChanged(Phonon::State newstate,Phonon::State oldstate)
 {
	if(newstate == Phonon::PlayingState)
	{
		if(!previousPlayEstablished)
		{
			m_audioVideoMonitorThreadMutex.lock();
			m_moviePlayEstablished = true;
			m_audioVideoMonitorThreadMutex.unlock();
		}
	}
	// sorry to say, I could not find a common reaction between windows and linux after adding movie truncating
	// so I have to split on the build define
#ifdef LINUX_BUILD
	if((oldstate == Phonon::PlayingState || newstate == Phonon::ErrorState) && m_currentSceneType == GameScene::SCENE_MOVIE)
#endif
#ifdef WINDOWS_BUILD
	if((newstate == Phonon::PausedState || newstate == Phonon::ErrorState) && m_currentSceneType == GameScene::SCENE_MOVIE)
#endif
	{
		m_currentSceneType = GameScene::SCENE_ERROR;  // stop anymore updates to the movie
		signalAdvanceScenario();
		m_conversationLabel->setText("");
	}
}

void MainWindow::slideSprite()
{
	m_pixmapDisplayLabel->setPixmap(m_slideVector.at(m_currentVectorImage++));
	if(m_currentVectorImage == (int) m_slideVector.size())
	{
		m_spriteSlideTimer->stop();
		signalAdvanceScenario();
	}
}

bool MainWindow::loadBackgrounds(const string &dataDirectory)
{
	bool ret = true;
	string imageDirectory = dataDirectory;
	imageDirectory += "image/";
	for(int i = 0;(i < BACKGROUND_MAX) && ret;i++)
	{
		QPixmap pixmap;
		string currentFile = imageDirectory;
		bool found = true;
		switch(i)
		{
		case BACKGROUND_BAR:
			currentFile += "bar.png";
			if(!pixmap.load(currentFile.c_str()))
			{
				ret = false;
				fprintf(stderr,"error loading background: %s\n",currentFile.c_str());
			}
			break;
		case BACKGROUND_BEACH:
			currentFile += "beach.png";
			if(!pixmap.load(currentFile.c_str()))
			{
				ret = false;
				fprintf(stderr,"error loading background: %s\n",currentFile.c_str());
			}
			break;
		case BACKGROUND_BLACK:
			currentFile += "black.png";
			if(!pixmap.load(currentFile.c_str()))
			{
				ret = false;
				fprintf(stderr,"error loading background: %s\n",currentFile.c_str());
			}
			break;
		case BACKGROUND_BRIDGE:
			currentFile += "bridge.png";
			if(!pixmap.load(currentFile.c_str()))
			{
				ret = false;
				fprintf(stderr,"error loading background: %s\n",currentFile.c_str());
			}
			break;
		case BACKGROUND_CAVE:
			currentFile += "cave.png";
			if(!pixmap.load(currentFile.c_str()))
			{
				ret = false;
				fprintf(stderr,"error loading background: %s\n",currentFile.c_str());
			}
			break;
		case BACKGROUND_CEMETERY:
			currentFile += "cemetery.png";
			if(!pixmap.load(currentFile.c_str()))
			{
				ret = false;
				fprintf(stderr,"error loading background: %s\n",currentFile.c_str());
			}
			break;
		case BACKGROUND_CITY:
			currentFile += "city.png";
			if(!pixmap.load(currentFile.c_str()))
			{
				ret = false;
				fprintf(stderr,"error loading background: %s\n",currentFile.c_str());
			}
			break;
		case BACKGROUND_CLOUDS:
			currentFile += "clouds.png";
			if(!pixmap.load(currentFile.c_str()))
			{
				ret = false;
				fprintf(stderr,"error loading background: %s\n",currentFile.c_str());
			}
			break;
		case BACKGROUND_DESERT:
			currentFile += "desert.png";
			if(!pixmap.load(currentFile.c_str()))
			{
				ret = false;
				fprintf(stderr,"error loading background: %s\n",currentFile.c_str());
			}
			break;
		case BACKGROUND_DINER:
			currentFile += "diner.png";
			if(!pixmap.load(currentFile.c_str()))
			{
				ret = false;
				fprintf(stderr,"error loading background: %s\n",currentFile.c_str());
			}
			break;
		case BACKGROUND_DINER_INSIDE:
			currentFile += "diner_inside.png";
			if(!pixmap.load(currentFile.c_str()))
			{
				ret = false;
				fprintf(stderr,"error loading background: %s\n",currentFile.c_str());
			}
			break;
		case BACKGROUND_DON_OFFICE:
			currentFile += "don_office.png";
			if(!pixmap.load(currentFile.c_str()))
			{
				ret = false;
				fprintf(stderr,"error loading background: %s\n",currentFile.c_str());
			}
			break;
		case BACKGROUND_FORREST:
			currentFile += "forrest.png";
			if(!pixmap.load(currentFile.c_str()))
			{
				ret = false;
				fprintf(stderr,"error loading background: %s\n",currentFile.c_str());
			}
			break;
		case BACKGROUND_GAME_OVER:
			currentFile += "game_over.png";
			if(!pixmap.load(currentFile.c_str()))
			{
				ret = false;
				fprintf(stderr,"error loading background: %s\n",currentFile.c_str());
			}
			break;
		case BACKGROUND_HOTEL_BASEMENT:
			currentFile += "hotel_basement.png";
			if(!pixmap.load(currentFile.c_str()))
			{
				ret = false;
				fprintf(stderr,"error loading background: %s\n",currentFile.c_str());
			}
			break;
		case BACKGROUND_HOTEL_DINING_ROOM:
			currentFile += "hotel_dining_room.png";
			if(!pixmap.load(currentFile.c_str()))
			{
				ret = false;
				fprintf(stderr,"error loading background: %s\n",currentFile.c_str());
			}
			break;
		case BACKGROUND_INSIDE_TRAIN:
			currentFile += "inside_train.png";
			if(!pixmap.load(currentFile.c_str()))
			{
				ret = false;
				fprintf(stderr,"error loading background: %s\n",currentFile.c_str());
			}
			break;
		case BACKGROUND_INSIDE_TRAIN_2:
			currentFile += "inside_train_2.png";
			if(!pixmap.load(currentFile.c_str()))
			{
				ret = false;
				fprintf(stderr,"error loading background: %s\n",currentFile.c_str());
			}
			break;
		case BACKGROUND_JAIL:
			currentFile += "jail.png";
			if(!pixmap.load(currentFile.c_str()))
			{
				ret = false;
				fprintf(stderr,"error loading background: %s\n",currentFile.c_str());
			}
			break;
		case BACKGROUND_LEEDS_HOUSE:
			currentFile += "leeds_house.png";
			if(!pixmap.load(currentFile.c_str()))
			{
				ret = false;
				fprintf(stderr,"error loading background: %s\n",currentFile.c_str());
			}
			break;
		case BACKGROUND_LEEDS_INSIDE:
			currentFile += "leeds_inside.png";
			if(!pixmap.load(currentFile.c_str()))
			{
				ret = false;
				fprintf(stderr,"error loading background: %s\n",currentFile.c_str());
			}
			break;
		case BACKGROUND_LOBBY:
			currentFile += "lobby.png";
			if(!pixmap.load(currentFile.c_str()))
			{
				ret = false;
				fprintf(stderr,"error loading background: %s\n",currentFile.c_str());
			}
			break;
		case BACKGROUND_LONELY_GRAVE:
			currentFile += "lonely_grave.png";
			if(!pixmap.load(currentFile.c_str()))
			{
				ret = false;
				fprintf(stderr,"error loading background: %s\n",currentFile.c_str());
			}
			break;
		case BACKGROUND_MARY_HOTEL_ROOM:
			currentFile += "mary_hotel_room.png";
			if(!pixmap.load(currentFile.c_str()))
			{
				ret = false;
				fprintf(stderr,"error loading background: %s\n",currentFile.c_str());
			}
			break;
		case BACKGROUND_MONA_BEDROOM:
			currentFile += "mona_bedroom.png";
			if(!pixmap.load(currentFile.c_str()))
			{
				ret = false;
				fprintf(stderr,"error loading background: %s\n",currentFile.c_str());
			}
			break;
		case BACKGROUND_MONA_LOBBY:
			currentFile += "mona_lobby.png";
			if(!pixmap.load(currentFile.c_str()))
			{
				ret = false;
				fprintf(stderr,"error loading background: %s\n",currentFile.c_str());
			}
			break;
		case BACKGROUND_MONIQUE_BEDROOM:
			currentFile += "monique_bedroom.png";
			if(!pixmap.load(currentFile.c_str()))
			{
				ret = false;
				fprintf(stderr,"error loading background: %s\n",currentFile.c_str());
			}
			break;
		case BACKGROUND_MOON:
			currentFile += "moon.png";
			if(!pixmap.load(currentFile.c_str()))
			{
				ret = false;
				fprintf(stderr,"error loading background: %s\n",currentFile.c_str());
			}
			break;
		case BACKGROUND_NECTAR:
			currentFile += "nectar.png";
			if(!pixmap.load(currentFile.c_str()))
			{
				ret = false;
				fprintf(stderr,"error loading background: %s\n",currentFile.c_str());
			}
			break;
		case BACKGROUND_NORTH_STORM:
			currentFile += "north_storm.png";
			if(!pixmap.load(currentFile.c_str()))
			{
				ret = false;
				fprintf(stderr,"error loading background: %s\n",currentFile.c_str());
			}
			break;
		case BACKGROUND_OFFICE:
			currentFile += "office.png";
			if(!pixmap.load(currentFile.c_str()))
			{
				ret = false;
				fprintf(stderr,"error loading background: %s\n",currentFile.c_str());
			}
			break;
		case BACKGROUND_OLD_ROME:
			currentFile += "old_rome.png";
			if(!pixmap.load(currentFile.c_str()))
			{
				ret = false;
				fprintf(stderr,"error loading background: %s\n",currentFile.c_str());
			}
			break;
		case BACKGROUND_ORPHANAGE:
			currentFile += "orphanage.png";
			if(!pixmap.load(currentFile.c_str()))
			{
				ret = false;
				fprintf(stderr,"error loading background: %s\n",currentFile.c_str());
			}
			break;
		case BACKGROUND_OUR_TRAIN:
			currentFile += "our_train.png";
			if(!pixmap.load(currentFile.c_str()))
			{
				ret = false;
				fprintf(stderr,"error loading background: %s\n",currentFile.c_str());
			}
			break;
		case BACKGROUND_PLAINS:
			currentFile += "plains.png";
			if(!pixmap.load(currentFile.c_str()))
			{
				ret = false;
				fprintf(stderr,"error loading background: %s\n",currentFile.c_str());
			}
			break;
		case BACKGROUND_PRIMEVAL:
			currentFile += "primeval.png";
			if(!pixmap.load(currentFile.c_str()))
			{
				ret = false;
				fprintf(stderr,"error loading background: %s\n",currentFile.c_str());
			}
			break;
		case BACKGROUND_SPRING:
			currentFile += "spring.png";
			if(!pixmap.load(currentFile.c_str()))
			{
				ret = false;
				fprintf(stderr,"error loading background: %s\n",currentFile.c_str());
			}
			break;
		case BACKGROUND_STARS:
			currentFile += "stars.png";
			if(!pixmap.load(currentFile.c_str()))
			{
				ret = false;
				fprintf(stderr,"error loading background: %s\n",currentFile.c_str());
			}
			break;
		case BACKGROUND_STORM:
			currentFile += "storm.png";
			if(!pixmap.load(currentFile.c_str()))
			{
				ret = false;
				fprintf(stderr,"error loading background: %s\n",currentFile.c_str());
			}
			break;
		case BACKGROUND_SWAMP:
			currentFile += "swamp.png";
			if(!pixmap.load(currentFile.c_str()))
			{
				ret = false;
				fprintf(stderr,"error loading background: %s\n",currentFile.c_str());
			}
			break;
		case BACKGROUND_THALIA_STONE:
			currentFile += "thalia_stone.png";
			if(!pixmap.load(currentFile.c_str()))
			{
				ret = false;
				fprintf(stderr,"error loading background: %s\n",currentFile.c_str());
			}
			break;
		case BACKGROUND_TO_BE_CONTINUED:
			currentFile += "to_be_continued.png";
			if(!pixmap.load(currentFile.c_str()))
			{
				ret = false;
				fprintf(stderr,"error loading background: %s\n",currentFile.c_str());
			}
			break;
		case BACKGROUND_TRAIN_CAVE:
			currentFile += "train_cave.png";
			if(!pixmap.load(currentFile.c_str()))
			{
				ret = false;
				fprintf(stderr,"error loading background: %s\n",currentFile.c_str());
			}
			break;
		case BACKGROUND_TRAIN_STATION:
			currentFile += "train_station.png";
			if(!pixmap.load(currentFile.c_str()))
			{
				ret = false;
				fprintf(stderr,"error loading background: %s\n",currentFile.c_str());
			}
			break;
		case BACKGROUND_WINFIELD_ROOM:
			currentFile += "winfield_room.png";
			if(!pixmap.load(currentFile.c_str()))
			{
				ret = false;
				fprintf(stderr,"error loading background: %s\n",currentFile.c_str());
			}
			break;
		case BACKGROUND_WINTER:
			currentFile += "winter.png";
			if(!pixmap.load(currentFile.c_str()))
			{
				ret = false;
				fprintf(stderr,"error loading background: %s\n",currentFile.c_str());
			}
			break;
		default:
			found = false;
			ret = false;
			fprintf(stderr,"error unreferenced background: %d\n",i);
			break;
		}
		if(found)
			m_backgroundVector.push_back(currentFile);
	}
	return ret;
}

int MainWindow::getBackgroundOffsetFromText(const string &backgroundText)
{
	int ret = -1;
	if(backgroundText == "bar")
		ret = BACKGROUND_BAR;
	else if(backgroundText == "beach")
		ret = BACKGROUND_BEACH;
	else if(backgroundText == "black")
		ret = BACKGROUND_BLACK;
	else if(backgroundText == "bridge")
		ret = BACKGROUND_BRIDGE;
	else if(backgroundText == "cave")
		ret = BACKGROUND_CAVE;
	else if(backgroundText == "cemetery")
		ret = BACKGROUND_CEMETERY;
	else if(backgroundText == "city")
		ret = BACKGROUND_CITY;
	else if(backgroundText == "clouds")
		ret = BACKGROUND_CLOUDS;
	else if(backgroundText == "desert")
		ret = BACKGROUND_DESERT;
	else if(backgroundText == "diner")
		ret = BACKGROUND_DINER;
	else if(backgroundText == "diner_inside")
		ret = BACKGROUND_DINER_INSIDE;
	else if(backgroundText == "don_office")
		ret = BACKGROUND_DON_OFFICE;
	else if(backgroundText == "forrest")
		ret = BACKGROUND_FORREST;
	else if(backgroundText == "game_over")
		ret = BACKGROUND_GAME_OVER;
	else if(backgroundText == "hotel_basement")
		ret = BACKGROUND_HOTEL_BASEMENT;
	else if(backgroundText == "hotel_dining_room")
		ret = BACKGROUND_HOTEL_DINING_ROOM;
	else if(backgroundText == "inside_train")
		ret = BACKGROUND_INSIDE_TRAIN;
	else if(backgroundText == "inside_train_2")
		ret = BACKGROUND_INSIDE_TRAIN_2;
	else if(backgroundText == "jail")
		ret = BACKGROUND_JAIL;
	else if(backgroundText == "leeds_house")
		ret = BACKGROUND_LEEDS_HOUSE;
	else if(backgroundText == "leeds_inside")
		ret = BACKGROUND_LEEDS_INSIDE;
	else if(backgroundText == "lobby")
		ret = BACKGROUND_LOBBY;
	else if(backgroundText == "lonely_grave")
		ret = BACKGROUND_LONELY_GRAVE;
	else if(backgroundText == "mary_hotel_room")
		ret = BACKGROUND_MARY_HOTEL_ROOM;
	else if(backgroundText == "mona_bedroom")
		ret = BACKGROUND_MONA_BEDROOM;
	else if(backgroundText == "mona_lobby")
		ret = BACKGROUND_MONA_LOBBY;
	else if(backgroundText == "monique_bedroom")
		ret = BACKGROUND_MONIQUE_BEDROOM;
	else if(backgroundText == "moon")
		ret = BACKGROUND_MOON;
	else if(backgroundText == "nectar")
		ret = BACKGROUND_NECTAR;
	else if(backgroundText == "north_storm")
		ret = BACKGROUND_NORTH_STORM;
	else if(backgroundText == "office")
		ret = BACKGROUND_OFFICE;
	else if(backgroundText == "old_rome")
		ret = BACKGROUND_OLD_ROME;
	else if(backgroundText == "orphanage")
		ret = BACKGROUND_ORPHANAGE;
	else if(backgroundText == "our_train")
		ret = BACKGROUND_OUR_TRAIN;
	else if(backgroundText == "plains")
		ret = BACKGROUND_PLAINS;
	else if(backgroundText == "primeval")
		ret = BACKGROUND_PRIMEVAL;
	else if(backgroundText == "spring")
		ret = BACKGROUND_SPRING;
	else if(backgroundText == "stars")
		ret = BACKGROUND_STARS;
	else if(backgroundText == "storm")
		ret = BACKGROUND_STORM;
	else if(backgroundText == "swamp")
		ret = BACKGROUND_SWAMP;
	else if(backgroundText == "thalia_stone")
		ret = BACKGROUND_THALIA_STONE;
	else if(backgroundText == "to_be_continued")
		ret = BACKGROUND_TO_BE_CONTINUED;
	else if(backgroundText == "train_cave")
		ret = BACKGROUND_TRAIN_CAVE;
	else if(backgroundText == "train_station")
		ret = BACKGROUND_TRAIN_STATION;
	else if(backgroundText == "winfield_room")
		ret = BACKGROUND_WINFIELD_ROOM;
	else if(backgroundText == "winter")
		ret = BACKGROUND_WINTER;
	return ret;
}

bool MainWindow::getBackgroundTextFromOffset(int offset,string &text)
{
	bool ret = false;
	text = "";
	switch(offset)
	{
	case BACKGROUND_BAR:
		text = "bar";
		break;
	case BACKGROUND_BEACH:
		text = "beach";
		break;
	case BACKGROUND_BLACK:
		text = "black";
		break;
	case BACKGROUND_BRIDGE:
		text = "bridge";
		break;
	case BACKGROUND_CAVE:
		text = "cave";
		break;
	case BACKGROUND_CEMETERY:
		text = "cemetery";
		break;
	case BACKGROUND_CITY:
		text = "city";
		break;
	case BACKGROUND_CLOUDS:
		text = "clouds";
		break;
	case BACKGROUND_DESERT:
		text = "desert";
		break;
	case BACKGROUND_DINER:
		text = "diner";
		break;
	case BACKGROUND_DINER_INSIDE:
		text = "diner_inside";
		break;
	case BACKGROUND_DON_OFFICE:
		text = "don_office";
		break;
	case BACKGROUND_FORREST:
		text = "forrest";
		break;
	case BACKGROUND_GAME_OVER:
		text = "game_over";
		break;
	case BACKGROUND_HOTEL_BASEMENT:
		text = "hotel_basement";
		break;
	case BACKGROUND_HOTEL_DINING_ROOM:
		text = "hotel_dining_room";
		break;
	case BACKGROUND_INSIDE_TRAIN:
		text = "inside_train";
		break;
	case BACKGROUND_INSIDE_TRAIN_2:
		text = "inside_train_2";
		break;
	case BACKGROUND_JAIL:
		text = "jail";
		break;
	case BACKGROUND_LEEDS_HOUSE:
		text = "leeds_house";
		break;
	case BACKGROUND_LEEDS_INSIDE:
		text = "leeds_inside";
		break;
	case BACKGROUND_LOBBY:
		text = "lobby";
		break;
	case BACKGROUND_LONELY_GRAVE:
		text = "lonely_grave";
		break;
	case BACKGROUND_MARY_HOTEL_ROOM:
		text = "mary_hotel_room";
		break;
	case BACKGROUND_MONA_BEDROOM:
		text = "mona_bedroom";
		break;
	case BACKGROUND_MONA_LOBBY:
		text = "mona_lobby";
		break;
	case BACKGROUND_MONIQUE_BEDROOM:
		text = "monique_bedroom";
		break;
	case BACKGROUND_MOON:
		text = "moon";
		break;
	case BACKGROUND_NECTAR:
		text = "nectar";
		break;
	case BACKGROUND_NORTH_STORM:
		text = "north_storm";
		break;
	case BACKGROUND_OFFICE:
		text = "office";
		break;
	case BACKGROUND_OLD_ROME:
		text = "old_rome";
		break;
	case BACKGROUND_ORPHANAGE:
		text = "orphanage";
		break;
	case BACKGROUND_OUR_TRAIN:
		text = "our_train";
		break;
	case BACKGROUND_PLAINS:
		text = "plains";
		break;
	case BACKGROUND_PRIMEVAL:
		text = "primeval";
		break;
	case BACKGROUND_SPRING:
		text = "spring";
		break;
	case BACKGROUND_STARS:
		text = "stars";
		break;
	case BACKGROUND_STORM:
		text = "storm";
		break;
	case BACKGROUND_SWAMP:
		text = "swamp";
		break;
	case BACKGROUND_THALIA_STONE:
		text = "thalia_stone";
		break;
	case BACKGROUND_TO_BE_CONTINUED:
		text = "to_be_continued";
		break;
	case BACKGROUND_TRAIN_CAVE:
		text = "train_cave";
		break;
	case BACKGROUND_TRAIN_STATION:
		text = "train_station";
		break;
	case BACKGROUND_WINFIELD_ROOM:
		text = "winfield_room";
		break;
	case BACKGROUND_WINTER:
		text = "winter";
		break;
	default:
		break;
	}
	if(text.length())
		ret = true;
	return ret;
}

bool MainWindow::loadSprites(const string &dataDirectory)
{
	bool ret = true;
	string imageDirectory = dataDirectory;
	imageDirectory += "image/";
	for(int i = 0;(i < SPRITE_MAX) && ret;i++)
	{
		QPixmap pixmap;
		string currentFile = imageDirectory;
		bool found = true;
		switch(i)
		{
		case SPRITE_ALPO:
			currentFile += "alpo.png";
			if(!pixmap.load(currentFile.c_str()))
			{
				ret = false;
				fprintf(stderr,"error loading sprite: %s\n",currentFile.c_str());
			}
			break;
		case SPRITE_AXE_MAN_LEFT:
			currentFile += "axe_man_left.png";
			if(!pixmap.load(currentFile.c_str()))
			{
				ret = false;
				fprintf(stderr,"error loading sprite: %s\n",currentFile.c_str());
			}
			break;
		case SPRITE_AXE_MAN_MIDDLE:
			currentFile += "axe_man_middle.png";
			if(!pixmap.load(currentFile.c_str()))
			{
				ret = false;
				fprintf(stderr,"error loading sprite: %s\n",currentFile.c_str());
			}
			break;
		case SPRITE_AXE_MAN_RIGHT:
			currentFile += "axe_man_right.png";
			if(!pixmap.load(currentFile.c_str()))
			{
				ret = false;
				fprintf(stderr,"error loading sprite: %s\n",currentFile.c_str());
			}
			break;
		case SPRITE_BAMBI:
			currentFile += "bambi.png";
			if(!pixmap.load(currentFile.c_str()))
			{
				ret = false;
				fprintf(stderr,"error loading sprite: %s\n",currentFile.c_str());
			}
			break;
		case SPRITE_BAND:
			currentFile += "band.png";
			if(!pixmap.load(currentFile.c_str()))
			{
				ret = false;
				fprintf(stderr,"error loading sprite: %s\n",currentFile.c_str());
			}
			break;
		case SPRITE_BAR_PATRONS:
			currentFile += "bar_patrons.png";
			if(!pixmap.load(currentFile.c_str()))
			{
				ret = false;
				fprintf(stderr,"error loading sprite: %s\n",currentFile.c_str());
			}
			break;
		case SPRITE_BARTENDER:
			currentFile += "bartender.png";
			if(!pixmap.load(currentFile.c_str()))
			{
				ret = false;
				fprintf(stderr,"error loading sprite: %s\n",currentFile.c_str());
			}
			break;
		case SPRITE_BAR_TOUGH:
			currentFile += "bar_tough.png";
			if(!pixmap.load(currentFile.c_str()))
			{
				ret = false;
				fprintf(stderr,"error loading sprite: %s\n",currentFile.c_str());
			}
			break;
		case SPRITE_BASEBALL_BAT:
			currentFile += "baseball_bat.png";
			if(!pixmap.load(currentFile.c_str()))
			{
				ret = false;
				fprintf(stderr,"error loading sprite: %s\n",currentFile.c_str());
			}
			break;
		case SPRITE_BEER:
			currentFile += "beer.png";
			if(!pixmap.load(currentFile.c_str()))
			{
				ret = false;
				fprintf(stderr,"error loading sprite: %s\n",currentFile.c_str());
			}
			break;
		case SPRITE_BOARD:
			currentFile += "board.png";
			if(!pixmap.load(currentFile.c_str()))
			{
				ret = false;
				fprintf(stderr,"error loading sprite: %s\n",currentFile.c_str());
			}
			break;
		case SPRITE_BREAKFAST:
			currentFile += "breakfast.png";
			if(!pixmap.load(currentFile.c_str()))
			{
				ret = false;
				fprintf(stderr,"error loading sprite: %s\n",currentFile.c_str());
			}
			break;
		case SPRITE_BRUCE:
			currentFile += "bruce.png";
			if(!pixmap.load(currentFile.c_str()))
			{
				ret = false;
				fprintf(stderr,"error loading sprite: %s\n",currentFile.c_str());
			}
			break;
		case SPRITE_BUFFY:
			currentFile += "buffy.png";
			if(!pixmap.load(currentFile.c_str()))
			{
				ret = false;
				fprintf(stderr,"error loading sprite: %s\n",currentFile.c_str());
			}
			break;
		case SPRITE_BUNNY:
			currentFile += "bunny.png";
			if(!pixmap.load(currentFile.c_str()))
			{
				ret = false;
				fprintf(stderr,"error loading sprite: %s\n",currentFile.c_str());
			}
			break;
		case SPRITE_CAROLINE:
			currentFile += "caroline.png";
			if(!pixmap.load(currentFile.c_str()))
			{
				ret = false;
				fprintf(stderr,"error loading sprite: %s\n",currentFile.c_str());
			}
			break;
		case SPRITE_CARPET:
			currentFile += "carpet.png";
			if(!pixmap.load(currentFile.c_str()))
			{
				ret = false;
				fprintf(stderr,"error loading sprite: %s\n",currentFile.c_str());
			}
			break;
		case SPRITE_CAVE_JELLY:
			currentFile += "cave_jelly.png";
			if(!pixmap.load(currentFile.c_str()))
			{
				ret = false;
				fprintf(stderr,"error loading sprite: %s\n",currentFile.c_str());
			}
			break;
		case SPRITE_CHEF:
			currentFile += "chef.png";
			if(!pixmap.load(currentFile.c_str()))
			{
				ret = false;
				fprintf(stderr,"error loading sprite: %s\n",currentFile.c_str());
			}
			break;
		case SPRITE_CHIANTI:
			currentFile += "chianti.png";
			if(!pixmap.load(currentFile.c_str()))
			{
				ret = false;
				fprintf(stderr,"error loading sprite: %s\n",currentFile.c_str());
			}
			break;
		case SPRITE_CIRCE:
			currentFile += "circe.png";
			if(!pixmap.load(currentFile.c_str()))
			{
				ret = false;
				fprintf(stderr,"error loading sprite: %s\n",currentFile.c_str());
			}
			break;
		case SPRITE_CLOSED_BOX:
			currentFile += "closed_box.png";
			if(!pixmap.load(currentFile.c_str()))
			{
				ret = false;
				fprintf(stderr,"error loading sprite: %s\n",currentFile.c_str());
			}
			break;
		case SPRITE_COSTAIRS:
			currentFile += "costairs.png";
			if(!pixmap.load(currentFile.c_str()))
			{
				ret = false;
				fprintf(stderr,"error loading sprite: %s\n",currentFile.c_str());
			}
			break;
		case SPRITE_CRAB:
			currentFile += "crab.png";
			if(!pixmap.load(currentFile.c_str()))
			{
				ret = false;
				fprintf(stderr,"error loading sprite: %s\n",currentFile.c_str());
			}
			break;
		case SPRITE_DEAD_END:
			currentFile += "dead_end.png";
			if(!pixmap.load(currentFile.c_str()))
			{
				ret = false;
				fprintf(stderr,"error loading sprite: %s\n",currentFile.c_str());
			}
			break;
		case SPRITE_DEAD_MONIQUE:
			currentFile += "dead_monique.png";
			if(!pixmap.load(currentFile.c_str()))
			{
				ret = false;
				fprintf(stderr,"error loading sprite: %s\n",currentFile.c_str());
			}
			break;
		case SPRITE_DEAD_MONIQUE_DIM:
			currentFile += "dead_monique_dim.png";
			if(!pixmap.load(currentFile.c_str()))
			{
				ret = false;
				fprintf(stderr,"error loading sprite: %s\n",currentFile.c_str());
			}
			break;
		case SPRITE_DEAD_MONIQUE_MID:
			currentFile += "dead_monique_mid.png";
			if(!pixmap.load(currentFile.c_str()))
			{
				ret = false;
				fprintf(stderr,"error loading sprite: %s\n",currentFile.c_str());
			}
			break;
		case SPRITE_DON:
			currentFile += "don.png";
			if(!pixmap.load(currentFile.c_str()))
			{
				ret = false;
				fprintf(stderr,"error loading sprite: %s\n",currentFile.c_str());
			}
			break;
		case SPRITE_DUNCAN:
			currentFile += "duncan.png";
			if(!pixmap.load(currentFile.c_str()))
			{
				ret = false;
				fprintf(stderr,"error loading sprite: %s\n",currentFile.c_str());
			}
			break;
		case SPRITE_EAR:
			currentFile += "ear.png";
			if(!pixmap.load(currentFile.c_str()))
			{
				ret = false;
				fprintf(stderr,"error loading sprite: %s\n",currentFile.c_str());
			}
			break;
		case SPRITE_ELOPE_COUPLE:
			currentFile += "elope_couple.png";
			if(!pixmap.load(currentFile.c_str()))
			{
				ret = false;
				fprintf(stderr,"error loading sprite: %s\n",currentFile.c_str());
			}
			break;
		case SPRITE_ENRICO:
			currentFile += "enrico.png";
			if(!pixmap.load(currentFile.c_str()))
			{
				ret = false;
				fprintf(stderr,"error loading sprite: %s\n",currentFile.c_str());
			}
			break;
		case SPRITE_EVELYN:
			currentFile += "evelyn.png";
			if(!pixmap.load(currentFile.c_str()))
			{
				ret = false;
				fprintf(stderr,"error loading sprite: %s\n",currentFile.c_str());
			}
			break;
		case SPRITE_FEDERAL_MEN:
			currentFile += "federal_men.png";
			if(!pixmap.load(currentFile.c_str()))
			{
				ret = false;
				fprintf(stderr,"error loading sprite: %s\n",currentFile.c_str());
			}
			break;
		case SPRITE_FLABERSHAM:
			currentFile += "flabersham.png";
			if(!pixmap.load(currentFile.c_str()))
			{
				ret = false;
				fprintf(stderr,"error loading sprite: %s\n",currentFile.c_str());
			}
			break;
		case SPRITE_FLOCK:
			currentFile += "flock.png";
			if(!pixmap.load(currentFile.c_str()))
			{
				ret = false;
				fprintf(stderr,"error loading sprite: %s\n",currentFile.c_str());
			}
			break;
		case SPRITE_FONDUE:
			currentFile += "fondue.png";
			if(!pixmap.load(currentFile.c_str()))
			{
				ret = false;
				fprintf(stderr,"error loading sprite: %s\n",currentFile.c_str());
			}
			break;
		case SPRITE_FUNERAL_LEFT:
			currentFile += "funeral_left.png";
			if(!pixmap.load(currentFile.c_str()))
			{
				ret = false;
				fprintf(stderr,"error loading sprite: %s\n",currentFile.c_str());
			}
			break;
		case SPRITE_FUNERAL_RIGHT:
			currentFile += "funeral_right.png";
			if(!pixmap.load(currentFile.c_str()))
			{
				ret = false;
				fprintf(stderr,"error loading sprite: %s\n",currentFile.c_str());
			}
			break;
		case SPRITE_GIUSEPPE:
			currentFile += "giuseppe.png";
			if(!pixmap.load(currentFile.c_str()))
			{
				ret = false;
				fprintf(stderr,"error loading sprite: %s\n",currentFile.c_str());
			}
			break;
		case SPRITE_GLOVES:
			currentFile += "gloves.png";
			if(!pixmap.load(currentFile.c_str()))
			{
				ret = false;
				fprintf(stderr,"error loading sprite: %s\n",currentFile.c_str());
			}
			break;
		case SPRITE_GUIDO:
			currentFile += "guido.png";
			if(!pixmap.load(currentFile.c_str()))
			{
				ret = false;
				fprintf(stderr,"error loading sprite: %s\n",currentFile.c_str());
			}
			break;
		case SPRITE_GUN:
			currentFile += "gun.png";
			if(!pixmap.load(currentFile.c_str()))
			{
				ret = false;
				fprintf(stderr,"error loading sprite: %s\n",currentFile.c_str());
			}
			break;
		case SPRITE_HANDSHAKE:
			currentFile += "handshake.png";
			if(!pixmap.load(currentFile.c_str()))
			{
				ret = false;
				fprintf(stderr,"error loading sprite: %s\n",currentFile.c_str());
			}
			break;
		case SPRITE_HEART_OF_ALGEA:
			currentFile += "heart_of_algea.png";
			if(!pixmap.load(currentFile.c_str()))
			{
				ret = false;
				fprintf(stderr,"error loading sprite: %s\n",currentFile.c_str());
			}
			break;
		case SPRITE_HEMLOCK_LEFT:
			currentFile += "hemlock_left.png";
			if(!pixmap.load(currentFile.c_str()))
			{
				ret = false;
				fprintf(stderr,"error loading sprite: %s\n",currentFile.c_str());
			}
			break;
		case SPRITE_HEMLOCK_RIGHT:
			currentFile += "hemlock_right.png";
			if(!pixmap.load(currentFile.c_str()))
			{
				ret = false;
				fprintf(stderr,"error loading sprite: %s\n",currentFile.c_str());
			}
			break;
		case SPRITE_HOTEL_BELL:
			currentFile += "hotel_bell.png";
			if(!pixmap.load(currentFile.c_str()))
			{
				ret = false;
				fprintf(stderr,"error loading sprite: %s\n",currentFile.c_str());
			}
			break;
		case SPRITE_JACOB:
			currentFile += "jacob.png";
			if(!pixmap.load(currentFile.c_str()))
			{
				ret = false;
				fprintf(stderr,"error loading sprite: %s\n",currentFile.c_str());
			}
			break;
		case SPRITE_KILOWATSON:
			currentFile += "kilowatson.png";
			if(!pixmap.load(currentFile.c_str()))
			{
				ret = false;
				fprintf(stderr,"error loading sprite: %s\n",currentFile.c_str());
			}
			break;
		case SPRITE_LEEDS:
			currentFile += "leeds.png";
			if(!pixmap.load(currentFile.c_str()))
			{
				ret = false;
				fprintf(stderr,"error loading sprite: %s\n",currentFile.c_str());
			}
			break;
		case SPRITE_LIGHT_BULB:
			currentFile += "light_bulb.png";
			if(!pixmap.load(currentFile.c_str()))
			{
				ret = false;
				fprintf(stderr,"error loading sprite: %s\n",currentFile.c_str());
			}
			break;
		case SPRITE_LITTLE_MARY:
			currentFile += "little_mary.png";
			if(!pixmap.load(currentFile.c_str()))
			{
				ret = false;
				fprintf(stderr,"error loading sprite: %s\n",currentFile.c_str());
			}
			break;
		case SPRITE_LOU:
			currentFile += "lou.png";
			if(!pixmap.load(currentFile.c_str()))
			{
				ret = false;
				fprintf(stderr,"error loading sprite: %s\n",currentFile.c_str());
			}
			break;
		case SPRITE_LOU_GUN:
			currentFile += "lou_gun.png";
			if(!pixmap.load(currentFile.c_str()))
			{
				ret = false;
				fprintf(stderr,"error loading sprite: %s\n",currentFile.c_str());
			}
			break;
		case SPRITE_MARY:
			currentFile += "mary.png";
			if(!pixmap.load(currentFile.c_str()))
			{
				ret = false;
				fprintf(stderr,"error loading sprite: %s\n",currentFile.c_str());
			}
			break;
		case SPRITE_MARY_STANDING:
			currentFile += "mary_standing.png";
			if(!pixmap.load(currentFile.c_str()))
			{
				ret = false;
				fprintf(stderr,"error loading sprite: %s\n",currentFile.c_str());
			}
			break;
		case SPRITE_MCFABISH:
			currentFile += "mcfabish.png";
			if(!pixmap.load(currentFile.c_str()))
			{
				ret = false;
				fprintf(stderr,"error loading sprite: %s\n",currentFile.c_str());
			}
			break;
		case SPRITE_MEAL:
			currentFile += "meal.png";
			if(!pixmap.load(currentFile.c_str()))
			{
				ret = false;
				fprintf(stderr,"error loading sprite: %s\n",currentFile.c_str());
			}
			break;
		case SPRITE_MEAT_MEAL:
			currentFile += "meat_meal.png";
			if(!pixmap.load(currentFile.c_str()))
			{
				ret = false;
				fprintf(stderr,"error loading sprite: %s\n",currentFile.c_str());
			}
			break;
		case SPRITE_MONA_LEFT:
			currentFile += "mona_left.png";
			if(!pixmap.load(currentFile.c_str()))
			{
				ret = false;
				fprintf(stderr,"error loading sprite: %s\n",currentFile.c_str());
			}
			break;
		case SPRITE_MONA_RIGHT:
			currentFile += "mona_right.png";
			if(!pixmap.load(currentFile.c_str()))
			{
				ret = false;
				fprintf(stderr,"error loading sprite: %s\n",currentFile.c_str());
			}
			break;
		case SPRITE_MONEY:
			currentFile += "money.png";
			if(!pixmap.load(currentFile.c_str()))
			{
				ret = false;
				fprintf(stderr,"error loading sprite: %s\n",currentFile.c_str());
			}
			break;
		case SPRITE_MONICA:
			currentFile += "monica.png";
			if(!pixmap.load(currentFile.c_str()))
			{
				ret = false;
				fprintf(stderr,"error loading sprite: %s\n",currentFile.c_str());
			}
			break;
		case SPRITE_MONIQUE:
			currentFile += "monique.png";
			if(!pixmap.load(currentFile.c_str()))
			{
				ret = false;
				fprintf(stderr,"error loading sprite: %s\n",currentFile.c_str());
			}
			break;
		case SPRITE_NEWSPAPER:
			currentFile += "newspaper.png";
			if(!pixmap.load(currentFile.c_str()))
			{
				ret = false;
				fprintf(stderr,"error loading sprite: %s\n",currentFile.c_str());
			}
			break;
		case SPRITE_NOTE:
			currentFile += "note.png";
			if(!pixmap.load(currentFile.c_str()))
			{
				ret = false;
				fprintf(stderr,"error loading sprite: %s\n",currentFile.c_str());
			}
			break;
		case SPRITE_OPENED_BOX:
			currentFile += "opened_box.png";
			if(!pixmap.load(currentFile.c_str()))
			{
				ret = false;
				fprintf(stderr,"error loading sprite: %s\n",currentFile.c_str());
			}
			break;
		case SPRITE_PHONE:
			currentFile += "phone.png";
			if(!pixmap.load(currentFile.c_str()))
			{
				ret = false;
				fprintf(stderr,"error loading sprite: %s\n",currentFile.c_str());
			}
			break;
		case SPRITE_PHOTO:
			currentFile += "photo.png";
			if(!pixmap.load(currentFile.c_str()))
			{
				ret = false;
				fprintf(stderr,"error loading sprite: %s\n",currentFile.c_str());
			}
			break;
		case SPRITE_POLICEMAN:
			currentFile += "policeman.png";
			if(!pixmap.load(currentFile.c_str()))
			{
				ret = false;
				fprintf(stderr,"error loading sprite: %s\n",currentFile.c_str());
			}
			break;
		case SPRITE_POLICEWOMAN:
			currentFile += "policewoman.png";
			if(!pixmap.load(currentFile.c_str()))
			{
				ret = false;
				fprintf(stderr,"error loading sprite: %s\n",currentFile.c_str());
			}
			break;
		case SPRITE_POOL_TABLE:
			currentFile += "pool_table.png";
			if(!pixmap.load(currentFile.c_str()))
			{
				ret = false;
				fprintf(stderr,"error loading sprite: %s\n",currentFile.c_str());
			}
			break;
		case SPRITE_PORKCHOP:
			currentFile += "porkchop.png";
			if(!pixmap.load(currentFile.c_str()))
			{
				ret = false;
				fprintf(stderr,"error loading sprite: %s\n",currentFile.c_str());
			}
			break;
		case SPRITE_PORTER:
			currentFile += "porter.png";
			if(!pixmap.load(currentFile.c_str()))
			{
				ret = false;
				fprintf(stderr,"error loading sprite: %s\n",currentFile.c_str());
			}
			break;
		case SPRITE_PROFESSOR:
			currentFile += "professor.png";
			if(!pixmap.load(currentFile.c_str()))
			{
				ret = false;
				fprintf(stderr,"error loading sprite: %s\n",currentFile.c_str());
			}
			break;
		case SPRITE_RAVEN_ON_RED:
			currentFile += "raven_on_red.png";
			if(!pixmap.load(currentFile.c_str()))
			{
				ret = false;
				fprintf(stderr,"error loading sprite: %s\n",currentFile.c_str());
			}
			break;
		case SPRITE_ROBOT:
			currentFile += "robot.png";
			if(!pixmap.load(currentFile.c_str()))
			{
				ret = false;
				fprintf(stderr,"error loading sprite: %s\n",currentFile.c_str());
			}
			break;
		case SPRITE_ROCKET:
			currentFile += "rocket.png";
			if(!pixmap.load(currentFile.c_str()))
			{
				ret = false;
				fprintf(stderr,"error loading sprite: %s\n",currentFile.c_str());
			}
			break;
		case SPRITE_ROSE:
			currentFile += "rose.png";
			if(!pixmap.load(currentFile.c_str()))
			{
				ret = false;
				fprintf(stderr,"error loading sprite: %s\n",currentFile.c_str());
			}
			break;
		case SPRITE_SCALES:
			currentFile += "scales.png";
			if(!pixmap.load(currentFile.c_str()))
			{
				ret = false;
				fprintf(stderr,"error loading sprite: %s\n",currentFile.c_str());
			}
			break;
		case SPRITE_SCYTHE:
			currentFile += "scythe.png";
			if(!pixmap.load(currentFile.c_str()))
			{
				ret = false;
				fprintf(stderr,"error loading sprite: %s\n",currentFile.c_str());
			}
			break;
		case SPRITE_SECRETARY_1:
			currentFile += "secretary_1.png";
			if(!pixmap.load(currentFile.c_str()))
			{
				ret = false;
				fprintf(stderr,"error loading sprite: %s\n",currentFile.c_str());
			}
			break;
		case SPRITE_SECRETARY_2:
			currentFile += "secretary_2.png";
			if(!pixmap.load(currentFile.c_str()))
			{
				ret = false;
				fprintf(stderr,"error loading sprite: %s\n",currentFile.c_str());
			}
			break;
		case SPRITE_SEYMOUR:
			currentFile += "seymour.png";
			if(!pixmap.load(currentFile.c_str()))
			{
				ret = false;
				fprintf(stderr,"error loading sprite: %s\n",currentFile.c_str());
			}
			break;
		case SPRITE_SHERIFF:
			currentFile += "sheriff.png";
			if(!pixmap.load(currentFile.c_str()))
			{
				ret = false;
				fprintf(stderr,"error loading sprite: %s\n",currentFile.c_str());
			}
			break;
		case SPRITE_SNAKE:
			currentFile += "snake.png";
			if(!pixmap.load(currentFile.c_str()))
			{
				ret = false;
				fprintf(stderr,"error loading sprite: %s\n",currentFile.c_str());
			}
			break;
		case SPRITE_SOLDIER:
			currentFile += "soldier.png";
			if(!pixmap.load(currentFile.c_str()))
			{
				ret = false;
				fprintf(stderr,"error loading sprite: %s\n",currentFile.c_str());
			}
			break;
		case SPRITE_STOOL:
			currentFile += "stool.png";
			if(!pixmap.load(currentFile.c_str()))
			{
				ret = false;
				fprintf(stderr,"error loading sprite: %s\n",currentFile.c_str());
			}
			break;
		case SPRITE_TAB:
			currentFile += "tab.png";
			if(!pixmap.load(currentFile.c_str()))
			{
				ret = false;
				fprintf(stderr,"error loading sprite: %s\n",currentFile.c_str());
			}
			break;
		case SPRITE_TANYA_LEFT:
			currentFile += "tanya_left.png";
			if(!pixmap.load(currentFile.c_str()))
			{
				ret = false;
				fprintf(stderr,"error loading sprite: %s\n",currentFile.c_str());
			}
			break;
		case SPRITE_TANYA_RIGHT:
			currentFile += "tanya_right.png";
			if(!pixmap.load(currentFile.c_str()))
			{
				ret = false;
				fprintf(stderr,"error loading sprite: %s\n",currentFile.c_str());
			}
			break;
		case SPRITE_TIME_MACHINE:
			currentFile += "time_machine.png";
			if(!pixmap.load(currentFile.c_str()))
			{
				ret = false;
				fprintf(stderr,"error loading sprite: %s\n",currentFile.c_str());
			}
			break;
		case SPRITE_TONY:
			currentFile += "tony.png";
			if(!pixmap.load(currentFile.c_str()))
			{
				ret = false;
				fprintf(stderr,"error loading sprite: %s\n",currentFile.c_str());
			}
			break;
		case SPRITE_TRANSPARENT:
			currentFile += "transparent.png";
			if(!pixmap.load(currentFile.c_str()))
			{
				ret = false;
				fprintf(stderr,"error loading sprite: %s\n",currentFile.c_str());
			}
			break;
		case SPRITE_TREX:
			currentFile += "trex.png";
			if(!pixmap.load(currentFile.c_str()))
			{
				ret = false;
				fprintf(stderr,"error loading sprite: %s\n",currentFile.c_str());
			}
			break;
		case SPRITE_TUTORS_LEFT:
			currentFile += "tutors_left.png";
			if(!pixmap.load(currentFile.c_str()))
			{
				ret = false;
				fprintf(stderr,"error loading sprite: %s\n",currentFile.c_str());
			}
			break;
		case SPRITE_TUTORS_RIGHT:
			currentFile += "tutors_right.png";
			if(!pixmap.load(currentFile.c_str()))
			{
				ret = false;
				fprintf(stderr,"error loading sprite: %s\n",currentFile.c_str());
			}
			break;
		case SPRITE_UZI:
			currentFile += "uzi.png";
			if(!pixmap.load(currentFile.c_str()))
			{
				ret = false;
				fprintf(stderr,"error loading sprite: %s\n",currentFile.c_str());
			}
			break;
		case SPRITE_VEGETABLES:
			currentFile += "vegetables.png";
			if(!pixmap.load(currentFile.c_str()))
			{
				ret = false;
				fprintf(stderr,"error loading sprite: %s\n",currentFile.c_str());
			}
			break;
		case SPRITE_VERA:
			currentFile += "vera.png";
			if(!pixmap.load(currentFile.c_str()))
			{
				ret = false;
				fprintf(stderr,"error loading sprite: %s\n",currentFile.c_str());
			}
			break;
		case SPRITE_WATCHED:
			currentFile += "watched.png";
			if(!pixmap.load(currentFile.c_str()))
			{
				ret = false;
				fprintf(stderr,"error loading sprite: %s\n",currentFile.c_str());
			}
			break;
		case SPRITE_WINFIELD:
			currentFile += "winfield.png";
			if(!pixmap.load(currentFile.c_str()))
			{
				ret = false;
				fprintf(stderr,"error loading sprite: %s\n",currentFile.c_str());
			}
			break;
		case SPRITE_WOODEN_CHAIR:
			currentFile += "wooden_chair.png";
			if(!pixmap.load(currentFile.c_str()))
			{
				ret = false;
				fprintf(stderr,"error loading sprite: %s\n",currentFile.c_str());
			}
			break;
		default:
			found = false;
			ret = false;
			fprintf(stderr,"error unreferenced sprite: %d\n",i);
			break;
		}
		if(found)
			m_spriteVector.push_back(currentFile);
	}
	return ret;
}

int MainWindow::getSpriteOffsetFromText(const string &spriteName)
{
	int ret = -1;
	if(spriteName == "alpo")
		ret = SPRITE_ALPO;
	else if(spriteName == "axe_man_left")
		ret = SPRITE_AXE_MAN_LEFT;
	else if(spriteName == "axe_man_middle")
		ret = SPRITE_AXE_MAN_MIDDLE;
	else if(spriteName == "axe_man_right")
		ret = SPRITE_AXE_MAN_RIGHT;
	else if(spriteName == "bambi")
		ret = SPRITE_BAMBI;
	else if(spriteName == "band")
		ret = SPRITE_BAND;
	else if(spriteName == "bar_patrons")
		ret = SPRITE_BAR_PATRONS;
	else if(spriteName == "bartender")
		ret = SPRITE_BARTENDER;
	else if(spriteName == "bar_tough")
		ret = SPRITE_BAR_TOUGH;
	else if(spriteName == "baseball_bat")
		ret = SPRITE_BASEBALL_BAT;
	else if(spriteName == "beer")
		ret = SPRITE_BEER;
	else if(spriteName == "board")
		ret = SPRITE_BOARD;
	else if(spriteName == "breakfast")
		ret = SPRITE_BREAKFAST;
	else if(spriteName == "bruce")
		ret = SPRITE_BRUCE;
	else if(spriteName == "buffy")
		ret = SPRITE_BUFFY;
	else if(spriteName == "bunny")
		ret = SPRITE_BUNNY;
	else if(spriteName == "caroline")
		ret = SPRITE_CAROLINE;
	else if(spriteName == "carpet")
		ret = SPRITE_CARPET;
	else if(spriteName == "cave_jelly")
		ret = SPRITE_CAVE_JELLY;
	else if(spriteName == "chef")
		ret = SPRITE_CHEF;
	else if(spriteName == "chianti")
		ret = SPRITE_CHIANTI;
	else if(spriteName == "circe")
		ret = SPRITE_CIRCE;
	else if(spriteName == "closed_box")
		ret = SPRITE_CLOSED_BOX;
	else if(spriteName == "costairs")
		ret = SPRITE_COSTAIRS;
	else if(spriteName == "crab")
		ret = SPRITE_CRAB;
	else if(spriteName == "dead_end")
		ret = SPRITE_DEAD_END;
	else if(spriteName == "dead_monique")
		ret = SPRITE_DEAD_MONIQUE;
	else if(spriteName == "dead_monique_dim")
		ret = SPRITE_DEAD_MONIQUE_DIM;
	else if(spriteName == "dead_monique_mid")
		ret = SPRITE_DEAD_MONIQUE_MID;
	else if(spriteName == "don")
		ret = SPRITE_DON;
	else if(spriteName == "duncan")
		ret = SPRITE_DUNCAN;
	else if(spriteName == "ear")
		ret = SPRITE_EAR;
	else if(spriteName == "elope_couple")
		ret = SPRITE_ELOPE_COUPLE;
	else if(spriteName == "enrico")
		ret = SPRITE_ENRICO;
	else if(spriteName == "evelyn")
		ret = SPRITE_EVELYN;
	else if(spriteName == "federal_men")
		ret = SPRITE_FEDERAL_MEN;
	else if(spriteName == "flabersham")
		ret = SPRITE_FLABERSHAM;
	else if(spriteName == "flock")
		ret = SPRITE_FLOCK;
	else if(spriteName == "fondue")
		ret = SPRITE_FONDUE;
	else if(spriteName == "funeral_left")
		ret = SPRITE_FUNERAL_LEFT;
	else if(spriteName == "funeral_right")
		ret = SPRITE_FUNERAL_RIGHT;
	else if(spriteName == "giuseppe")
		ret = SPRITE_GIUSEPPE;
	else if(spriteName == "gloves")
		ret = SPRITE_GLOVES;
	else if(spriteName == "guido")
		ret = SPRITE_GUIDO;
	else if(spriteName == "gun")
		ret = SPRITE_GUN;
	else if(spriteName == "handshake")
		ret = SPRITE_HANDSHAKE;
	else if(spriteName == "heart_of_algea")
		ret = SPRITE_HEART_OF_ALGEA;
	else if(spriteName == "hemlock_left")
		ret = SPRITE_HEMLOCK_LEFT;
	else if(spriteName == "hemlock_right")
		ret = SPRITE_HEMLOCK_RIGHT;
	else if(spriteName == "hotel_bell")
		ret = SPRITE_HOTEL_BELL;
	else if(spriteName == "jacob")
		ret = SPRITE_JACOB;
	else if(spriteName == "kilowatson")
		ret = SPRITE_KILOWATSON;
	else if(spriteName == "leeds")
		ret = SPRITE_LEEDS;
	else if(spriteName == "light_bulb")
		ret = SPRITE_LIGHT_BULB;
	else if(spriteName == "little_mary")
		ret = SPRITE_LITTLE_MARY;
	else if(spriteName == "lou")
		ret = SPRITE_LOU;
	else if(spriteName == "lou_gun")
		ret = SPRITE_LOU_GUN;
	else if(spriteName == "mary")
		ret = SPRITE_MARY;
	else if(spriteName == "mary_standing")
		ret = SPRITE_MARY_STANDING;
	else if(spriteName == "mcfabish")
		ret = SPRITE_MCFABISH;
	else if(spriteName == "meal")
		ret = SPRITE_MEAL;
	else if(spriteName == "meat_meal")
		ret = SPRITE_MEAT_MEAL;
	else if(spriteName == "mona_left")
		ret = SPRITE_MONA_LEFT;
	else if(spriteName == "mona_right")
		ret = SPRITE_MONA_RIGHT;
	else if(spriteName == "money")
		ret = SPRITE_MONEY;
	else if(spriteName == "monica")
		ret = SPRITE_MONICA;
	else if(spriteName == "monique")
		ret = SPRITE_MONIQUE;
	else if(spriteName == "newspaper")
		ret = SPRITE_NEWSPAPER;
	else if(spriteName == "note")
		ret = SPRITE_NOTE;
	else if(spriteName == "opened_box")
		ret = SPRITE_OPENED_BOX;
	else if(spriteName == "phone")
		ret = SPRITE_PHONE;
	else if(spriteName == "photo")
		ret = SPRITE_PHOTO;
	else if(spriteName == "policeman")
		ret = SPRITE_POLICEMAN;
	else if(spriteName == "policewoman")
		ret = SPRITE_POLICEWOMAN;
	else if(spriteName == "pool_table")
		ret = SPRITE_POOL_TABLE;
	else if(spriteName == "porkchop")
		ret = SPRITE_PORKCHOP;
	else if(spriteName == "porter")
		ret = SPRITE_PORTER;
	else if(spriteName == "professor")
		ret = SPRITE_PROFESSOR;
	else if(spriteName == "raven_on_red")
		ret = SPRITE_RAVEN_ON_RED;
	else if(spriteName == "robot")
		ret = SPRITE_ROBOT;
	else if(spriteName == "rocket")
		ret = SPRITE_ROCKET;
	else if(spriteName == "rose")
		ret = SPRITE_ROSE;
	else if(spriteName == "scales")
		ret = SPRITE_SCALES;
	else if(spriteName == "scythe")
		ret = SPRITE_SCYTHE;
	else if(spriteName == "secretary_1")
		ret = SPRITE_SECRETARY_1;
	else if(spriteName == "secretary_2")
		ret = SPRITE_SECRETARY_2;
	else if(spriteName == "seymour")
		ret = SPRITE_SEYMOUR;
	else if(spriteName == "sheriff")
		ret = SPRITE_SHERIFF;
	else if(spriteName == "snake")
		ret = SPRITE_SNAKE;
	else if(spriteName == "soldier")
		ret = SPRITE_SOLDIER;
	else if(spriteName == "stool")
		ret = SPRITE_STOOL;
	else if(spriteName == "tab")
		ret = SPRITE_TAB;
	else if(spriteName == "tanya_left")
		ret = SPRITE_TANYA_LEFT;
	else if(spriteName == "tanya_right")
		ret = SPRITE_TANYA_RIGHT;
	else if(spriteName == "time_machine")
		ret = SPRITE_TIME_MACHINE;
	else if(spriteName == "tony")
		ret = SPRITE_TONY;
	else if(spriteName == "transparent")
		ret = SPRITE_TRANSPARENT;
	else if(spriteName == "trex")
		ret = SPRITE_TREX;
	else if(spriteName == "tutors_left")
		ret = SPRITE_TUTORS_LEFT;
	else if(spriteName == "tutors_right")
		ret = SPRITE_TUTORS_RIGHT;
	else if(spriteName == "uzi")
		ret = SPRITE_UZI;
	else if(spriteName == "vegetables")
		ret = SPRITE_VEGETABLES;
	else if(spriteName == "vera")
		ret = SPRITE_VERA;
	else if(spriteName == "watched")
		ret = SPRITE_WATCHED;
	else if(spriteName == "winfield")
		ret = SPRITE_WINFIELD;
	else if(spriteName == "wooden_chair")
		ret = SPRITE_WOODEN_CHAIR;
	return ret;
}

bool MainWindow::getSpriteTextFromOffset(int offset,string &text)
{
	bool ret = false;
	text = "";
	switch(offset)
	{
	case SPRITE_ALPO:
		text = "alpo";
		break;
	case SPRITE_AXE_MAN_LEFT:
		text = "axe_man_left";
		break;
	case SPRITE_AXE_MAN_MIDDLE:
		text = "axe_man_middle";
		break;
	case SPRITE_AXE_MAN_RIGHT:
		text = "axe_man_right";
		break;
	case SPRITE_BAMBI:
		text = "bambi";
		break;
	case SPRITE_BAND:
		text = "band";
		break;
	case SPRITE_BAR_PATRONS:
		text = "bar_patrons";
		break;
	case SPRITE_BARTENDER:
		text = "bartender";
		break;
	case SPRITE_BAR_TOUGH:
		text = "bar_tough";
		break;
	case SPRITE_BASEBALL_BAT:
		text = "baseball_bat";
		break;
	case SPRITE_BEER:
		text = "beer";
		break;
	case SPRITE_BOARD:
		text = "board";
		break;
	case SPRITE_BREAKFAST:
		text = "breakfast";
		break;
	case SPRITE_BRUCE:
		text = "bruce";
		break;
	case SPRITE_BUFFY:
		text = "buffy";
		break;
	case SPRITE_BUNNY:
		text = "bunny";
		break;
	case SPRITE_CAROLINE:
		text = "caroline";
		break;
	case SPRITE_CARPET:
		text = "carpet";
		break;
	case SPRITE_CAVE_JELLY:
		text = "cave_jelly";
		break;
	case SPRITE_CHEF:
		text = "chef";
		break;
	case SPRITE_CHIANTI:
		text = "chianti";
		break;
	case SPRITE_CIRCE:
		text = "circe";
		break;
	case SPRITE_CLOSED_BOX:
		text = "closed_box";
		break;
	case SPRITE_COSTAIRS:
		text = "costairs";
		break;
	case SPRITE_CRAB:
		text = "crab";
		break;
	case SPRITE_DEAD_END:
		text = "dead_end";
		break;
	case SPRITE_DEAD_MONIQUE:
		text = "dead_monique";
		break;
	case SPRITE_DEAD_MONIQUE_DIM:
		text = "dead_monique_dim";
		break;
	case SPRITE_DEAD_MONIQUE_MID:
		text = "dead_monique_mid";
		break;
	case SPRITE_DON:
		text = "don";
		break;
	case SPRITE_DUNCAN:
		text = "duncan";
		break;
	case SPRITE_EAR:
		text = "ear";
		break;
	case SPRITE_ELOPE_COUPLE:
		text = "elope_couple";
		break;
	case SPRITE_ENRICO:
		text = "enrico";
		break;
	case SPRITE_EVELYN:
		text = "evelyn";
		break;
	case SPRITE_FEDERAL_MEN:
		text = "federal_men";
		break;
	case SPRITE_FLABERSHAM:
		text = "flabersham";
		break;
	case SPRITE_FLOCK:
		text = "flock";
		break;
	case SPRITE_FONDUE:
		text = "fondue";
		break;
	case SPRITE_FUNERAL_LEFT:
		text = "funeral_left";
		break;
	case SPRITE_FUNERAL_RIGHT:
		text = "funeral_right";
		break;
	case SPRITE_GIUSEPPE:
		text = "giuseppe";
		break;
	case SPRITE_GLOVES:
		text = "gloves";
		break;
	case SPRITE_GUIDO:
		text = "guido";
		break;
	case SPRITE_GUN:
		text = "gun";
		break;
	case SPRITE_HANDSHAKE:
		text = "handshake";
		break;
	case SPRITE_HEART_OF_ALGEA:
		text = "heart_of_algea";
		break;
	case SPRITE_HEMLOCK_LEFT:
		text = "hemlock_left";
		break;
	case SPRITE_HEMLOCK_RIGHT:
		text = "hemlock_right";
		break;
	case SPRITE_HOTEL_BELL:
		text = "hotel_bell";
		break;
	case SPRITE_JACOB:
		text = "jacob";
		break;
	case SPRITE_KILOWATSON:
		text = "kilowatson";
		break;
	case SPRITE_LEEDS:
		text = "leeds";
		break;
	case SPRITE_LIGHT_BULB:
		text = "light_bulb";
		break;
	case SPRITE_LITTLE_MARY:
		text = "little_mary";
		break;
	case SPRITE_LOU:
		text = "lou";
		break;
	case SPRITE_LOU_GUN:
		text = "lou_gun";
		break;
	case SPRITE_MARY:
		text = "mary";
		break;
	case SPRITE_MARY_STANDING:
		text = "mary_standing";
		break;
	case SPRITE_MCFABISH:
		text = "mcfabish";
		break;
	case SPRITE_MEAL:
		text = "meal";
		break;
	case SPRITE_MEAT_MEAL:
		text = "meat_meal";
		break;
	case SPRITE_MONA_LEFT:
		text = "mona_left";
		break;
	case SPRITE_MONA_RIGHT:
		text = "mona_right";
		break;
	case SPRITE_MONEY:
		text = "money";
		break;
	case SPRITE_MONICA:
		text = "monica";
		break;
	case SPRITE_MONIQUE:
		text = "monique";
		break;
	case SPRITE_NEWSPAPER:
		text = "newspaper";
		break;
	case SPRITE_NOTE:
		text = "note";
		break;
	case SPRITE_OPENED_BOX:
		text = "opened_box";
		break;
	case SPRITE_PHONE:
		text = "phone";
		break;
	case SPRITE_PHOTO:
		text = "photo";
		break;
	case SPRITE_POLICEMAN:
		text = "policeman";
		break;
	case SPRITE_POLICEWOMAN:
		text = "policewoman";
		break;
	case SPRITE_POOL_TABLE:
		text = "pool_table";
		break;
	case SPRITE_PORKCHOP:
		text = "porkchop";
		break;
	case SPRITE_PORTER:
		text = "porter";
		break;
	case SPRITE_PROFESSOR:
		text = "professor";
		break;
	case SPRITE_RAVEN_ON_RED:
		text = "raven_on_red";
		break;
	case SPRITE_ROBOT:
		text = "robot";
		break;
	case SPRITE_ROCKET:
		text = "rocket";
		break;
	case SPRITE_ROSE:
		text = "rose";
		break;
	case SPRITE_SCALES:
		text = "scales";
		break;
	case SPRITE_SCYTHE:
		text = "scythe";
		break;
	case SPRITE_SECRETARY_1:
		text = "secretary_1";
		break;
	case SPRITE_SECRETARY_2:
		text = "secretary_2";
		break;
	case SPRITE_SEYMOUR:
		text = "seymour";
		break;
	case SPRITE_SHERIFF:
		text = "sheriff";
		break;
	case SPRITE_SNAKE:
		text = "snake";
		break;
	case SPRITE_SOLDIER:
		text = "soldier";
		break;
	case SPRITE_STOOL:
		text = "stool";
		break;
	case SPRITE_TAB:
		text = "tab";
		break;
	case SPRITE_TANYA_LEFT:
		text = "tanya_left";
		break;
	case SPRITE_TANYA_RIGHT:
		text = "tanya_right";
		break;
	case SPRITE_TIME_MACHINE:
		text = "time_machine";
		break;
	case SPRITE_TONY:
		text = "tony";
		break;
	case SPRITE_TRANSPARENT:
		text = "transparent";
		break;
	case SPRITE_TREX:
		text = "trex";
		break;
	case SPRITE_TUTORS_LEFT:
		text = "tutors_left";
		break;
	case SPRITE_TUTORS_RIGHT:
		text = "tutors_right";
		break;
	case SPRITE_UZI:
		text = "uzi";
		break;
	case SPRITE_VEGETABLES:
		text = "vegetables";
		break;
	case SPRITE_VERA:
		text = "vera";
		break;
	case SPRITE_WATCHED:
		text = "watched";
		break;
	case SPRITE_WINFIELD:
		text = "winfield";
		break;
	case SPRITE_WOODEN_CHAIR:
		text = "wooden_chair";
		break;
	default:
		break;
	}
	if(text.length())
		ret = true;
	return ret;
}

bool MainWindow::loadScenarios(const string &dataDirectory)
{
	bool ret = true;
	string textDirectory = dataDirectory;
	textDirectory += "text/";
	for(int i = 0;(i < SCENARIO_MAX) && ret;i++)
	{
		GameScenario gameScenario;
		string currentFile = textDirectory;
		switch(i)
		{
		case SCENARIO_START:
			currentFile += "scenario.start.xml";
			if(parseScenario(currentFile,gameScenario))
				m_gameScenarioVector.push_back(gameScenario);
			else
			{
				ret = false;
				fprintf(stderr,"error loading scenario: %s\n",currentFile.c_str());
			}
			break;
		case SCENARIO_DRINK_AT_SAMS_AFTER_MURDER:
			currentFile += "scenario.drink_at_sams_after_murder.xml";
			if(parseScenario(currentFile,gameScenario))
				m_gameScenarioVector.push_back(gameScenario);
			else
			{
				ret = false;
				fprintf(stderr,"error loading scenario: %s\n",currentFile.c_str());
			}
			break;
		case SCENARIO_TELL_MARY_AFTER_MURDER:
			currentFile += "scenario.tell_mary_after_murder.xml";
			if(parseScenario(currentFile,gameScenario))
				m_gameScenarioVector.push_back(gameScenario);
			else
			{
				ret = false;
				fprintf(stderr,"error loading scenario: %s\n",currentFile.c_str());
			}
			break;
		case SCENARIO_QUIT_AFTER_TONY:
			currentFile += "scenario.quit_after_tony.xml";
			if(parseScenario(currentFile,gameScenario))
				m_gameScenarioVector.push_back(gameScenario);
			else
			{
				ret = false;
				fprintf(stderr,"error loading scenario: %s\n",currentFile.c_str());
			}
			break;
		case SCENARIO_SEARCH_HOUSE_AFTER_TONY:
			currentFile += "scenario.search_house_after_tony.xml";
			if(parseScenario(currentFile,gameScenario))
				m_gameScenarioVector.push_back(gameScenario);
			else
			{
				ret = false;
				fprintf(stderr,"error loading scenario: %s\n",currentFile.c_str());
			}
			break;
		case SCENARIO_CRUSTACEAN_COVE:
			currentFile += "scenario.crustacean_cove.xml";
			if(parseScenario(currentFile,gameScenario))
				m_gameScenarioVector.push_back(gameScenario);
			else
			{
				ret = false;
				fprintf(stderr,"error loading scenario: %s\n",currentFile.c_str());
			}
			break;
		case SCENARIO_HAUNTED_CAVES:
			currentFile += "scenario.haunted_caves.xml";
			if(parseScenario(currentFile,gameScenario))
				m_gameScenarioVector.push_back(gameScenario);
			else
			{
				ret = false;
				fprintf(stderr,"error loading scenario: %s\n",currentFile.c_str());
			}
			break;
		case SCENARIO_LOU_DIAMOND:
			currentFile += "scenario.lou_diamond.xml";
			if(parseScenario(currentFile,gameScenario))
				m_gameScenarioVector.push_back(gameScenario);
			else
			{
				ret = false;
				fprintf(stderr,"error loading scenario: %s\n",currentFile.c_str());
			}
			break;
		case SCENARIO_CRUSTACEAN_COVE_CHOICES:
			currentFile += "scenario.crustacean_cove_choices.xml";
			if(parseScenario(currentFile,gameScenario))
				m_gameScenarioVector.push_back(gameScenario);
			else
			{
				ret = false;
				fprintf(stderr,"error loading scenario: %s\n",currentFile.c_str());
			}
			break;
		case SCENARIO_BAR_QUIT:
			currentFile += "scenario.bar_quit.xml";
			if(parseScenario(currentFile,gameScenario))
				m_gameScenarioVector.push_back(gameScenario);
			else
			{
				ret = false;
				fprintf(stderr,"error loading scenario: %s\n",currentFile.c_str());
			}
			break;
		case SCENARIO_BRUCE:
			currentFile += "scenario.bruce.xml";
			if(parseScenario(currentFile,gameScenario))
				m_gameScenarioVector.push_back(gameScenario);
			else
			{
				ret = false;
				fprintf(stderr,"error loading scenario: %s\n",currentFile.c_str());
			}
			break;
		case SCENARIO_QUESTION_DON:
			currentFile += "scenario.question_don.xml";
			if(parseScenario(currentFile,gameScenario))
				m_gameScenarioVector.push_back(gameScenario);
			else
			{
				ret = false;
				fprintf(stderr,"error loading scenario: %s\n",currentFile.c_str());
			}
			break;
		case SCENARIO_CRUSTACEAN_COVE_CONTINUE:
			currentFile += "scenario.crustacean_cove_continue.xml";
			if(parseScenario(currentFile,gameScenario))
				m_gameScenarioVector.push_back(gameScenario);
			else
			{
				ret = false;
				fprintf(stderr,"error loading scenario: %s\n",currentFile.c_str());
			}
			break;
		case SCENARIO_CRUSTACEAN_COVE_KILLER_CHOICES:
			currentFile += "scenario.crustacean_cove_killer_choices.xml";
			if(parseScenario(currentFile,gameScenario))
				m_gameScenarioVector.push_back(gameScenario);
			else
			{
				ret = false;
				fprintf(stderr,"error loading scenario: %s\n",currentFile.c_str());
			}
			break;
		case SCENARIO_CRUSTACEAN_COVE_ENRICO_KILLER:
			currentFile += "scenario.crustacean_cove_enrico_killer.xml";
			if(parseScenario(currentFile,gameScenario))
				m_gameScenarioVector.push_back(gameScenario);
			else
			{
				ret = false;
				fprintf(stderr,"error loading scenario: %s\n",currentFile.c_str());
			}
			break;
		case SCENARIO_CRUSTACEAN_COVE_GIUSEPPE_KILLER:
			currentFile += "scenario.crustacean_cove_giuseppe_killer.xml";
			if(parseScenario(currentFile,gameScenario))
				m_gameScenarioVector.push_back(gameScenario);
			else
			{
				ret = false;
				fprintf(stderr,"error loading scenario: %s\n",currentFile.c_str());
			}
			break;
		case SCENARIO_CRUSTACEAN_COVE_BRUCE_KILLER:
			currentFile += "scenario.crustacean_cove_bruce_killer.xml";
			if(parseScenario(currentFile,gameScenario))
				m_gameScenarioVector.push_back(gameScenario);
			else
			{
				ret = false;
				fprintf(stderr,"error loading scenario: %s\n",currentFile.c_str());
			}
			break;
		case SCENARIO_CRUSTACEAN_COVE_EVELYN_KILLER:
			currentFile += "scenario.crustacean_cove_evelyn_killer.xml";
			if(parseScenario(currentFile,gameScenario))
				m_gameScenarioVector.push_back(gameScenario);
			else
			{
				ret = false;
				fprintf(stderr,"error loading scenario: %s\n",currentFile.c_str());
			}
			break;
		case SCENARIO_CRUSTACEAN_COVE_DON_KILLER:
			currentFile += "scenario.crustacean_cove_don_killer.xml";
			if(parseScenario(currentFile,gameScenario))
				m_gameScenarioVector.push_back(gameScenario);
			else
			{
				ret = false;
				fprintf(stderr,"error loading scenario: %s\n",currentFile.c_str());
			}
			break;
		case SCENARIO_CRUSTACEAN_COVE_LOU_KILLER:
			currentFile += "scenario.crustacean_cove_lou_killer.xml";
			if(parseScenario(currentFile,gameScenario))
				m_gameScenarioVector.push_back(gameScenario);
			else
			{
				ret = false;
				fprintf(stderr,"error loading scenario: %s\n",currentFile.c_str());
			}
			break;
		case SCENARIO_CRUSTACEAN_COVE_GUIDO_KILLER:
			currentFile += "scenario.crustacean_cove_guido_killer.xml";
			if(parseScenario(currentFile,gameScenario))
				m_gameScenarioVector.push_back(gameScenario);
			else
			{
				ret = false;
				fprintf(stderr,"error loading scenario: %s\n",currentFile.c_str());
			}
			break;
		case SCENARIO_OHANNITY_AFTER_CRUSTACEAN_COVE:
			currentFile += "scenario.ohannity_after_crustacean_cove.xml";
			if(parseScenario(currentFile,gameScenario))
				m_gameScenarioVector.push_back(gameScenario);
			else
			{
				ret = false;
				fprintf(stderr,"error loading scenario: %s\n",currentFile.c_str());
			}
			break;
		case SCENARIO_MONA:
			currentFile += "scenario.mona.xml";
			if(parseScenario(currentFile,gameScenario))
				m_gameScenarioVector.push_back(gameScenario);
			else
			{
				ret = false;
				fprintf(stderr,"error loading scenario: %s\n",currentFile.c_str());
			}
			break;
		case SCENARIO_MONA_CHOOSE:
			currentFile += "scenario.mona_choose.xml";
			if(parseScenario(currentFile,gameScenario))
				m_gameScenarioVector.push_back(gameScenario);
			else
			{
				ret = false;
				fprintf(stderr,"error loading scenario: %s\n",currentFile.c_str());
			}
			break;
		case SCENARIO_MONA_CONTINUE:
			currentFile += "scenario.mona_continue.xml";
			if(parseScenario(currentFile,gameScenario))
				m_gameScenarioVector.push_back(gameScenario);
			else
			{
				ret = false;
				fprintf(stderr,"error loading scenario: %s\n",currentFile.c_str());
			}
			break;
		case SCENARIO_TRAIN_SUSPECT_CHOOSE:
			currentFile += "scenario.train_suspect_choose.xml";
			if(parseScenario(currentFile,gameScenario))
				m_gameScenarioVector.push_back(gameScenario);
			else
			{
				ret = false;
				fprintf(stderr,"error loading scenario: %s\n",currentFile.c_str());
			}
			break;
		case SCENARIO_TRAIN_CAVE:
			currentFile += "scenario.train_cave.xml";
			if(parseScenario(currentFile,gameScenario))
				m_gameScenarioVector.push_back(gameScenario);
			else
			{
				ret = false;
				fprintf(stderr,"error loading scenario: %s\n",currentFile.c_str());
			}
			break;
		case SCENARIO_TRAIN_JACOB:
			currentFile += "scenario.train_jacob.xml";
			if(parseScenario(currentFile,gameScenario))
				m_gameScenarioVector.push_back(gameScenario);
			else
			{
				ret = false;
				fprintf(stderr,"error loading scenario: %s\n",currentFile.c_str());
			}
			break;
		case SCENARIO_COLLAGE_GIRLS:
			currentFile += "scenario.collage_girls.xml";
			if(parseScenario(currentFile,gameScenario))
				m_gameScenarioVector.push_back(gameScenario);
			else
			{
				ret = false;
				fprintf(stderr,"error loading scenario: %s\n",currentFile.c_str());
			}
			break;
		case SCENARIO_MONICA:
			currentFile += "scenario.monica.xml";
			if(parseScenario(currentFile,gameScenario))
				m_gameScenarioVector.push_back(gameScenario);
			else
			{
				ret = false;
				fprintf(stderr,"error loading scenario: %s\n",currentFile.c_str());
			}
			break;
		case SCENARIO_VERA:
			currentFile += "scenario.vera.xml";
			if(parseScenario(currentFile,gameScenario))
				m_gameScenarioVector.push_back(gameScenario);
			else
			{
				ret = false;
				fprintf(stderr,"error loading scenario: %s\n",currentFile.c_str());
			}
			break;
		case SCENARIO_SCHOMES_KILOWATSON:
			currentFile += "scenario.schomes_kilowatson.xml";
			if(parseScenario(currentFile,gameScenario))
				m_gameScenarioVector.push_back(gameScenario);
			else
			{
				ret = false;
				fprintf(stderr,"error loading scenario: %s\n",currentFile.c_str());
			}
			break;
		case SCENARIO_DUNCAN:
			currentFile += "scenario.duncan.xml";
			if(parseScenario(currentFile,gameScenario))
				m_gameScenarioVector.push_back(gameScenario);
			else
			{
				ret = false;
				fprintf(stderr,"error loading scenario: %s\n",currentFile.c_str());
			}
			break;
		case SCENARIO_KILBANE:
			currentFile += "scenario.kilbane.xml";
			if(parseScenario(currentFile,gameScenario))
				m_gameScenarioVector.push_back(gameScenario);
			else
			{
				ret = false;
				fprintf(stderr,"error loading scenario: %s\n",currentFile.c_str());
			}
			break;
		case SCENARIO_MCFABISH:
			currentFile += "scenario.mcfabish.xml";
			if(parseScenario(currentFile,gameScenario))
				m_gameScenarioVector.push_back(gameScenario);
			else
			{
				ret = false;
				fprintf(stderr,"error loading scenario: %s\n",currentFile.c_str());
			}
			break;
		case SCENARIO_LADY_MARGARET:
			currentFile += "scenario.lady_margaret.xml";
			if(parseScenario(currentFile,gameScenario))
				m_gameScenarioVector.push_back(gameScenario);
			else
			{
				ret = false;
				fprintf(stderr,"error loading scenario: %s\n",currentFile.c_str());
			}
			break;
		case SCENARIO_TRAIN_CONTINUE:
			currentFile += "scenario.train_continue.xml";
			if(parseScenario(currentFile,gameScenario))
				m_gameScenarioVector.push_back(gameScenario);
			else
			{
				ret = false;
				fprintf(stderr,"error loading scenario: %s\n",currentFile.c_str());
			}
			break;
		case SCENARIO_TRAIN_GUILTY_CHOOSE:
			currentFile += "scenario.train_guilty_choose.xml";
			if(parseScenario(currentFile,gameScenario))
				m_gameScenarioVector.push_back(gameScenario);
			else
			{
				ret = false;
				fprintf(stderr,"error loading scenario: %s\n",currentFile.c_str());
			}
			break;
		case SCENARIO_HEMLOCK_GUILTY:
			currentFile += "scenario.hemlock_guilty.xml";
			if(parseScenario(currentFile,gameScenario))
				m_gameScenarioVector.push_back(gameScenario);
			else
			{
				ret = false;
				fprintf(stderr,"error loading scenario: %s\n",currentFile.c_str());
			}
			break;
		case SCENARIO_KILOWATSON_GUILTY:
			currentFile += "scenario.kilowatson_guilty.xml";
			if(parseScenario(currentFile,gameScenario))
				m_gameScenarioVector.push_back(gameScenario);
			else
			{
				ret = false;
				fprintf(stderr,"error loading scenario: %s\n",currentFile.c_str());
			}
			break;
		case SCENARIO_COLLAGE_GIRLS_GUILTY:
			currentFile += "scenario.collage_girls_guilty.xml";
			if(parseScenario(currentFile,gameScenario))
				m_gameScenarioVector.push_back(gameScenario);
			else
			{
				ret = false;
				fprintf(stderr,"error loading scenario: %s\n",currentFile.c_str());
			}
			break;
		case SCENARIO_DUNCAN_GUILTY:
			currentFile += "scenario.duncan_guilty.xml";
			if(parseScenario(currentFile,gameScenario))
				m_gameScenarioVector.push_back(gameScenario);
			else
			{
				ret = false;
				fprintf(stderr,"error loading scenario: %s\n",currentFile.c_str());
			}
			break;
		case SCENARIO_MARGARET_GUILTY:
			currentFile += "scenario.margaret_guilty.xml";
			if(parseScenario(currentFile,gameScenario))
				m_gameScenarioVector.push_back(gameScenario);
			else
			{
				ret = false;
				fprintf(stderr,"error loading scenario: %s\n",currentFile.c_str());
			}
			break;
		case SCENARIO_MCFABISH_GUILTY:
			currentFile += "scenario.mcfabish_guilty.xml";
			if(parseScenario(currentFile,gameScenario))
				m_gameScenarioVector.push_back(gameScenario);
			else
			{
				ret = false;
				fprintf(stderr,"error loading scenario: %s\n",currentFile.c_str());
			}
			break;
		case SCENARIO_MONICA_GUILTY:
			currentFile += "scenario.monica_guilty.xml";
			if(parseScenario(currentFile,gameScenario))
				m_gameScenarioVector.push_back(gameScenario);
			else
			{
				ret = false;
				fprintf(stderr,"error loading scenario: %s\n",currentFile.c_str());
			}
			break;
		case SCENARIO_VERA_GUILTY:
			currentFile += "scenario.vera_guilty.xml";
			if(parseScenario(currentFile,gameScenario))
				m_gameScenarioVector.push_back(gameScenario);
			else
			{
				ret = false;
				fprintf(stderr,"error loading scenario: %s\n",currentFile.c_str());
			}
			break;
		case SCENARIO_KILBANE_GUILTY:
			currentFile += "scenario.kilbane_guilty.xml";
			if(parseScenario(currentFile,gameScenario))
				m_gameScenarioVector.push_back(gameScenario);
			else
			{
				ret = false;
				fprintf(stderr,"error loading scenario: %s\n",currentFile.c_str());
			}
			break;
		case SCENARIO_JACOB_GUILTY:
			currentFile += "scenario.jacob_guilty.xml";
			if(parseScenario(currentFile,gameScenario))
				m_gameScenarioVector.push_back(gameScenario);
			else
			{
				ret = false;
				fprintf(stderr,"error loading scenario: %s\n",currentFile.c_str());
			}
			break;
		case SCENARIO_PORTER_GUILTY:
			currentFile += "scenario.porter_guilty.xml";
			if(parseScenario(currentFile,gameScenario))
				m_gameScenarioVector.push_back(gameScenario);
			else
			{
				ret = false;
				fprintf(stderr,"error loading scenario: %s\n",currentFile.c_str());
			}
			break;
		case SCENARIO_DIRK_TRIVIA:
			currentFile += "scenario.dirk_trivia.xml";
			if(parseScenario(currentFile,gameScenario))
				m_gameScenarioVector.push_back(gameScenario);
			else
			{
				ret = false;
				fprintf(stderr,"error loading scenario: %s\n",currentFile.c_str());
			}
			break;
		case SCENARIO_LEEDS_OFFICE_TRIVIA:
			currentFile += "scenario.leeds_office_trivia.xml";
			if(parseScenario(currentFile,gameScenario))
				m_gameScenarioVector.push_back(gameScenario);
			else
			{
				ret = false;
				fprintf(stderr,"error loading scenario: %s\n",currentFile.c_str());
			}
			break;
		case SCENARIO_LEED_TRIVIA:
			currentFile += "scenario.leed_trivia.xml";
			if(parseScenario(currentFile,gameScenario))
				m_gameScenarioVector.push_back(gameScenario);
			else
			{
				ret = false;
				fprintf(stderr,"error loading scenario: %s\n",currentFile.c_str());
			}
			break;
		case SCENARIO_FUNERAL_TRIVIA:
			currentFile += "scenario.funeral_trivia.xml";
			if(parseScenario(currentFile,gameScenario))
				m_gameScenarioVector.push_back(gameScenario);
			else
			{
				ret = false;
				fprintf(stderr,"error loading scenario: %s\n",currentFile.c_str());
			}
			break;
		case SCENARIO_DON_TRIVIA:
			currentFile += "scenario.don_trivia.xml";
			if(parseScenario(currentFile,gameScenario))
				m_gameScenarioVector.push_back(gameScenario);
			else
			{
				ret = false;
				fprintf(stderr,"error loading scenario: %s\n",currentFile.c_str());
			}
			break;
		case SCENARIO_TRAIN_BABY_TRIVIA:
			currentFile += "scenario.train_baby_trivia.xml";
			if(parseScenario(currentFile,gameScenario))
				m_gameScenarioVector.push_back(gameScenario);
			else
			{
				ret = false;
				fprintf(stderr,"error loading scenario: %s\n",currentFile.c_str());
			}
			break;
		case SCENARIO_LOU_TRIVIA:
			currentFile += "scenario.lou_trivia.xml";
			if(parseScenario(currentFile,gameScenario))
				m_gameScenarioVector.push_back(gameScenario);
			else
			{
				ret = false;
				fprintf(stderr,"error loading scenario: %s\n",currentFile.c_str());
			}
			break;
		case SCENARIO_MONEY_TRIVIA:
			currentFile += "scenario.money_trivia.xml";
			if(parseScenario(currentFile,gameScenario))
				m_gameScenarioVector.push_back(gameScenario);
			else
			{
				ret = false;
				fprintf(stderr,"error loading scenario: %s\n",currentFile.c_str());
			}
			break;
		case SCENARIO_SHERIFF_TRIVIA:
			currentFile += "scenario.sheriff_trivia.xml";
			if(parseScenario(currentFile,gameScenario))
				m_gameScenarioVector.push_back(gameScenario);
			else
			{
				ret = false;
				fprintf(stderr,"error loading scenario: %s\n",currentFile.c_str());
			}
			break;
		case SCENARIO_CIRCE_TRIVIA:
			currentFile += "scenario.circe_trivia.xml";
			if(parseScenario(currentFile,gameScenario))
				m_gameScenarioVector.push_back(gameScenario);
			else
			{
				ret = false;
				fprintf(stderr,"error loading scenario: %s\n",currentFile.c_str());
			}
			break;
		case SCENARIO_TERROR_TRIVIA:
			currentFile += "scenario.terror_trivia.xml";
			if(parseScenario(currentFile,gameScenario))
				m_gameScenarioVector.push_back(gameScenario);
			else
			{
				ret = false;
				fprintf(stderr,"error loading scenario: %s\n",currentFile.c_str());
			}
			break;
		case SCENARIO_VEAL_TRIVIA:
			currentFile += "scenario.veal_trivia.xml";
			if(parseScenario(currentFile,gameScenario))
				m_gameScenarioVector.push_back(gameScenario);
			else
			{
				ret = false;
				fprintf(stderr,"error loading scenario: %s\n",currentFile.c_str());
			}
			break;
		case SCENARIO_MARGARET_TRIVIA:
			currentFile += "scenario.margaret_trivia.xml";
			if(parseScenario(currentFile,gameScenario))
				m_gameScenarioVector.push_back(gameScenario);
			else
			{
				ret = false;
				fprintf(stderr,"error loading scenario: %s\n",currentFile.c_str());
			}
			break;
		case SCENARIO_BUNNY_TRIVIA:
			currentFile += "scenario.bunny_trivia.xml";
			if(parseScenario(currentFile,gameScenario))
				m_gameScenarioVector.push_back(gameScenario);
			else
			{
				ret = false;
				fprintf(stderr,"error loading scenario: %s\n",currentFile.c_str());
			}
			break;
		case SCENARIO_BRUCE_TRIVIA:
			currentFile += "scenario.bruce_trivia.xml";
			if(parseScenario(currentFile,gameScenario))
				m_gameScenarioVector.push_back(gameScenario);
			else
			{
				ret = false;
				fprintf(stderr,"error loading scenario: %s\n",currentFile.c_str());
			}
			break;
		case SCENARIO_CAROLINE_TRIVIA:
			currentFile += "scenario.caroline_trivia.xml";
			if(parseScenario(currentFile,gameScenario))
				m_gameScenarioVector.push_back(gameScenario);
			else
			{
				ret = false;
				fprintf(stderr,"error loading scenario: %s\n",currentFile.c_str());
			}
			break;
		case SCENARIO_MONIQUE_TRIVIA:
			currentFile += "scenario.monique_trivia.xml";
			if(parseScenario(currentFile,gameScenario))
				m_gameScenarioVector.push_back(gameScenario);
			else
			{
				ret = false;
				fprintf(stderr,"error loading scenario: %s\n",currentFile.c_str());
			}
			break;
		case SCENARIO_LOCH_TRIVIA:
			currentFile += "scenario.loch_trivia.xml";
			if(parseScenario(currentFile,gameScenario))
				m_gameScenarioVector.push_back(gameScenario);
			else
			{
				ret = false;
				fprintf(stderr,"error loading scenario: %s\n",currentFile.c_str());
			}
			break;
		case SCENARIO_REMEMBER_PAST_CHOICES:
			currentFile += "scenario.remember_past_choices.xml";
			if(parseScenario(currentFile,gameScenario))
				m_gameScenarioVector.push_back(gameScenario);
			else
			{
				ret = false;
				fprintf(stderr,"error loading scenario: %s\n",currentFile.c_str());
			}
			break;
		case SCENARIO_SEARCH_LEEDS_HOUSE:
			currentFile += "scenario.search_leeds_house.xml";
			if(parseScenario(currentFile,gameScenario))
				m_gameScenarioVector.push_back(gameScenario);
			else
			{
				ret = false;
				fprintf(stderr,"error loading scenario: %s\n",currentFile.c_str());
			}
			break;
		case SCENARIO_PAST_SEYMOUR:
			currentFile += "scenario.past_seymour.xml";
			if(parseScenario(currentFile,gameScenario))
				m_gameScenarioVector.push_back(gameScenario);
			else
			{
				ret = false;
				fprintf(stderr,"error loading scenario: %s\n",currentFile.c_str());
			}
			break;
		case SCENARIO_MONIQUE_DEAD:
			currentFile += "scenario.monique_dead.xml";
			if(parseScenario(currentFile,gameScenario))
				m_gameScenarioVector.push_back(gameScenario);
			else
			{
				ret = false;
				fprintf(stderr,"error loading scenario: %s\n",currentFile.c_str());
			}
			break;
		case SCENARIO_THE_STRIPPER_TRIVIA:
			currentFile += "scenario.the_stripper_trivia.xml";
			if(parseScenario(currentFile,gameScenario))
				m_gameScenarioVector.push_back(gameScenario);
			else
			{
				ret = false;
				fprintf(stderr,"error loading scenario: %s\n",currentFile.c_str());
			}
			break;
		case SCENARIO_REMEMBER_PAST_CONTINUE:
			currentFile += "scenario.remember_past_continue.xml";
			if(parseScenario(currentFile,gameScenario))
				m_gameScenarioVector.push_back(gameScenario);
			else
			{
				ret = false;
				fprintf(stderr,"error loading scenario: %s\n",currentFile.c_str());
			}
			break;
		case SCENARIO_HOOVER_TRIVIA:
			currentFile += "scenario.hoover_trivia.xml";
			if(parseScenario(currentFile,gameScenario))
				m_gameScenarioVector.push_back(gameScenario);
			else
			{
				ret = false;
				fprintf(stderr,"error loading scenario: %s\n",currentFile.c_str());
			}
			break;
		case SCENARIO_DON_LOUISIANA_TRIVIA:
			currentFile += "scenario.don_louisiana_trivia.xml";
			if(parseScenario(currentFile,gameScenario))
				m_gameScenarioVector.push_back(gameScenario);
			else
			{
				ret = false;
				fprintf(stderr,"error loading scenario: %s\n",currentFile.c_str());
			}
			break;
		default:
			ret = false;
			fprintf(stderr,"error unreferenced scenario: %d\n",i);
			break;
		}
	}
	return ret;
}

int MainWindow::getScenarioOffsetFromText(const string &scenarioName)
{
	int ret = -1;
	if(scenarioName == "start")
		ret = SCENARIO_START;
	else if(scenarioName == "drink_at_sams_after_murder")
		ret = SCENARIO_DRINK_AT_SAMS_AFTER_MURDER;
	else if(scenarioName == "tell_mary_after_murder")
		ret = SCENARIO_TELL_MARY_AFTER_MURDER;
	else if(scenarioName == "quit_after_tony")
		ret = SCENARIO_QUIT_AFTER_TONY;
	else if(scenarioName == "search_house_after_tony")
		ret = SCENARIO_SEARCH_HOUSE_AFTER_TONY;
	else if(scenarioName == "crustacean_cove")
		ret = SCENARIO_CRUSTACEAN_COVE;
	else if(scenarioName == "haunted_caves")
		ret = SCENARIO_HAUNTED_CAVES;
	else if(scenarioName == "lou_diamond")
		ret = SCENARIO_LOU_DIAMOND;
	else if(scenarioName == "crustacean_cove_choices")
		ret = SCENARIO_CRUSTACEAN_COVE_CHOICES;
	else if(scenarioName == "bar_quit")
		ret = SCENARIO_BAR_QUIT;
	else if(scenarioName == "bruce")
		ret = SCENARIO_BRUCE;
	else if(scenarioName == "question_don")
		ret = SCENARIO_QUESTION_DON;
	else if(scenarioName == "crustacean_cove_continue")
		ret = SCENARIO_CRUSTACEAN_COVE_CONTINUE;
	else if(scenarioName == "crustacean_cove_killer_choices")
		ret = SCENARIO_CRUSTACEAN_COVE_KILLER_CHOICES;
	else if(scenarioName == "crustacean_cove_enrico_killer")
		ret = SCENARIO_CRUSTACEAN_COVE_ENRICO_KILLER;
	else if(scenarioName == "crustacean_cove_giuseppe_killer")
		ret = SCENARIO_CRUSTACEAN_COVE_GIUSEPPE_KILLER;
	else if(scenarioName == "crustacean_cove_bruce_killer")
		ret = SCENARIO_CRUSTACEAN_COVE_BRUCE_KILLER;
	else if(scenarioName == "crustacean_cove_evelyn_killer")
		ret = SCENARIO_CRUSTACEAN_COVE_EVELYN_KILLER;
	else if(scenarioName == "crustacean_cove_don_killer")
		ret = SCENARIO_CRUSTACEAN_COVE_DON_KILLER;
	else if(scenarioName == "crustacean_cove_lou_killer")
		ret = SCENARIO_CRUSTACEAN_COVE_LOU_KILLER;
	else if(scenarioName == "crustacean_cove_guido_killer")
		ret = SCENARIO_CRUSTACEAN_COVE_GUIDO_KILLER;
	else if(scenarioName == "ohannity_after_crustacean_cove")
		ret = SCENARIO_OHANNITY_AFTER_CRUSTACEAN_COVE;
	else if(scenarioName == "mona")
		ret = SCENARIO_MONA;
	else if(scenarioName == "mona_choose")
		ret = SCENARIO_MONA_CHOOSE;
	else if(scenarioName == "mona_continue")
		ret = SCENARIO_MONA_CONTINUE;
	else if(scenarioName == "train_suspect_choose")
		ret = SCENARIO_TRAIN_SUSPECT_CHOOSE;
	else if(scenarioName == "train_cave")
		ret = SCENARIO_TRAIN_CAVE;
	else if(scenarioName == "train_jacob")
		ret = SCENARIO_TRAIN_JACOB;
	else if(scenarioName == "collage_girls")
		ret = SCENARIO_COLLAGE_GIRLS;
	else if(scenarioName == "monica")
		ret = SCENARIO_MONICA;
	else if(scenarioName == "vera")
		ret = SCENARIO_VERA;
	else if(scenarioName == "schomes_kilowatson")
		ret = SCENARIO_SCHOMES_KILOWATSON;
	else if(scenarioName == "duncan")
		ret = SCENARIO_DUNCAN;
	else if(scenarioName == "kilbane")
		ret = SCENARIO_KILBANE;
	else if(scenarioName == "mcfabish")
		ret = SCENARIO_MCFABISH;
	else if(scenarioName == "lady_margaret")
		ret = SCENARIO_LADY_MARGARET;
	else if(scenarioName == "train_continue")
		ret = SCENARIO_TRAIN_CONTINUE;
	else if(scenarioName == "train_guilty_choose")
		ret = SCENARIO_TRAIN_GUILTY_CHOOSE;
	else if(scenarioName == "hemlock_guilty")
		ret = SCENARIO_HEMLOCK_GUILTY;
	else if(scenarioName == "kilowatson_guilty")
		ret = SCENARIO_KILOWATSON_GUILTY;
	else if(scenarioName == "collage_girls_guilty")
		ret = SCENARIO_COLLAGE_GIRLS_GUILTY;
	else if(scenarioName == "duncan_guilty")
		ret = SCENARIO_DUNCAN_GUILTY;
	else if(scenarioName == "margaret_guilty")
		ret = SCENARIO_MARGARET_GUILTY;
	else if(scenarioName == "mcfabish_guilty")
		ret = SCENARIO_MCFABISH_GUILTY;
	else if(scenarioName == "monica_guilty")
		ret = SCENARIO_MONICA_GUILTY;
	else if(scenarioName == "vera_guilty")
		ret = SCENARIO_VERA_GUILTY;
	else if(scenarioName == "kilbane_guilty")
		ret = SCENARIO_KILBANE_GUILTY;
	else if(scenarioName == "jacob_guilty")
		ret = SCENARIO_JACOB_GUILTY;
	else if(scenarioName == "porter_guilty")
		ret = SCENARIO_PORTER_GUILTY;
	else if(scenarioName == "dirk_trivia")
		ret = SCENARIO_DIRK_TRIVIA;
	else if(scenarioName == "leeds_office_trivia")
		ret = SCENARIO_LEEDS_OFFICE_TRIVIA;
	else if(scenarioName == "leed_trivia")
		ret = SCENARIO_LEED_TRIVIA;
	else if(scenarioName == "funeral_trivia")
		ret = SCENARIO_FUNERAL_TRIVIA;
	else if(scenarioName == "don_trivia")
		ret = SCENARIO_DON_TRIVIA;
	else if(scenarioName == "train_baby_trivia")
		ret = SCENARIO_TRAIN_BABY_TRIVIA;
	else if(scenarioName == "lou_trivia")
		ret = SCENARIO_LOU_TRIVIA;
	else if(scenarioName == "money_trivia")
		ret = SCENARIO_MONEY_TRIVIA;
	else if(scenarioName == "sheriff_trivia")
		ret = SCENARIO_SHERIFF_TRIVIA;
	else if(scenarioName == "circe_trivia")
		ret = SCENARIO_CIRCE_TRIVIA;
	else if(scenarioName == "terror_trivia")
		ret = SCENARIO_TERROR_TRIVIA;
	else if(scenarioName == "veal_trivia")
		ret = SCENARIO_VEAL_TRIVIA;
	else if(scenarioName == "margaret_trivia")
		ret = SCENARIO_MARGARET_TRIVIA;
	else if(scenarioName == "bunny_trivia")
		ret = SCENARIO_BUNNY_TRIVIA;
	else if(scenarioName == "bruce_trivia")
		ret = SCENARIO_BRUCE_TRIVIA;
	else if(scenarioName == "caroline_trivia")
		ret = SCENARIO_CAROLINE_TRIVIA;
	else if(scenarioName == "monique_trivia")
		ret = SCENARIO_MONIQUE_TRIVIA;
	else if(scenarioName == "loch_trivia")
		ret = SCENARIO_LOCH_TRIVIA;
	else if(scenarioName == "remember_past_choices")
		ret = SCENARIO_REMEMBER_PAST_CHOICES;
	else if(scenarioName == "search_leeds_house")
		ret = SCENARIO_SEARCH_LEEDS_HOUSE;
	else if(scenarioName == "past_seymour")
		ret = SCENARIO_PAST_SEYMOUR;
	else if(scenarioName == "monique_dead")
		ret = SCENARIO_MONIQUE_DEAD;
	else if(scenarioName == "the_stripper_trivia")
		ret = SCENARIO_THE_STRIPPER_TRIVIA;
	else if(scenarioName == "remember_past_continue")
		ret = SCENARIO_REMEMBER_PAST_CONTINUE;
	else if(scenarioName == "hoover_trivia")
		ret = SCENARIO_HOOVER_TRIVIA;
	else if(scenarioName == "don_louisiana_trivia")
		ret = SCENARIO_DON_LOUISIANA_TRIVIA;
	return ret;
}

bool MainWindow::loadMovies(const string &dataDirectory)
{
	bool ret = true;
	string movieDirectory = dataDirectory;
	movieDirectory += "movie/";
	for(int i = 0;(i < MOVIE_MAX) && ret;i++)
	{
		string currentFile = movieDirectory;
		bool found = true;
		switch(i)
		{
		case MOVIE_BLESSING:
			currentFile += "blessing.avi";
			if(!QFile::exists(currentFile.c_str()))
			{
				ret = false;
				fprintf(stderr,"error loading movie: %s\n",currentFile.c_str());
			}
			break;
		case MOVIE_BOOGIE:
			currentFile += "boogie.avi";
			if(!QFile::exists(currentFile.c_str()))
			{
				ret = false;
				fprintf(stderr,"error loading movie: %s\n",currentFile.c_str());
			}
			break;
		case MOVIE_BOOGIE_2:
			currentFile += "boogie_2.avi";
			if(!QFile::exists(currentFile.c_str()))
			{
				ret = false;
				fprintf(stderr,"error loading movie: %s\n",currentFile.c_str());
			}
			break;
		case MOVIE_BOOGIE_3:
			currentFile += "boogie_3.avi";
			if(!QFile::exists(currentFile.c_str()))
			{
				ret = false;
				fprintf(stderr,"error loading movie: %s\n",currentFile.c_str());
			}
			break;
		case MOVIE_BOOGIE_4:
			currentFile += "boogie_4.avi";
			if(!QFile::exists(currentFile.c_str()))
			{
				ret = false;
				fprintf(stderr,"error loading movie: %s\n",currentFile.c_str());
			}
			break;
		case MOVIE_BRUCE_ESCAPE:
			currentFile += "bruce_escape.avi";
			if(!QFile::exists(currentFile.c_str()))
			{
				ret = false;
				fprintf(stderr,"error loading movie: %s\n",currentFile.c_str());
			}
			break;
		case MOVIE_CALLING_POLICE:
			currentFile += "calling_police.avi";
			if(!QFile::exists(currentFile.c_str()))
			{
				ret = false;
				fprintf(stderr,"error loading movie: %s\n",currentFile.c_str());
			}
			break;
		case MOVIE_CAT_JAZZ:
			currentFile += "cat_jazz.avi";
			if(!QFile::exists(currentFile.c_str()))
			{
				ret = false;
				fprintf(stderr,"error loading movie: %s\n",currentFile.c_str());
			}
			break;
		case MOVIE_CAVE_JELLY:
			currentFile += "cave_jelly.avi";
			if(!QFile::exists(currentFile.c_str()))
			{
				ret = false;
				fprintf(stderr,"error loading movie: %s\n",currentFile.c_str());
			}
			break;
		case MOVIE_CHARIOT:
			currentFile += "chariot.avi";
			if(!QFile::exists(currentFile.c_str()))
			{
				ret = false;
				fprintf(stderr,"error loading movie: %s\n",currentFile.c_str());
			}
			break;
		case MOVIE_COME_DOWN_STAIRS:
			currentFile += "come_down_stairs.avi";
			if(!QFile::exists(currentFile.c_str()))
			{
				ret = false;
				fprintf(stderr,"error loading movie: %s\n",currentFile.c_str());
			}
			break;
		case MOVIE_COME_IN_LISTEN:
			currentFile += "come_in_listen.avi";
			if(!QFile::exists(currentFile.c_str()))
			{
				ret = false;
				fprintf(stderr,"error loading movie: %s\n",currentFile.c_str());
			}
			break;
		case MOVIE_COUPLE_HUG:
			currentFile += "couple_hug.avi";
			if(!QFile::exists(currentFile.c_str()))
			{
				ret = false;
				fprintf(stderr,"error loading movie: %s\n",currentFile.c_str());
			}
			break;
		case MOVIE_CRAB_ATTACK:
			currentFile += "crab_attack.avi";
			if(!QFile::exists(currentFile.c_str()))
			{
				ret = false;
				fprintf(stderr,"error loading movie: %s\n",currentFile.c_str());
			}
			break;
		case MOVIE_CRAZY:
			currentFile += "crazy.avi";
			if(!QFile::exists(currentFile.c_str()))
			{
				ret = false;
				fprintf(stderr,"error loading movie: %s\n",currentFile.c_str());
			}
			break;
		case MOVIE_CREEPING_SHADOW:
			currentFile += "creeping_shadow.avi";
			if(!QFile::exists(currentFile.c_str()))
			{
				ret = false;
				fprintf(stderr,"error loading movie: %s\n",currentFile.c_str());
			}
			break;
		case MOVIE_CURRY:
			currentFile += "curry.avi";
			if(!QFile::exists(currentFile.c_str()))
			{
				ret = false;
				fprintf(stderr,"error loading movie: %s\n",currentFile.c_str());
			}
			break;
		case MOVIE_DARKMAN:
			currentFile += "darkman.avi";
			if(!QFile::exists(currentFile.c_str()))
			{
				ret = false;
				fprintf(stderr,"error loading movie: %s\n",currentFile.c_str());
			}
			break;
		case MOVIE_DECENTLY:
			currentFile += "decently.avi";
			if(!QFile::exists(currentFile.c_str()))
			{
				ret = false;
				fprintf(stderr,"error loading movie: %s\n",currentFile.c_str());
			}
			break;
		case MOVIE_DON_FINDS_BLOOD:
			currentFile += "don_finds_blood.avi";
			if(!QFile::exists(currentFile.c_str()))
			{
				ret = false;
				fprintf(stderr,"error loading movie: %s\n",currentFile.c_str());
			}
			break;
		case MOVIE_DON_GET_GUN:
			currentFile += "don_get_gun.avi";
			if(!QFile::exists(currentFile.c_str()))
			{
				ret = false;
				fprintf(stderr,"error loading movie: %s\n",currentFile.c_str());
			}
			break;
		case MOVIE_DON_STARE:
			currentFile += "don_stare.avi";
			if(!QFile::exists(currentFile.c_str()))
			{
				ret = false;
				fprintf(stderr,"error loading movie: %s\n",currentFile.c_str());
			}
			break;
		case MOVIE_DON_THOUGHT:
			currentFile += "don_thought.avi";
			if(!QFile::exists(currentFile.c_str()))
			{
				ret = false;
				fprintf(stderr,"error loading movie: %s\n",currentFile.c_str());
			}
			break;
		case MOVIE_DONT_REMEMBER:
			currentFile += "dont_remember.avi";
			if(!QFile::exists(currentFile.c_str()))
			{
				ret = false;
				fprintf(stderr,"error loading movie: %s\n",currentFile.c_str());
			}
			break;
		case MOVIE_DONT_WORRY:
			currentFile += "dont_worry.avi";
			if(!QFile::exists(currentFile.c_str()))
			{
				ret = false;
				fprintf(stderr,"error loading movie: %s\n",currentFile.c_str());
			}
			break;
		case MOVIE_DOOR_GUNMAN:
			currentFile += "door_gunman.avi";
			if(!QFile::exists(currentFile.c_str()))
			{
				ret = false;
				fprintf(stderr,"error loading movie: %s\n",currentFile.c_str());
			}
			break;
		case MOVIE_DOUBLECROSS:
			currentFile += "doublecross.avi";
			if(!QFile::exists(currentFile.c_str()))
			{
				ret = false;
				fprintf(stderr,"error loading movie: %s\n",currentFile.c_str());
			}
			break;
		case MOVIE_DRAMATIC:
			currentFile += "dramatic.avi";
			if(!QFile::exists(currentFile.c_str()))
			{
				ret = false;
				fprintf(stderr,"error loading movie: %s\n",currentFile.c_str());
			}
			break;
		case MOVIE_DRIVE_RAIN:
			currentFile += "drive_rain.avi";
			if(!QFile::exists(currentFile.c_str()))
			{
				ret = false;
				fprintf(stderr,"error loading movie: %s\n",currentFile.c_str());
			}
			break;
		case MOVIE_ELOPE:
			currentFile += "elope.avi";
			if(!QFile::exists(currentFile.c_str()))
			{
				ret = false;
				fprintf(stderr,"error loading movie: %s\n",currentFile.c_str());
			}
			break;
		case MOVIE_ENTER_HOTEL:
			currentFile += "enter_hotel.avi";
			if(!QFile::exists(currentFile.c_str()))
			{
				ret = false;
				fprintf(stderr,"error loading movie: %s\n",currentFile.c_str());
			}
			break;
		case MOVIE_ESCAPE_CHAIR:
			currentFile += "escape_chair.avi";
			if(!QFile::exists(currentFile.c_str()))
			{
				ret = false;
				fprintf(stderr,"error loading movie: %s\n",currentFile.c_str());
			}
			break;
		case MOVIE_EVELYN_CRYING:
			currentFile += "evelyn_crying.avi";
			if(!QFile::exists(currentFile.c_str()))
			{
				ret = false;
				fprintf(stderr,"error loading movie: %s\n",currentFile.c_str());
			}
			break;
		case MOVIE_FACE_RAP:
			currentFile += "face_rap.avi";
			if(!QFile::exists(currentFile.c_str()))
			{
				ret = false;
				fprintf(stderr,"error loading movie: %s\n",currentFile.c_str());
			}
			break;
		case MOVIE_FIND_COSTAIRS:
			currentFile += "find_costairs.avi";
			if(!QFile::exists(currentFile.c_str()))
			{
				ret = false;
				fprintf(stderr,"error loading movie: %s\n",currentFile.c_str());
			}
			break;
		case MOVIE_FLASHBACK:
			currentFile += "flashback.avi";
			if(!QFile::exists(currentFile.c_str()))
			{
				ret = false;
				fprintf(stderr,"error loading movie: %s\n",currentFile.c_str());
			}
			break;
		case MOVIE_GLASS_OF_WHISKEY:
			currentFile += "glass_of_whiskey.avi";
			if(!QFile::exists(currentFile.c_str()))
			{
				ret = false;
				fprintf(stderr,"error loading movie: %s\n",currentFile.c_str());
			}
			break;
		case MOVIE_GOOD_NIGHT:
			currentFile += "good_night.avi";
			if(!QFile::exists(currentFile.c_str()))
			{
				ret = false;
				fprintf(stderr,"error loading movie: %s\n",currentFile.c_str());
			}
			break;
		case MOVIE_GRAB_GUIDO:
			currentFile += "grab_guido.avi";
			if(!QFile::exists(currentFile.c_str()))
			{
				ret = false;
				fprintf(stderr,"error loading movie: %s\n",currentFile.c_str());
			}
			break;
		case MOVIE_GUARD_IT:
			currentFile += "guard_it.avi";
			if(!QFile::exists(currentFile.c_str()))
			{
				ret = false;
				fprintf(stderr,"error loading movie: %s\n",currentFile.c_str());
			}
			break;
		case MOVIE_GUESS_NOT:
			currentFile += "guess_not.avi";
			if(!QFile::exists(currentFile.c_str()))
			{
				ret = false;
				fprintf(stderr,"error loading movie: %s\n",currentFile.c_str());
			}
			break;
		case MOVIE_HAD_IT_COMING:
			currentFile += "had_it_coming.avi";
			if(!QFile::exists(currentFile.c_str()))
			{
				ret = false;
				fprintf(stderr,"error loading movie: %s\n",currentFile.c_str());
			}
			break;
		case MOVIE_HANDS_UP:
			currentFile += "hands_up.avi";
			if(!QFile::exists(currentFile.c_str()))
			{
				ret = false;
				fprintf(stderr,"error loading movie: %s\n",currentFile.c_str());
			}
			break;
		case MOVIE_HIDE_BEHIND_BOXES:
			currentFile += "hide_behind_boxes.avi";
			if(!QFile::exists(currentFile.c_str()))
			{
				ret = false;
				fprintf(stderr,"error loading movie: %s\n",currentFile.c_str());
			}
			break;
		case MOVIE_HOLD_YOU:
			currentFile += "hold_you.avi";
			if(!QFile::exists(currentFile.c_str()))
			{
				ret = false;
				fprintf(stderr,"error loading movie: %s\n",currentFile.c_str());
			}
			break;
		case MOVIE_HOTEL_MURDER_SCENE:
			currentFile += "hotel_murder_scene.avi";
			if(!QFile::exists(currentFile.c_str()))
			{
				ret = false;
				fprintf(stderr,"error loading movie: %s\n",currentFile.c_str());
			}
			break;
		case MOVIE_HOTEL_MURDER_SCENE_2:
			currentFile += "hotel_murder_scene_2.avi";
			if(!QFile::exists(currentFile.c_str()))
			{
				ret = false;
				fprintf(stderr,"error loading movie: %s\n",currentFile.c_str());
			}
			break;
		case MOVIE_HOTEL_MURDER_SCENE_3:
			currentFile += "hotel_murder_scene_3.avi";
			if(!QFile::exists(currentFile.c_str()))
			{
				ret = false;
				fprintf(stderr,"error loading movie: %s\n",currentFile.c_str());
			}
			break;
		case MOVIE_I_AM:
			currentFile += "i_am.avi";
			if(!QFile::exists(currentFile.c_str()))
			{
				ret = false;
				fprintf(stderr,"error loading movie: %s\n",currentFile.c_str());
			}
			break;
		case MOVIE_I_SEE:
			currentFile += "i_see.avi";
			if(!QFile::exists(currentFile.c_str()))
			{
				ret = false;
				fprintf(stderr,"error loading movie: %s\n",currentFile.c_str());
			}
			break;
		case MOVIE_IM_FREE:
			currentFile += "im_free.avi";
			if(!QFile::exists(currentFile.c_str()))
			{
				ret = false;
				fprintf(stderr,"error loading movie: %s\n",currentFile.c_str());
			}
			break;
		case MOVIE_IN_CELLER:
			currentFile += "in_celler.avi";
			if(!QFile::exists(currentFile.c_str()))
			{
				ret = false;
				fprintf(stderr,"error loading movie: %s\n",currentFile.c_str());
			}
			break;
		case MOVIE_KILOWATSON_LATE:
			currentFile += "kilowatson_late.avi";
			if(!QFile::exists(currentFile.c_str()))
			{
				ret = false;
				fprintf(stderr,"error loading movie: %s\n",currentFile.c_str());
			}
			break;
		case MOVIE_KNEW_LEEDS:
			currentFile += "knew_leeds.avi";
			if(!QFile::exists(currentFile.c_str()))
			{
				ret = false;
				fprintf(stderr,"error loading movie: %s\n",currentFile.c_str());
			}
			break;
		case MOVIE_KNOCKED_WIND:
			currentFile += "knocked_wind.avi";
			if(!QFile::exists(currentFile.c_str()))
			{
				ret = false;
				fprintf(stderr,"error loading movie: %s\n",currentFile.c_str());
			}
			break;
		case MOVIE_LEEDS_ANGRY:
			currentFile += "leeds_angry.avi";
			if(!QFile::exists(currentFile.c_str()))
			{
				ret = false;
				fprintf(stderr,"error loading movie: %s\n",currentFile.c_str());
			}
			break;
		case MOVIE_LEEDS_UPSET:
			currentFile += "leeds_upset.avi";
			if(!QFile::exists(currentFile.c_str()))
			{
				ret = false;
				fprintf(stderr,"error loading movie: %s\n",currentFile.c_str());
			}
			break;
		case MOVIE_LET_ME_OUT:
			currentFile += "let_me_out.avi";
			if(!QFile::exists(currentFile.c_str()))
			{
				ret = false;
				fprintf(stderr,"error loading movie: %s\n",currentFile.c_str());
			}
			break;
		case MOVIE_LIGHT_SMOKE:
			currentFile += "light_smoke.avi";
			if(!QFile::exists(currentFile.c_str()))
			{
				ret = false;
				fprintf(stderr,"error loading movie: %s\n",currentFile.c_str());
			}
			break;
		case MOVIE_LISTEN_AT_DOOR:
			currentFile += "listen_at_door.avi";
			if(!QFile::exists(currentFile.c_str()))
			{
				ret = false;
				fprintf(stderr,"error loading movie: %s\n",currentFile.c_str());
			}
			break;
		case MOVIE_LOCKED_DOOR:
			currentFile += "locked_door.avi";
			if(!QFile::exists(currentFile.c_str()))
			{
				ret = false;
				fprintf(stderr,"error loading movie: %s\n",currentFile.c_str());
			}
			break;
		case MOVIE_LONG_TIME_AGO:
			currentFile += "long_time_ago.avi";
			if(!QFile::exists(currentFile.c_str()))
			{
				ret = false;
				fprintf(stderr,"error loading movie: %s\n",currentFile.c_str());
			}
			break;
		case MOVIE_LOOK_AROUND:
			currentFile += "look_around.avi";
			if(!QFile::exists(currentFile.c_str()))
			{
				ret = false;
				fprintf(stderr,"error loading movie: %s\n",currentFile.c_str());
			}
			break;
		case MOVIE_LOOK_CORNER:
			currentFile += "look_corner.avi";
			if(!QFile::exists(currentFile.c_str()))
			{
				ret = false;
				fprintf(stderr,"error loading movie: %s\n",currentFile.c_str());
			}
			break;
		case MOVIE_LOU_DON:
			currentFile += "lou_don.avi";
			if(!QFile::exists(currentFile.c_str()))
			{
				ret = false;
				fprintf(stderr,"error loading movie: %s\n",currentFile.c_str());
			}
			break;
		case MOVIE_LOVE_ANY_MORE:
			currentFile += "love_any_more.avi";
			if(!QFile::exists(currentFile.c_str()))
			{
				ret = false;
				fprintf(stderr,"error loading movie: %s\n",currentFile.c_str());
			}
			break;
		case MOVIE_LURK_IN_CELLER:
			currentFile += "lurk_in_celler.avi";
			if(!QFile::exists(currentFile.c_str()))
			{
				ret = false;
				fprintf(stderr,"error loading movie: %s\n",currentFile.c_str());
			}
			break;
		case MOVIE_MAKE_BREAK:
			currentFile += "make_break.avi";
			if(!QFile::exists(currentFile.c_str()))
			{
				ret = false;
				fprintf(stderr,"error loading movie: %s\n",currentFile.c_str());
			}
			break;
		case MOVIE_MARGARET_LOOK:
			currentFile += "margaret_look.avi";
			if(!QFile::exists(currentFile.c_str()))
			{
				ret = false;
				fprintf(stderr,"error loading movie: %s\n",currentFile.c_str());
			}
			break;
		case MOVIE_MCFABISH_ARRIVES:
			currentFile += "mcfabish_arrives.avi";
			if(!QFile::exists(currentFile.c_str()))
			{
				ret = false;
				fprintf(stderr,"error loading movie: %s\n",currentFile.c_str());
			}
			break;
		case MOVIE_MCFABISH_KNOCK:
			currentFile += "mcfabish_knock.avi";
			if(!QFile::exists(currentFile.c_str()))
			{
				ret = false;
				fprintf(stderr,"error loading movie: %s\n",currentFile.c_str());
			}
			break;
		case MOVIE_MEANING_OF_THIS:
			currentFile += "meaning_of_this.avi";
			if(!QFile::exists(currentFile.c_str()))
			{
				ret = false;
				fprintf(stderr,"error loading movie: %s\n",currentFile.c_str());
			}
			break;
		case MOVIE_MEANT_IT:
			currentFile += "meant_it.avi";
			if(!QFile::exists(currentFile.c_str()))
			{
				ret = false;
				fprintf(stderr,"error loading movie: %s\n",currentFile.c_str());
			}
			break;
		case MOVIE_MEET_SCHOMES:
			currentFile += "meet_schomes.avi";
			if(!QFile::exists(currentFile.c_str()))
			{
				ret = false;
				fprintf(stderr,"error loading movie: %s\n",currentFile.c_str());
			}
			break;
		case MOVIE_MINIONS:
			currentFile += "minions.avi";
			if(!QFile::exists(currentFile.c_str()))
			{
				ret = false;
				fprintf(stderr,"error loading movie: %s\n",currentFile.c_str());
			}
			break;
		case MOVIE_MORSE:
			currentFile += "morse.avi";
			if(!QFile::exists(currentFile.c_str()))
			{
				ret = false;
				fprintf(stderr,"error loading movie: %s\n",currentFile.c_str());
			}
			break;
		case MOVIE_MURDER_SCENE:
			currentFile += "murder_scene.avi";
			if(!QFile::exists(currentFile.c_str()))
			{
				ret = false;
				fprintf(stderr,"error loading movie: %s\n",currentFile.c_str());
			}
			break;
		case MOVIE_NOT_A_MURDERER:
			currentFile += "not_a_murderer.avi";
			if(!QFile::exists(currentFile.c_str()))
			{
				ret = false;
				fprintf(stderr,"error loading movie: %s\n",currentFile.c_str());
			}
			break;
		case MOVIE_NOTHING_TO_CONFESS:
			currentFile += "nothing_to_confess.avi";
			if(!QFile::exists(currentFile.c_str()))
			{
				ret = false;
				fprintf(stderr,"error loading movie: %s\n",currentFile.c_str());
			}
			break;
		case MOVIE_OFFICE_FRONT:
			currentFile += "office_front.avi";
			if(!QFile::exists(currentFile.c_str()))
			{
				ret = false;
				fprintf(stderr,"error loading movie: %s\n",currentFile.c_str());
			}
			break;
		case MOVIE_PARTNERS:
			currentFile += "partners.avi";
			if(!QFile::exists(currentFile.c_str()))
			{
				ret = false;
				fprintf(stderr,"error loading movie: %s\n",currentFile.c_str());
			}
			break;
		case MOVIE_PROFESSOR_LOOK:
			currentFile += "professor_look.avi";
			if(!QFile::exists(currentFile.c_str()))
			{
				ret = false;
				fprintf(stderr,"error loading movie: %s\n",currentFile.c_str());
			}
			break;
		case MOVIE_PROTECTED_SELF:
			currentFile += "protected_self.avi";
			if(!QFile::exists(currentFile.c_str()))
			{
				ret = false;
				fprintf(stderr,"error loading movie: %s\n",currentFile.c_str());
			}
			break;
		case MOVIE_RAIN_GUN:
			currentFile += "rain_gun.avi";
			if(!QFile::exists(currentFile.c_str()))
			{
				ret = false;
				fprintf(stderr,"error loading movie: %s\n",currentFile.c_str());
			}
			break;
		case MOVIE_RIGHT_AHEAD:
			currentFile += "right_ahead.avi";
			if(!QFile::exists(currentFile.c_str()))
			{
				ret = false;
				fprintf(stderr,"error loading movie: %s\n",currentFile.c_str());
			}
			break;
		case MOVIE_ROUGH_HIM_UP:
			currentFile += "rough_him_up.avi";
			if(!QFile::exists(currentFile.c_str()))
			{
				ret = false;
				fprintf(stderr,"error loading movie: %s\n",currentFile.c_str());
			}
			break;
		case MOVIE_SEE_JEWEL:
			currentFile += "see_jewel.avi";
			if(!QFile::exists(currentFile.c_str()))
			{
				ret = false;
				fprintf(stderr,"error loading movie: %s\n",currentFile.c_str());
			}
			break;
		case MOVIE_SHERIFF_COMES_IN:
			currentFile += "sheriff_comes_in.avi";
			if(!QFile::exists(currentFile.c_str()))
			{
				ret = false;
				fprintf(stderr,"error loading movie: %s\n",currentFile.c_str());
			}
			break;
		case MOVIE_SHERIFF_ENTERS_LOBBY:
			currentFile += "sheriff_enters_lobby.avi";
			if(!QFile::exists(currentFile.c_str()))
			{
				ret = false;
				fprintf(stderr,"error loading movie: %s\n",currentFile.c_str());
			}
			break;
		case MOVIE_SHERIFF_IN_RAIN:
			currentFile += "sheriff_in_rain.avi";
			if(!QFile::exists(currentFile.c_str()))
			{
				ret = false;
				fprintf(stderr,"error loading movie: %s\n",currentFile.c_str());
			}
			break;
		case MOVIE_SHUT_THE_DOOR:
			currentFile += "shut_the_door.avi";
			if(!QFile::exists(currentFile.c_str()))
			{
				ret = false;
				fprintf(stderr,"error loading movie: %s\n",currentFile.c_str());
			}
			break;
		case MOVIE_SOMETHING_WRONG:
			currentFile += "something_wrong.avi";
			if(!QFile::exists(currentFile.c_str()))
			{
				ret = false;
				fprintf(stderr,"error loading movie: %s\n",currentFile.c_str());
			}
			break;
		case MOVIE_SPACESHIP_MOVIE:
			currentFile += "spaceship_movie.avi";
			if(!QFile::exists(currentFile.c_str()))
			{
				ret = false;
				fprintf(stderr,"error loading movie: %s\n",currentFile.c_str());
			}
			break;
		case MOVIE_TIDDLY:
			currentFile += "tiddly.avi";
			if(!QFile::exists(currentFile.c_str()))
			{
				ret = false;
				fprintf(stderr,"error loading movie: %s\n",currentFile.c_str());
			}
			break;
		case MOVIE_TIED_UP:
			currentFile += "tied_up.avi";
			if(!QFile::exists(currentFile.c_str()))
			{
				ret = false;
				fprintf(stderr,"error loading movie: %s\n",currentFile.c_str());
			}
			break;
		case MOVIE_TIMELY_ASSIST:
			currentFile += "timely_assist.avi";
			if(!QFile::exists(currentFile.c_str()))
			{
				ret = false;
				fprintf(stderr,"error loading movie: %s\n",currentFile.c_str());
			}
			break;
		case MOVIE_TRAIN_LEAVING:
			currentFile += "train_leaving.avi";
			if(!QFile::exists(currentFile.c_str()))
			{
				ret = false;
				fprintf(stderr,"error loading movie: %s\n",currentFile.c_str());
			}
			break;
		case MOVIE_TRAIN_STATION:
			currentFile += "train_station.avi";
			if(!QFile::exists(currentFile.c_str()))
			{
				ret = false;
				fprintf(stderr,"error loading movie: %s\n",currentFile.c_str());
			}
			break;
		case MOVIE_TRAIN_STOP:
			currentFile += "train_stop.avi";
			if(!QFile::exists(currentFile.c_str()))
			{
				ret = false;
				fprintf(stderr,"error loading movie: %s\n",currentFile.c_str());
			}
			break;
		case MOVIE_TRAIN_TRAVEL:
			currentFile += "train_travel.avi";
			if(!QFile::exists(currentFile.c_str()))
			{
				ret = false;
				fprintf(stderr,"error loading movie: %s\n",currentFile.c_str());
			}
			break;
		case MOVIE_TREX_MOVIE:
			currentFile += "trex_movie.avi";
			if(!QFile::exists(currentFile.c_str()))
			{
				ret = false;
				fprintf(stderr,"error loading movie: %s\n",currentFile.c_str());
			}
			break;
		case MOVIE_TRICK:
			currentFile += "trick.avi";
			if(!QFile::exists(currentFile.c_str()))
			{
				ret = false;
				fprintf(stderr,"error loading movie: %s\n",currentFile.c_str());
			}
			break;
		case MOVIE_TRY_DOOR:
			currentFile += "try_door.avi";
			if(!QFile::exists(currentFile.c_str()))
			{
				ret = false;
				fprintf(stderr,"error loading movie: %s\n",currentFile.c_str());
			}
			break;
		case MOVIE_VERA_DINING:
			currentFile += "vera_dining.avi";
			if(!QFile::exists(currentFile.c_str()))
			{
				ret = false;
				fprintf(stderr,"error loading movie: %s\n",currentFile.c_str());
			}
			break;
		case MOVIE_VERA_LOOK:
			currentFile += "vera_look.avi";
			if(!QFile::exists(currentFile.c_str()))
			{
				ret = false;
				fprintf(stderr,"error loading movie: %s\n",currentFile.c_str());
			}
			break;
		case MOVIE_VERA_WALKING:
			currentFile += "vera_walking.avi";
			if(!QFile::exists(currentFile.c_str()))
			{
				ret = false;
				fprintf(stderr,"error loading movie: %s\n",currentFile.c_str());
			}
			break;
		case MOVIE_VERA_WATCHING:
			currentFile += "vera_watching.avi";
			if(!QFile::exists(currentFile.c_str()))
			{
				ret = false;
				fprintf(stderr,"error loading movie: %s\n",currentFile.c_str());
			}
			break;
		case MOVIE_WHO_I_MARRY:
			currentFile += "who_i_marry.avi";
			if(!QFile::exists(currentFile.c_str()))
			{
				ret = false;
				fprintf(stderr,"error loading movie: %s\n",currentFile.c_str());
			}
			break;
		case MOVIE_WHY_TRAIN:
			currentFile += "why_train.avi";
			if(!QFile::exists(currentFile.c_str()))
			{
				ret = false;
				fprintf(stderr,"error loading movie: %s\n",currentFile.c_str());
			}
			break;
		case MOVIE_WINDOW_FACE:
			currentFile += "window_face.avi";
			if(!QFile::exists(currentFile.c_str()))
			{
				ret = false;
				fprintf(stderr,"error loading movie: %s\n",currentFile.c_str());
			}
			break;
		case MOVIE_YOU_COULDNT_HAVE:
			currentFile += "you_couldnt_have.avi";
			if(!QFile::exists(currentFile.c_str()))
			{
				ret = false;
				fprintf(stderr,"error loading movie: %s\n",currentFile.c_str());
			}
			break;
		default:
			found = false;
			ret = false;
			fprintf(stderr,"error unreferenced movie: %d\n",i);
			break;
		}
		if(found)
			m_movieVector.push_back(currentFile);
	}
	return ret;
}

int MainWindow::getMovieOffsetFromText(const string &movieText)
{
	int ret = -1;
	if(movieText == "blessing")
		ret = MOVIE_BLESSING;
	else if(movieText == "boogie")
		ret = MOVIE_BOOGIE;
	else if(movieText == "boogie_2")
		ret = MOVIE_BOOGIE_2;
	else if(movieText == "boogie_3")
		ret = MOVIE_BOOGIE_3;
	else if(movieText == "boogie_4")
		ret = MOVIE_BOOGIE_4;
	else if(movieText == "bruce_escape")
		ret = MOVIE_BRUCE_ESCAPE;
	else if(movieText == "calling_police")
		ret = MOVIE_CALLING_POLICE;
	else if(movieText == "cat_jazz")
		ret = MOVIE_CAT_JAZZ;
	else if(movieText == "cave_jelly")
		ret = MOVIE_CAVE_JELLY;
	else if(movieText == "chariot")
		ret = MOVIE_CHARIOT;
	else if(movieText == "come_down_stairs")
		ret = MOVIE_COME_DOWN_STAIRS;
	else if(movieText == "come_in_listen")
		ret = MOVIE_COME_IN_LISTEN;
	else if(movieText == "couple_hug")
		ret = MOVIE_COUPLE_HUG;
	else if(movieText == "crab_attack")
		ret = MOVIE_CRAB_ATTACK;
	else if(movieText == "crazy")
		ret = MOVIE_CRAZY;
	else if(movieText == "creeping_shadow")
		ret = MOVIE_CREEPING_SHADOW;
	else if(movieText == "curry")
		ret = MOVIE_CURRY;
	else if(movieText == "darkman")
		ret = MOVIE_DARKMAN;
	else if(movieText == "decently")
		ret = MOVIE_DECENTLY;
	else if(movieText == "don_finds_blood")
		ret = MOVIE_DON_FINDS_BLOOD;
	else if(movieText == "don_get_gun")
		ret = MOVIE_DON_GET_GUN;
	else if(movieText == "don_stare")
		ret = MOVIE_DON_STARE;
	else if(movieText == "don_thought")
		ret = MOVIE_DON_THOUGHT;
	else if(movieText == "dont_remember")
		ret = MOVIE_DONT_REMEMBER;
	else if(movieText == "dont_worry")
		ret = MOVIE_DONT_WORRY;
	else if(movieText == "door_gunman")
		ret = MOVIE_DOOR_GUNMAN;
	else if(movieText == "doublecross")
		ret = MOVIE_DOUBLECROSS;
	else if(movieText == "dramatic")
		ret = MOVIE_DRAMATIC;
	else if(movieText == "drive_rain")
		ret = MOVIE_DRIVE_RAIN;
	else if(movieText == "elope")
		ret = MOVIE_ELOPE;
	else if(movieText == "enter_hotel")
		ret = MOVIE_ENTER_HOTEL;
	else if(movieText == "escape_chair")
		ret = MOVIE_ESCAPE_CHAIR;
	else if(movieText == "evelyn_crying")
		ret = MOVIE_EVELYN_CRYING;
	else if(movieText == "face_rap")
		ret = MOVIE_FACE_RAP;
	else if(movieText == "find_costairs")
		ret = MOVIE_FIND_COSTAIRS;
	else if(movieText == "flashback")
		ret = MOVIE_FLASHBACK;
	else if(movieText == "glass_of_whiskey")
		ret = MOVIE_GLASS_OF_WHISKEY;
	else if(movieText == "good_night")
		ret = MOVIE_GOOD_NIGHT;
	else if(movieText == "grab_guido")
		ret = MOVIE_GRAB_GUIDO;
	else if(movieText == "guard_it")
		ret = MOVIE_GUARD_IT;
	else if(movieText == "guess_not")
		ret = MOVIE_GUESS_NOT;
	else if(movieText == "had_it_coming")
		ret = MOVIE_HAD_IT_COMING;
	else if(movieText == "hands_up")
		ret = MOVIE_HANDS_UP;
	else if(movieText == "hide_behind_boxes")
		ret = MOVIE_HIDE_BEHIND_BOXES;
	else if(movieText == "hold_you")
		ret = MOVIE_HOLD_YOU;
	else if(movieText == "hotel_murder_scene")
		ret = MOVIE_HOTEL_MURDER_SCENE;
	else if(movieText == "hotel_murder_scene_2")
		ret = MOVIE_HOTEL_MURDER_SCENE_2;
	else if(movieText == "hotel_murder_scene_3")
		ret = MOVIE_HOTEL_MURDER_SCENE_3;
	else if(movieText == "i_am")
		ret = MOVIE_I_AM;
	else if(movieText == "i_see")
		ret = MOVIE_I_SEE;
	else if(movieText == "im_free")
		ret = MOVIE_IM_FREE;
	else if(movieText == "in_celler")
		ret = MOVIE_IN_CELLER;
	else if(movieText == "kilowatson_late")
		ret = MOVIE_KILOWATSON_LATE;
	else if(movieText == "knew_leeds")
		ret = MOVIE_KNEW_LEEDS;
	else if(movieText == "knocked_wind")
		ret = MOVIE_KNOCKED_WIND;
	else if(movieText == "leeds_angry")
		ret = MOVIE_LEEDS_ANGRY;
	else if(movieText == "leeds_upset")
		ret = MOVIE_LEEDS_UPSET;
	else if(movieText == "let_me_out")
		ret = MOVIE_LET_ME_OUT;
	else if(movieText == "light_smoke")
		ret = MOVIE_LIGHT_SMOKE;
	else if(movieText == "listen_at_door")
		ret = MOVIE_LISTEN_AT_DOOR;
	else if(movieText == "locked_door")
		ret = MOVIE_LOCKED_DOOR;
	else if(movieText == "long_time_ago")
		ret = MOVIE_LONG_TIME_AGO;
	else if(movieText == "look_around")
		ret = MOVIE_LOOK_AROUND;
	else if(movieText == "look_corner")
		ret = MOVIE_LOOK_CORNER;
	else if(movieText == "lou_don")
		ret = MOVIE_LOU_DON;
	else if(movieText == "love_any_more")
		ret = MOVIE_LOVE_ANY_MORE;
	else if(movieText == "lurk_in_celler")
		ret = MOVIE_LURK_IN_CELLER;
	else if(movieText == "make_break")
		ret = MOVIE_MAKE_BREAK;
	else if(movieText == "margaret_look")
		ret = MOVIE_MARGARET_LOOK;
	else if(movieText == "mcfabish_arrives")
		ret = MOVIE_MCFABISH_ARRIVES;
	else if(movieText == "mcfabish_knock")
		ret = MOVIE_MCFABISH_KNOCK;
	else if(movieText == "meaning_of_this")
		ret = MOVIE_MEANING_OF_THIS;
	else if(movieText == "meant_it")
		ret = MOVIE_MEANT_IT;
	else if(movieText == "meet_schomes")
		ret = MOVIE_MEET_SCHOMES;
	else if(movieText == "minions")
		ret = MOVIE_MINIONS;
	else if(movieText == "morse")
		ret = MOVIE_MORSE;
	else if(movieText == "murder_scene")
		ret = MOVIE_MURDER_SCENE;
	else if(movieText == "not_a_murderer")
		ret = MOVIE_NOT_A_MURDERER;
	else if(movieText == "nothing_to_confess")
		ret = MOVIE_NOTHING_TO_CONFESS;
	else if(movieText == "office_front")
		ret = MOVIE_OFFICE_FRONT;
	else if(movieText == "partners")
		ret = MOVIE_PARTNERS;
	else if(movieText == "professor_look")
		ret = MOVIE_PROFESSOR_LOOK;
	else if(movieText == "protected_self")
		ret = MOVIE_PROTECTED_SELF;
	else if(movieText == "rain_gun")
		ret = MOVIE_RAIN_GUN;
	else if(movieText == "right_ahead")
		ret = MOVIE_RIGHT_AHEAD;
	else if(movieText == "rough_him_up")
		ret = MOVIE_ROUGH_HIM_UP;
	else if(movieText == "see_jewel")
		ret = MOVIE_SEE_JEWEL;
	else if(movieText == "sheriff_comes_in")
		ret = MOVIE_SHERIFF_COMES_IN;
	else if(movieText == "sheriff_enters_lobby")
		ret = MOVIE_SHERIFF_ENTERS_LOBBY;
	else if(movieText == "sheriff_in_rain")
		ret = MOVIE_SHERIFF_IN_RAIN;
	else if(movieText == "shut_the_door")
		ret = MOVIE_SHUT_THE_DOOR;
	else if(movieText == "something_wrong")
		ret = MOVIE_SOMETHING_WRONG;
	else if(movieText == "spaceship_movie")
		ret = MOVIE_SPACESHIP_MOVIE;
	else if(movieText == "tiddly")
		ret = MOVIE_TIDDLY;
	else if(movieText == "tied_up")
		ret = MOVIE_TIED_UP;
	else if(movieText == "timely_assist")
		ret = MOVIE_TIMELY_ASSIST;
	else if(movieText == "train_leaving")
		ret = MOVIE_TRAIN_LEAVING;
	else if(movieText == "train_station")
		ret = MOVIE_TRAIN_STATION;
	else if(movieText == "train_stop")
		ret = MOVIE_TRAIN_STOP;
	else if(movieText == "train_travel")
		ret = MOVIE_TRAIN_TRAVEL;
	else if(movieText == "trex_movie")
		ret = MOVIE_TREX_MOVIE;
	else if(movieText == "trick")
		ret = MOVIE_TRICK;
	else if(movieText == "try_door")
		ret = MOVIE_TRY_DOOR;
	else if(movieText == "vera_dining")
		ret = MOVIE_VERA_DINING;
	else if(movieText == "vera_look")
		ret = MOVIE_VERA_LOOK;
	else if(movieText == "vera_walking")
		ret = MOVIE_VERA_WALKING;
	else if(movieText == "vera_watching")
		ret = MOVIE_VERA_WATCHING;
	else if(movieText == "who_i_marry")
		ret = MOVIE_WHO_I_MARRY;
	else if(movieText == "why_train")
		ret = MOVIE_WHY_TRAIN;
	else if(movieText == "window_face")
		ret = MOVIE_WINDOW_FACE;
	else if(movieText == "you_couldnt_have")
		ret = MOVIE_YOU_COULDNT_HAVE;
	return ret;
}

bool MainWindow::loadSounds(const string &dataDirectory)
{
	bool ret = true;
	string soundDirectory = dataDirectory;
	soundDirectory += "sound/";
	for(int i = 0;(i < SOUND_MAX) && ret;i++)
	{
		string currentFile = soundDirectory;
		bool found = true;
		switch(i)
		{
		case SOUND_AMAZING_GRACE:
			currentFile += "amazing_grace.mp3";
			if(!QFile::exists(currentFile.c_str()))
			{
				ret = false;
				fprintf(stderr,"error loading sound: %s\n",currentFile.c_str());
			}
			break;
		case SOUND_BREATH:
			currentFile += "breath.mp3";
			if(!QFile::exists(currentFile.c_str()))
			{
				ret = false;
				fprintf(stderr,"error loading sound: %s\n",currentFile.c_str());
			}
			break;
		case SOUND_BROKEN_PHONE:
			currentFile += "broken_phone.mp3";
			if(!QFile::exists(currentFile.c_str()))
			{
				ret = false;
				fprintf(stderr,"error loading sound: %s\n",currentFile.c_str());
			}
			break;
		case SOUND_BUZZER:
			currentFile += "buzzer.mp3";
			if(!QFile::exists(currentFile.c_str()))
			{
				ret = false;
				fprintf(stderr,"error loading sound: %s\n",currentFile.c_str());
			}
			break;
		case SOUND_CARD_FLIP:
			currentFile += "card_flip.mp3";
			if(!QFile::exists(currentFile.c_str()))
			{
				ret = false;
				fprintf(stderr,"error loading sound: %s\n",currentFile.c_str());
			}
			break;
		case SOUND_CASH_REGISTER:
			currentFile += "cash_register.mp3";
			if(!QFile::exists(currentFile.c_str()))
			{
				ret = false;
				fprintf(stderr,"error loading sound: %s\n",currentFile.c_str());
			}
			break;
		case SOUND_CHORD:
			currentFile += "chord.mp3";
			if(!QFile::exists(currentFile.c_str()))
			{
				ret = false;
				fprintf(stderr,"error loading sound: %s\n",currentFile.c_str());
			}
			break;
		case SOUND_CLEAR_THROAT:
			currentFile += "clear_throat.mp3";
			if(!QFile::exists(currentFile.c_str()))
			{
				ret = false;
				fprintf(stderr,"error loading sound: %s\n",currentFile.c_str());
			}
			break;
		case SOUND_COMMOTION:
			currentFile += "commotion.mp3";
			if(!QFile::exists(currentFile.c_str()))
			{
				ret = false;
				fprintf(stderr,"error loading sound: %s\n",currentFile.c_str());
			}
			break;
		case SOUND_CRICKETS:
			currentFile += "crickets.mp3";
			if(!QFile::exists(currentFile.c_str()))
			{
				ret = false;
				fprintf(stderr,"error loading sound: %s\n",currentFile.c_str());
			}
			break;
		case SOUND_CROW:
			currentFile += "crow.mp3";
			if(!QFile::exists(currentFile.c_str()))
			{
				ret = false;
				fprintf(stderr,"error loading sound: %s\n",currentFile.c_str());
			}
			break;
		case SOUND_CROWD_LAUGH:
			currentFile += "crowd_laugh.mp3";
			if(!QFile::exists(currentFile.c_str()))
			{
				ret = false;
				fprintf(stderr,"error loading sound: %s\n",currentFile.c_str());
			}
			break;
		case SOUND_CROWD_TALKING:
			currentFile += "crowd_talking.mp3";
			if(!QFile::exists(currentFile.c_str()))
			{
				ret = false;
				fprintf(stderr,"error loading sound: %s\n",currentFile.c_str());
			}
			break;
		case SOUND_DIAL_TONE:
			currentFile += "dial_tone.mp3";
			if(!QFile::exists(currentFile.c_str()))
			{
				ret = false;
				fprintf(stderr,"error loading sound: %s\n",currentFile.c_str());
			}
			break;
		case SOUND_DINER_EATING:
			currentFile += "diner_eating.mp3";
			if(!QFile::exists(currentFile.c_str()))
			{
				ret = false;
				fprintf(stderr,"error loading sound: %s\n",currentFile.c_str());
			}
			break;
		case SOUND_DINER_SONG:
			currentFile += "diner_song.mp3";
			if(!QFile::exists(currentFile.c_str()))
			{
				ret = false;
				fprintf(stderr,"error loading sound: %s\n",currentFile.c_str());
			}
			break;
		case SOUND_DIRK_LAUGH:
			currentFile += "dirk_laugh.mp3";
			if(!QFile::exists(currentFile.c_str()))
			{
				ret = false;
				fprintf(stderr,"error loading sound: %s\n",currentFile.c_str());
			}
			break;
		case SOUND_DIRK_SCREAM:
			currentFile += "dirk_scream.mp3";
			if(!QFile::exists(currentFile.c_str()))
			{
				ret = false;
				fprintf(stderr,"error loading sound: %s\n",currentFile.c_str());
			}
			break;
		case SOUND_DOOR_KNOCK:
			currentFile += "door_knock.mp3";
			if(!QFile::exists(currentFile.c_str()))
			{
				ret = false;
				fprintf(stderr,"error loading sound: %s\n",currentFile.c_str());
			}
			break;
		case SOUND_DRINKING_SONG:
			currentFile += "drinking_song.mp3";
			if(!QFile::exists(currentFile.c_str()))
			{
				ret = false;
				fprintf(stderr,"error loading sound: %s\n",currentFile.c_str());
			}
			break;
		case SOUND_EVELYN_CRY:
			currentFile += "evelyn_cry.mp3";
			if(!QFile::exists(currentFile.c_str()))
			{
				ret = false;
				fprintf(stderr,"error loading sound: %s\n",currentFile.c_str());
			}
			break;
		case SOUND_EVELYN_FOOL_SONG:
			currentFile += "evelyn_fool_song.mp3";
			if(!QFile::exists(currentFile.c_str()))
			{
				ret = false;
				fprintf(stderr,"error loading sound: %s\n",currentFile.c_str());
			}
			break;
		case SOUND_FLOOR_CREAK:
			currentFile += "floor_creak.mp3";
			if(!QFile::exists(currentFile.c_str()))
			{
				ret = false;
				fprintf(stderr,"error loading sound: %s\n",currentFile.c_str());
			}
			break;
		case SOUND_FOOTSTEPS:
			currentFile += "footsteps.mp3";
			if(!QFile::exists(currentFile.c_str()))
			{
				ret = false;
				fprintf(stderr,"error loading sound: %s\n",currentFile.c_str());
			}
			break;
		case SOUND_GHOST:
			currentFile += "ghost.mp3";
			if(!QFile::exists(currentFile.c_str()))
			{
				ret = false;
				fprintf(stderr,"error loading sound: %s\n",currentFile.c_str());
			}
			break;
		case SOUND_GUN_COCK:
			currentFile += "gun_cock.mp3";
			if(!QFile::exists(currentFile.c_str()))
			{
				ret = false;
				fprintf(stderr,"error loading sound: %s\n",currentFile.c_str());
			}
			break;
		case SOUND_GUNSHOT:
			currentFile += "gunshot.mp3";
			if(!QFile::exists(currentFile.c_str()))
			{
				ret = false;
				fprintf(stderr,"error loading sound: %s\n",currentFile.c_str());
			}
			break;
		case SOUND_HISS:
			currentFile += "hiss.mp3";
			if(!QFile::exists(currentFile.c_str()))
			{
				ret = false;
				fprintf(stderr,"error loading sound: %s\n",currentFile.c_str());
			}
			break;
		case SOUND_HOTEL_BELL:
			currentFile += "hotel_bell.mp3";
			if(!QFile::exists(currentFile.c_str()))
			{
				ret = false;
				fprintf(stderr,"error loading sound: %s\n",currentFile.c_str());
			}
			break;
		case SOUND_HOTEL_DINNER_SONG:
			currentFile += "hotel_dinner_song.mp3";
			if(!QFile::exists(currentFile.c_str()))
			{
				ret = false;
				fprintf(stderr,"error loading sound: %s\n",currentFile.c_str());
			}
			break;
		case SOUND_HOWL:
			currentFile += "howl.mp3";
			if(!QFile::exists(currentFile.c_str()))
			{
				ret = false;
				fprintf(stderr,"error loading sound: %s\n",currentFile.c_str());
			}
			break;
		case SOUND_IDEA:
			currentFile += "idea.mp3";
			if(!QFile::exists(currentFile.c_str()))
			{
				ret = false;
				fprintf(stderr,"error loading sound: %s\n",currentFile.c_str());
			}
			break;
		case SOUND_JACOB_SONG:
			currentFile += "jacob_song.mp3";
			if(!QFile::exists(currentFile.c_str()))
			{
				ret = false;
				fprintf(stderr,"error loading sound: %s\n",currentFile.c_str());
			}
			break;
		case SOUND_JAIL_DOOR:
			currentFile += "jail_door.mp3";
			if(!QFile::exists(currentFile.c_str()))
			{
				ret = false;
				fprintf(stderr,"error loading sound: %s\n",currentFile.c_str());
			}
			break;
		case SOUND_KEY_CLICK:
			currentFile += "key_click.mp3";
			if(!QFile::exists(currentFile.c_str()))
			{
				ret = false;
				fprintf(stderr,"error loading sound: %s\n",currentFile.c_str());
			}
			break;
		case SOUND_LITTLE_MARY:
			currentFile += "little_mary.mp3";
			if(!QFile::exists(currentFile.c_str()))
			{
				ret = false;
				fprintf(stderr,"error loading sound: %s\n",currentFile.c_str());
			}
			break;
		case SOUND_LITTLE_MARY_2:
			currentFile += "little_mary_2.mp3";
			if(!QFile::exists(currentFile.c_str()))
			{
				ret = false;
				fprintf(stderr,"error loading sound: %s\n",currentFile.c_str());
			}
			break;
		case SOUND_MACHINE_GUN:
			currentFile += "machine_gun.mp3";
			if(!QFile::exists(currentFile.c_str()))
			{
				ret = false;
				fprintf(stderr,"error loading sound: %s\n",currentFile.c_str());
			}
			break;
		case SOUND_MARY_HAS_BLUES_SONG:
			currentFile += "mary_has_blues_song.mp3";
			if(!QFile::exists(currentFile.c_str()))
			{
				ret = false;
				fprintf(stderr,"error loading sound: %s\n",currentFile.c_str());
			}
			break;
		case SOUND_MARY_HOTEL_DOOR:
			currentFile += "mary_hotel_door.mp3";
			if(!QFile::exists(currentFile.c_str()))
			{
				ret = false;
				fprintf(stderr,"error loading sound: %s\n",currentFile.c_str());
			}
			break;
		case SOUND_MARY_LAUGH:
			currentFile += "mary_laugh.mp3";
			if(!QFile::exists(currentFile.c_str()))
			{
				ret = false;
				fprintf(stderr,"error loading sound: %s\n",currentFile.c_str());
			}
			break;
		case SOUND_MARY_RESOURCEFUL:
			currentFile += "mary_resourceful.mp3";
			if(!QFile::exists(currentFile.c_str()))
			{
				ret = false;
				fprintf(stderr,"error loading sound: %s\n",currentFile.c_str());
			}
			break;
		case SOUND_MARY_SCREAM:
			currentFile += "mary_scream.mp3";
			if(!QFile::exists(currentFile.c_str()))
			{
				ret = false;
				fprintf(stderr,"error loading sound: %s\n",currentFile.c_str());
			}
			break;
		case SOUND_MARY_STRONG_SONG:
			currentFile += "mary_strong_song.mp3";
			if(!QFile::exists(currentFile.c_str()))
			{
				ret = false;
				fprintf(stderr,"error loading sound: %s\n",currentFile.c_str());
			}
			break;
		case SOUND_MARY_WEAK_SONG:
			currentFile += "mary_weak_song.mp3";
			if(!QFile::exists(currentFile.c_str()))
			{
				ret = false;
				fprintf(stderr,"error loading sound: %s\n",currentFile.c_str());
			}
			break;
		case SOUND_MARY_YAWN:
			currentFile += "mary_yawn.mp3";
			if(!QFile::exists(currentFile.c_str()))
			{
				ret = false;
				fprintf(stderr,"error loading sound: %s\n",currentFile.c_str());
			}
			break;
		case SOUND_MELANCHOLY:
			currentFile += "melancholy.mp3";
			if(!QFile::exists(currentFile.c_str()))
			{
				ret = false;
				fprintf(stderr,"error loading sound: %s\n",currentFile.c_str());
			}
			break;
		case SOUND_MONA_LAUGH:
			currentFile += "mona_laugh.mp3";
			if(!QFile::exists(currentFile.c_str()))
			{
				ret = false;
				fprintf(stderr,"error loading sound: %s\n",currentFile.c_str());
			}
			break;
		case SOUND_MONA_SONG:
			currentFile += "mona_song.mp3";
			if(!QFile::exists(currentFile.c_str()))
			{
				ret = false;
				fprintf(stderr,"error loading sound: %s\n",currentFile.c_str());
			}
			break;
		case SOUND_MOTHERS_GRAVE_SONG:
			currentFile += "mothers_grave_song.mp3";
			if(!QFile::exists(currentFile.c_str()))
			{
				ret = false;
				fprintf(stderr,"error loading sound: %s\n",currentFile.c_str());
			}
			break;
		case SOUND_MOTHERS_GRAVE_SONG3:
			currentFile += "mothers_grave_song3.mp3";
			if(!QFile::exists(currentFile.c_str()))
			{
				ret = false;
				fprintf(stderr,"error loading sound: %s\n",currentFile.c_str());
			}
			break;
		case SOUND_MOTHERS_GRAVE_SONG4:
			currentFile += "mothers_grave_song4.mp3";
			if(!QFile::exists(currentFile.c_str()))
			{
				ret = false;
				fprintf(stderr,"error loading sound: %s\n",currentFile.c_str());
			}
			break;
		case SOUND_OUTDOOR:
			currentFile += "outdoor.mp3";
			if(!QFile::exists(currentFile.c_str()))
			{
				ret = false;
				fprintf(stderr,"error loading sound: %s\n",currentFile.c_str());
			}
			break;
		case SOUND_PAPER_RUSTLE:
			currentFile += "paper_rustle.mp3";
			if(!QFile::exists(currentFile.c_str()))
			{
				ret = false;
				fprintf(stderr,"error loading sound: %s\n",currentFile.c_str());
			}
			break;
		case SOUND_PHONE_RING:
			currentFile += "phone_ring.mp3";
			if(!QFile::exists(currentFile.c_str()))
			{
				ret = false;
				fprintf(stderr,"error loading sound: %s\n",currentFile.c_str());
			}
			break;
		case SOUND_PHONE_TONE:
			currentFile += "phone_tone.mp3";
			if(!QFile::exists(currentFile.c_str()))
			{
				ret = false;
				fprintf(stderr,"error loading sound: %s\n",currentFile.c_str());
			}
			break;
		case SOUND_POLICE_SIREN:
			currentFile += "police_siren.mp3";
			if(!QFile::exists(currentFile.c_str()))
			{
				ret = false;
				fprintf(stderr,"error loading sound: %s\n",currentFile.c_str());
			}
			break;
		case SOUND_POP_CORK:
			currentFile += "pop_cork.mp3";
			if(!QFile::exists(currentFile.c_str()))
			{
				ret = false;
				fprintf(stderr,"error loading sound: %s\n",currentFile.c_str());
			}
			break;
		case SOUND_PORKCHOP_DROP:
			currentFile += "porkchop_drop.mp3";
			if(!QFile::exists(currentFile.c_str()))
			{
				ret = false;
				fprintf(stderr,"error loading sound: %s\n",currentFile.c_str());
			}
			break;
		case SOUND_POURING_BEER:
			currentFile += "pouring_beer.mp3";
			if(!QFile::exists(currentFile.c_str()))
			{
				ret = false;
				fprintf(stderr,"error loading sound: %s\n",currentFile.c_str());
			}
			break;
		case SOUND_PUNCH_LINE:
			currentFile += "punch_line.mp3";
			if(!QFile::exists(currentFile.c_str()))
			{
				ret = false;
				fprintf(stderr,"error loading sound: %s\n",currentFile.c_str());
			}
			break;
		case SOUND_RAIN:
			currentFile += "rain.mp3";
			if(!QFile::exists(currentFile.c_str()))
			{
				ret = false;
				fprintf(stderr,"error loading sound: %s\n",currentFile.c_str());
			}
			break;
		case SOUND_SEASHORE:
			currentFile += "seashore.mp3";
			if(!QFile::exists(currentFile.c_str()))
			{
				ret = false;
				fprintf(stderr,"error loading sound: %s\n",currentFile.c_str());
			}
			break;
		case SOUND_SHERIFF_SUE_SONG:
			currentFile += "sheriff_sue_song.mp3";
			if(!QFile::exists(currentFile.c_str()))
			{
				ret = false;
				fprintf(stderr,"error loading sound: %s\n",currentFile.c_str());
			}
			break;
		case SOUND_SIGH:
			currentFile += "sigh.mp3";
			if(!QFile::exists(currentFile.c_str()))
			{
				ret = false;
				fprintf(stderr,"error loading sound: %s\n",currentFile.c_str());
			}
			break;
		case SOUND_STARLINGS:
			currentFile += "starlings.mp3";
			if(!QFile::exists(currentFile.c_str()))
			{
				ret = false;
				fprintf(stderr,"error loading sound: %s\n",currentFile.c_str());
			}
			break;
		case SOUND_STRIPPER_DRUM:
			currentFile += "stripper_drum.mp3";
			if(!QFile::exists(currentFile.c_str()))
			{
				ret = false;
				fprintf(stderr,"error loading sound: %s\n",currentFile.c_str());
			}
			break;
		case SOUND_STRIPPER_SONG:
			currentFile += "stripper_song.mp3";
			if(!QFile::exists(currentFile.c_str()))
			{
				ret = false;
				fprintf(stderr,"error loading sound: %s\n",currentFile.c_str());
			}
			break;
		case SOUND_SWEDISH_ROCKABILLY:
			currentFile += "swedish_rockabilly.mp3";
			if(!QFile::exists(currentFile.c_str()))
			{
				ret = false;
				fprintf(stderr,"error loading sound: %s\n",currentFile.c_str());
			}
			break;
		case SOUND_TADA:
			currentFile += "tada.mp3";
			if(!QFile::exists(currentFile.c_str()))
			{
				ret = false;
				fprintf(stderr,"error loading sound: %s\n",currentFile.c_str());
			}
			break;
		case SOUND_TELL_MARY_SONG:
			currentFile += "tell_mary_song.mp3";
			if(!QFile::exists(currentFile.c_str()))
			{
				ret = false;
				fprintf(stderr,"error loading sound: %s\n",currentFile.c_str());
			}
			break;
		case SOUND_THUNDER:
			currentFile += "thunder.mp3";
			if(!QFile::exists(currentFile.c_str()))
			{
				ret = false;
				fprintf(stderr,"error loading sound: %s\n",currentFile.c_str());
			}
			break;
		case SOUND_TRAFFIC:
			currentFile += "traffic.mp3";
			if(!QFile::exists(currentFile.c_str()))
			{
				ret = false;
				fprintf(stderr,"error loading sound: %s\n",currentFile.c_str());
			}
			break;
		case SOUND_TRAIN_ROCK:
			currentFile += "train_rock.mp3";
			if(!QFile::exists(currentFile.c_str()))
			{
				ret = false;
				fprintf(stderr,"error loading sound: %s\n",currentFile.c_str());
			}
			break;
		case SOUND_TRAIN_SONG_1:
			currentFile += "train_song_1.mp3";
			if(!QFile::exists(currentFile.c_str()))
			{
				ret = false;
				fprintf(stderr,"error loading sound: %s\n",currentFile.c_str());
			}
			break;
		case SOUND_TRAIN_SONG_2:
			currentFile += "train_song_2.mp3";
			if(!QFile::exists(currentFile.c_str()))
			{
				ret = false;
				fprintf(stderr,"error loading sound: %s\n",currentFile.c_str());
			}
			break;
		case SOUND_TRAIN_SONG_3:
			currentFile += "train_song_3.mp3";
			if(!QFile::exists(currentFile.c_str()))
			{
				ret = false;
				fprintf(stderr,"error loading sound: %s\n",currentFile.c_str());
			}
			break;
		case SOUND_TRAIN_SONG_4:
			currentFile += "train_song_4.mp3";
			if(!QFile::exists(currentFile.c_str()))
			{
				ret = false;
				fprintf(stderr,"error loading sound: %s\n",currentFile.c_str());
			}
			break;
		case SOUND_TRAIN_SONG_5:
			currentFile += "train_song_5.mp3";
			if(!QFile::exists(currentFile.c_str()))
			{
				ret = false;
				fprintf(stderr,"error loading sound: %s\n",currentFile.c_str());
			}
			break;
		case SOUND_TRAIN_SONG_6:
			currentFile += "train_song_6.mp3";
			if(!QFile::exists(currentFile.c_str()))
			{
				ret = false;
				fprintf(stderr,"error loading sound: %s\n",currentFile.c_str());
			}
			break;
		case SOUND_TRAIN_SOUND:
			currentFile += "train_sound.mp3";
			if(!QFile::exists(currentFile.c_str()))
			{
				ret = false;
				fprintf(stderr,"error loading sound: %s\n",currentFile.c_str());
			}
			break;
		case SOUND_TROUBLE_SONG:
			currentFile += "trouble_song.mp3";
			if(!QFile::exists(currentFile.c_str()))
			{
				ret = false;
				fprintf(stderr,"error loading sound: %s\n",currentFile.c_str());
			}
			break;
		case SOUND_TYPEWRITER:
			currentFile += "typewriter.mp3";
			if(!QFile::exists(currentFile.c_str()))
			{
				ret = false;
				fprintf(stderr,"error loading sound: %s\n",currentFile.c_str());
			}
			break;
		case SOUND_UNDERESTIMATE:
			currentFile += "underestimate.mp3";
			if(!QFile::exists(currentFile.c_str()))
			{
				ret = false;
				fprintf(stderr,"error loading sound: %s\n",currentFile.c_str());
			}
			break;
		case SOUND_WIND:
			currentFile += "wind.mp3";
			if(!QFile::exists(currentFile.c_str()))
			{
				ret = false;
				fprintf(stderr,"error loading sound: %s\n",currentFile.c_str());
			}
			break;
		case SOUND_WOMAN_CRYING:
			currentFile += "woman_crying.mp3";
			if(!QFile::exists(currentFile.c_str()))
			{
				ret = false;
				fprintf(stderr,"error loading sound: %s\n",currentFile.c_str());
			}
			break;
		case SOUND_WOMAN_SIGH:
			currentFile += "woman_sigh.mp3";
			if(!QFile::exists(currentFile.c_str()))
			{
				ret = false;
				fprintf(stderr,"error loading sound: %s\n",currentFile.c_str());
			}
			break;
		case SOUND_WRITING:
			currentFile += "writing.mp3";
			if(!QFile::exists(currentFile.c_str()))
			{
				ret = false;
				fprintf(stderr,"error loading sound: %s\n",currentFile.c_str());
			}
			break;
		default:
			found = false;
			ret = false;
			fprintf(stderr,"error unreferenced sound: %d\n",i);
			break;
		}
		if(found)
			m_soundVector.push_back(currentFile);
	}
	return ret;
}

int MainWindow::getSoundOffsetFromText(const string &soundName)
{
	int ret = -1;
	if(soundName == "amazing_grace")
		ret = SOUND_AMAZING_GRACE;
	else if(soundName == "breath")
		ret = SOUND_BREATH;
	else if(soundName == "broken_phone")
		ret = SOUND_BROKEN_PHONE;
	else if(soundName == "buzzer")
		ret = SOUND_BUZZER;
	else if(soundName == "card_flip")
		ret = SOUND_CARD_FLIP;
	else if(soundName == "cash_register")
		ret = SOUND_CASH_REGISTER;
	else if(soundName == "chord")
		ret = SOUND_CHORD;
	else if(soundName == "clear_throat")
		ret = SOUND_CLEAR_THROAT;
	else if(soundName == "commotion")
		ret = SOUND_COMMOTION;
	else if(soundName == "crickets")
		ret = SOUND_CRICKETS;
	else if(soundName == "crow")
		ret = SOUND_CROW;
	else if(soundName == "crowd_laugh")
		ret = SOUND_CROWD_LAUGH;
	else if(soundName == "crowd_talking")
		ret = SOUND_CROWD_TALKING;
	else if(soundName == "dial_tone")
		ret = SOUND_DIAL_TONE;
	else if(soundName == "diner_eating")
		ret = SOUND_DINER_EATING;
	else if(soundName == "diner_song")
		ret = SOUND_DINER_SONG;
	else if(soundName == "dirk_laugh")
		ret = SOUND_DIRK_LAUGH;
	else if(soundName == "dirk_scream")
		ret = SOUND_DIRK_SCREAM;
	else if(soundName == "door_knock")
		ret = SOUND_DOOR_KNOCK;
	else if(soundName == "drinking_song")
		ret = SOUND_DRINKING_SONG;
	else if(soundName == "evelyn_cry")
		ret = SOUND_EVELYN_CRY;
	else if(soundName == "evelyn_fool_song")
		ret = SOUND_EVELYN_FOOL_SONG;
	else if(soundName == "floor_creak")
		ret = SOUND_FLOOR_CREAK;
	else if(soundName == "footsteps")
		ret = SOUND_FOOTSTEPS;
	else if(soundName == "ghost")
		ret = SOUND_GHOST;
	else if(soundName == "gun_cock")
		ret = SOUND_GUN_COCK;
	else if(soundName == "gunshot")
		ret = SOUND_GUNSHOT;
	else if(soundName == "hiss")
		ret = SOUND_HISS;
	else if(soundName == "hotel_bell")
		ret = SOUND_HOTEL_BELL;
	else if(soundName == "hotel_dinner_song")
		ret = SOUND_HOTEL_DINNER_SONG;
	else if(soundName == "howl")
		ret = SOUND_HOWL;
	else if(soundName == "idea")
		ret = SOUND_IDEA;
	else if(soundName == "jacob_song")
		ret = SOUND_JACOB_SONG;
	else if(soundName == "jail_door")
		ret = SOUND_JAIL_DOOR;
	else if(soundName == "key_click")
		ret = SOUND_KEY_CLICK;
	else if(soundName == "little_mary")
		ret = SOUND_LITTLE_MARY;
	else if(soundName == "little_mary_2")
		ret = SOUND_LITTLE_MARY_2;
	else if(soundName == "machine_gun")
		ret = SOUND_MACHINE_GUN;
	else if(soundName == "mary_has_blues_song")
		ret = SOUND_MARY_HAS_BLUES_SONG;
	else if(soundName == "mary_hotel_door")
		ret = SOUND_MARY_HOTEL_DOOR;
	else if(soundName == "mary_laugh")
		ret = SOUND_MARY_LAUGH;
	else if(soundName == "mary_resourceful")
		ret = SOUND_MARY_RESOURCEFUL;
	else if(soundName == "mary_scream")
		ret = SOUND_MARY_SCREAM;
	else if(soundName == "mary_strong_song")
		ret = SOUND_MARY_STRONG_SONG;
	else if(soundName == "mary_weak_song")
		ret = SOUND_MARY_WEAK_SONG;
	else if(soundName == "mary_yawn")
		ret = SOUND_MARY_YAWN;
	else if(soundName == "melancholy")
		ret = SOUND_MELANCHOLY;
	else if(soundName == "mona_laugh")
		ret = SOUND_MONA_LAUGH;
	else if(soundName == "mona_song")
		ret = SOUND_MONA_SONG;
	else if(soundName == "mothers_grave_song")
		ret = SOUND_MOTHERS_GRAVE_SONG;
	else if(soundName == "mothers_grave_song3")
		ret = SOUND_MOTHERS_GRAVE_SONG3;
	else if(soundName == "mothers_grave_song4")
		ret = SOUND_MOTHERS_GRAVE_SONG4;
	else if(soundName == "outdoor")
		ret = SOUND_OUTDOOR;
	else if(soundName == "paper_rustle")
		ret = SOUND_PAPER_RUSTLE;
	else if(soundName == "phone_ring")
		ret = SOUND_PHONE_RING;
	else if(soundName == "phone_tone")
		ret = SOUND_PHONE_TONE;
	else if(soundName == "police_siren")
		ret = SOUND_POLICE_SIREN;
	else if(soundName == "pop_cork")
		ret = SOUND_POP_CORK;
	else if(soundName == "porkchop_drop")
		ret = SOUND_PORKCHOP_DROP;
	else if(soundName == "pouring_beer")
		ret = SOUND_POURING_BEER;
	else if(soundName == "punch_line")
		ret = SOUND_PUNCH_LINE;
	else if(soundName == "rain")
		ret = SOUND_RAIN;
	else if(soundName == "seashore")
		ret = SOUND_SEASHORE;
	else if(soundName == "sheriff_sue_song")
		ret = SOUND_SHERIFF_SUE_SONG;
	else if(soundName == "sigh")
		ret = SOUND_SIGH;
	else if(soundName == "starlings")
		ret = SOUND_STARLINGS;
	else if(soundName == "stripper_drum")
		ret = SOUND_STRIPPER_DRUM;
	else if(soundName == "stripper_song")
		ret = SOUND_STRIPPER_SONG;
	else if(soundName == "swedish_rockabilly")
		ret = SOUND_SWEDISH_ROCKABILLY;
	else if(soundName == "tada")
		ret = SOUND_TADA;
	else if(soundName == "tell_mary_song")
		ret = SOUND_TELL_MARY_SONG;
	else if(soundName == "thunder")
		ret = SOUND_THUNDER;
	else if(soundName == "traffic")
		ret = SOUND_TRAFFIC;
	else if(soundName == "train_rock")
		ret = SOUND_TRAIN_ROCK;
	else if(soundName == "train_song_1")
		ret = SOUND_TRAIN_SONG_1;
	else if(soundName == "train_song_2")
		ret = SOUND_TRAIN_SONG_2;
	else if(soundName == "train_song_3")
		ret = SOUND_TRAIN_SONG_3;
	else if(soundName == "train_song_4")
		ret = SOUND_TRAIN_SONG_4;
	else if(soundName == "train_song_5")
		ret = SOUND_TRAIN_SONG_5;
	else if(soundName == "train_song_6")
		ret = SOUND_TRAIN_SONG_6;
	else if(soundName == "train_sound")
		ret = SOUND_TRAIN_SOUND;
	else if(soundName == "trouble_song")
		ret = SOUND_TROUBLE_SONG;
	else if(soundName == "typewriter")
		ret = SOUND_TYPEWRITER;
	else if(soundName == "underestimate")
		ret = SOUND_UNDERESTIMATE;
	else if(soundName == "wind")
		ret = SOUND_WIND;
	else if(soundName == "woman_crying")
		ret = SOUND_WOMAN_CRYING;
	else if(soundName == "woman_sigh")
		ret = SOUND_WOMAN_SIGH;
	else if(soundName == "writing")
		ret = SOUND_WRITING;
	return ret;
}

void MainWindow::advanceScenario()
{
	m_currentSceneType = m_currentGameScenario.nextScene(m_currentGameSceneOffset);
	performScene();
}

void MainWindow::gamePlayButtonClicked()
{
	m_skipping = false;
	m_mainStack->setCurrentWidget(m_gameCentralWidget);
	m_audioVideoMonitorThreadMutex.lock();
	m_audioVideoWarnDisplayed = false;
	m_audioVideoMonitorThreadMutex.unlock();
	signalStartScenario(SCENARIO_START);
}

void MainWindow::titleButtonClicked()
{
	m_accelerateConversation = true;
	QMessageBox mb(QMessageBox::Question,"Title Page","Go to the title page?",QMessageBox::Yes | QMessageBox::No,this);
	m_conversationLabel->releaseKeyboardToChild(true);
	int response = mb.exec();
	m_conversationLabel->releaseKeyboardToChild(false);
	if(response == QMessageBox::Yes)
	{
		m_mainStack->setCurrentWidget(m_titleCentralWidget);
		m_accelerateConversation = true;
		m_currentConversationString = "";
		m_currentConversationOffset = 0;
		QPixmap blackPixmap(m_backgroundVector.at(BACKGROUND_BLACK).c_str());
		blackPixmap.scaled(IMAGE_MOVIE_MINIMUM_WIDTH,IMAGE_MOVIE_MINIMUM_HEIGHT);
		m_pixmapDisplayLabel->setPixmap(blackPixmap);
		m_conversationLabel->setText("");
		if(m_saveLoadContainer.isLoadAvailable())
			m_gameLoadButton->setEnabled(true);
		else
			m_gameLoadButton->setEnabled(false);
	}
}

bool MainWindow::parseScenario(const string &currentFile,GameScenario &gameScenario)
{
	bool ret = true;
	QFile file(currentFile.c_str());
	if(file.exists())
	{
		if(file.open(QIODevice::ReadOnly))
		{
			QXmlStreamReader xml(&file);
			bool done = false;
			while(!done)
			{
				switch(xml.readNext())
				{
				case QXmlStreamReader::NoToken:
					ret = false;
					done = true;
					break;
				case QXmlStreamReader::Invalid:
					ret = false;
					done = true;
					break;
				case QXmlStreamReader::StartDocument:
					break;
				case QXmlStreamReader::EndDocument:
					done = true;
					break;
				case QXmlStreamReader::StartElement:
					{
						bool validElement = false;
						if(!xml.name().compare("sprite"))
						{
							validElement = true;
							if(!parseSprite(xml,gameScenario))
							{
								ret = false;
								done = true;
								fprintf(stderr,"error invalid sprite in scenario: %s\n",currentFile.c_str());
							}
						}
						if(!xml.name().compare("conversation"))
						{
							validElement = true;
							if(!parseConversation(xml,gameScenario))
							{
								ret = false;
								done = true;
								fprintf(stderr,"error invalid conversation in scenario: %s\n",currentFile.c_str());
							}
						}
						if(!xml.name().compare("movie"))
						{
							validElement = true;
							if(!parseMovie(xml,gameScenario))
							{
								ret = false;
								done = true;
								fprintf(stderr,"error invalid movie in scenario: %s\n",currentFile.c_str());
							}
						}
						if(!xml.name().compare("user_choice"))
						{
							validElement = true;
							if(!parseUserChoice(xml,gameScenario))
							{
								ret = false;
								done = true;
								fprintf(stderr,"error invalid user choice in scenario: %s\n",currentFile.c_str());
							}
						}
						if(!xml.name().compare("sound"))
						{
							validElement = true;
							if(!parseSound(xml,gameScenario))
							{
								ret = false;
								done = true;
								fprintf(stderr,"error invalid sound in scenario: %s\n",currentFile.c_str());
							}
						}
						if(!xml.name().compare("start_scenario"))
						{
							validElement = true;
							if(!parseStartScenario(xml,gameScenario))
							{
								ret = false;
								done = true;
								fprintf(stderr,"error invalid start scenario in scenario: %s\n",currentFile.c_str());
							}
						}
						if(!xml.name().compare("game_over"))
						{
							validElement = true;
							if(!parseGameOver(xml,gameScenario))
							{
								ret = false;
								done = true;
								fprintf(stderr,"error invalid game over in scenario: %s\n",currentFile.c_str());
							}
						}
						if(!xml.name().compare("trivia"))
						{
							validElement = true;
							if(!parseTrivia(xml,gameScenario))
							{
								ret = false;
								done = true;
								fprintf(stderr,"error invalid trivia in scenario: %s\n",currentFile.c_str());
							}
						}
						if(!xml.name().compare("trivia_return"))
						{
							validElement = true;
							if(!parseTriviaReturn(xml,gameScenario))
							{
								ret = false;
								done = true;
								fprintf(stderr,"error invalid trivia return in scenario: %s\n",currentFile.c_str());
							}
						}
						else if(!xml.name().compare("body"))
							validElement = true;
						else if(!xml.name().compare("game_scene"))
							validElement = true;
						if(!validElement)
						{
							ret = false;
							done = true;
							fprintf(stderr,"error invalid element: %s in scenario: %s\n",xml.name().toString().toStdString().c_str(),currentFile.c_str());
						}
					}
					break;
				case QXmlStreamReader::EndElement:
					break;
				case QXmlStreamReader::Characters:
					break;
				default:
					ret = false;
					done = true;
					fprintf(stderr,"error unexpected xml read type in scenario: %s\n",currentFile.c_str());
					break;
				}
			}
			file.close();
		}
		else  // could not open
		{
			ret = false;
			fprintf(stderr,"error could not open scenario: %s\n",currentFile.c_str());
		}
	}
	else  // file does not exist
	{
		ret = false;
		fprintf(stderr,"error scenario: %s does not exsist\n",currentFile.c_str());
	}
	return ret;
}

bool MainWindow::parseSprite(QXmlStreamReader &xml,GameScenario &gameScenario)
{
	bool ret = true;
	GameScene gameScene(GameScene::SCENE_SPRITE);
	bool done = false;
	while(!done)
	{
		switch(xml.readNext())
		{
		case QXmlStreamReader::StartElement:
			{
				bool validElement = false;
				if(!xml.name().compare("background"))
				{
					validElement = true;
					string backgroundText = xml.readElementText().toAscii().data();
					int backgroundOffset = getBackgroundOffsetFromText(backgroundText);
					if(backgroundOffset >= 0)
						gameScene.setBackgroundOffset(backgroundOffset);
					else
					{
						ret = false;
						done = true;
						fprintf(stderr,"error invalid background: %s\n",backgroundText.c_str());
					}

				}
				else if(!xml.name().compare("sprite_info"))
				{
					validElement = true;
					if(!parseSpriteInfo(xml,gameScene))
					{
						ret = false;
						done = true;
						fprintf(stderr,"error invalid sprite\n");
					}
				}
				if(!validElement)
				{
					ret = false;
					done = true;
					fprintf(stderr,"error invalid element: %s\n",xml.name().toString().toStdString().c_str());
				}
			}
			break;
		case QXmlStreamReader::Characters:
			break;
		case QXmlStreamReader::EndElement:
			done = true;
			break;
		default:
			done = true;
			ret = false;
			fprintf(stderr,"error unexpected xml read type\n");
			break;
		}
	}
	if(ret)
		gameScenario.addGameScene(gameScene);
	return ret;
}

bool MainWindow::parseSpriteInfo(QXmlStreamReader &xml,GameScene &gameScene)
{
	bool ret = true;
	int spriteOffset = -1;
	int verticalPosition = -1;
	int horizontalStartingPosition = -1;
	int horizontalEndingPosition = -1;
	int direction = -1;
	bool done = false;
	while(!done)
	{
		switch(xml.readNext())
		{
		case QXmlStreamReader::StartElement:
			{
				bool validElement = false;
				if(!xml.name().compare("name"))
				{
					validElement = true;
					string spriteName = xml.readElementText().toStdString();
					spriteOffset = getSpriteOffsetFromText(spriteName);
					if(spriteOffset < 0)
					{
						ret = false;
						done = true;
						fprintf(stderr,"error invalid sprite: %s\n",spriteName.c_str());
					}
				}
				else if(!xml.name().compare("vertical"))
				{
					validElement = true;
					string verticalString = xml.readElementText().toStdString();
					verticalPosition = getVerticalPositionFromText(verticalString);
					if(verticalPosition < 0)
					{
						ret = false;
						done = true;
						fprintf(stderr,"error invalid sprite vertical position: %s\n",verticalString.c_str());
					}
				}
				else if(!xml.name().compare("horizontal_start"))
				{
					validElement = true;
					string horizontalString = xml.readElementText().toStdString();
					horizontalStartingPosition = getHorizontalPositionFromText(horizontalString);
					if(horizontalStartingPosition < 0)
					{
						ret = false;
						done = true;
						fprintf(stderr,"error invalid sprite horizontal starting position: %s\n",horizontalString.c_str());
					}
				}
				else if(!xml.name().compare("horizontal_end"))
				{
					validElement = true;
					string horizontalString = xml.readElementText().toStdString();
					horizontalEndingPosition = getHorizontalPositionFromText(horizontalString);
					if(horizontalEndingPosition < 0)
					{
						ret = false;
						done = true;
						fprintf(stderr,"error invalid sprite horizontal ending position: %s\n",horizontalString.c_str());
					}
				}
				else if(!xml.name().compare("direction"))
				{
					validElement = true;
					string directionString = xml.readElementText().toStdString();
					direction =  getDirectionFromText(directionString);
					if(direction < 0)
					{
						ret = false;
						done = true;
						fprintf(stderr,"error invalid sprite direction: %s\n",directionString.c_str());
					}
				}
				if(!validElement)
				{
					ret = false;
					done = true;
					fprintf(stderr,"error invalid sprite element: %s\n",xml.name().toString().toStdString().c_str());
				}
			}
			break;
		case QXmlStreamReader::Characters:
			break;
		case QXmlStreamReader::EndElement:
			done = true;
			break;
		default:
			ret = false;
			done = true;
			fprintf(stderr,"error unexpected xml read type\n");
			break;
		}
	}
	if(ret)
	{
		if(spriteOffset >= 0 &&
			verticalPosition >= 0 &&
			horizontalStartingPosition >= 0 &&
			horizontalEndingPosition >= 0 &&
			direction >= 0)
		{
			gameScene.addSprite(spriteOffset,
								verticalPosition,
								horizontalStartingPosition,
								horizontalEndingPosition,
								direction);
		}
		else
		{
			ret = false;
			fprintf(stderr,"error incomplete sprite\n");
		}
	}
	return ret;
}

int MainWindow::getVerticalPositionFromText(const string &verticalString)
{
	int ret = -1;
	if(verticalString == "top")
		ret = SPRITE_VERTICAL_TOP;
	else if(verticalString == "middle")
		ret = SPRITE_VERTICAL_MIDDLE;
	else if(verticalString == "bottom")
		ret = SPRITE_VERTICAL_BOTTOM;
	return ret;
}

int MainWindow::getHorizontalPositionFromText(const string &horizontalString)
{
	int ret = -1;
	if(horizontalString == "off_screen")
		ret = SPRITE_HORIZONTAL_OFF_SCREEN;
	else if(horizontalString == "left")
		ret = SPRITE_HORIZONTAL_LEFT;
	else if(horizontalString == "middle")
		ret = SPRITE_HORIZONTAL_MIDDLE;
	else if(horizontalString == "right")
		ret = SPRITE_HORIZONTAL_RIGHT;
	return ret;
}

int MainWindow::getDirectionFromText(const string &directionString)
{
	int ret = -1;
	if(directionString == "from_left")
		ret = SPRITE_DIRECTION_FROM_LEFT;
	if(directionString == "from_right")
		ret = SPRITE_DIRECTION_FROM_RIGHT;
	return ret;
}

bool MainWindow::parseConversation(QXmlStreamReader &xml,GameScenario &gameScenario)
{
	bool ret = true;
	string conversation;
	bool done = false;
	while(!done)
	{
		switch(xml.readNext())
		{
		case QXmlStreamReader::StartElement:
			{
				bool validElement = false;
				if(!xml.name().compare("conversation_text"))
				{
					validElement = true;
					conversation = xml.readElementText().toStdString();
				}
				if(!validElement)
				{
					ret = false;
					done = true;
					fprintf(stderr,"error invalid conversation element: %s\n",xml.name().toString().toStdString().c_str());
				}
			}
			break;
		case QXmlStreamReader::Characters:
			break;
		case QXmlStreamReader::EndElement:
			done = true;
			break;
		default:
			ret = false;
			done = true;
			fprintf(stderr,"error unexpected xml read type\n");
			break;
		}
	}
	if(ret)
	{
		GameScene gameScene(GameScene::SCENE_CONVERSATION,conversation);
		gameScenario.addGameScene(gameScene);
	}
	return ret;
}

void MainWindow::startScenario(int scenarioOffset)
{
	m_currentGameScenario = m_gameScenarioVector.at(scenarioOffset);
	m_currentSceneType = m_currentGameScenario.firstScene(m_currentGameSceneOffset);
	m_currentGameScenarioOffset = scenarioOffset;
	m_conversationLabel->setText("");
	performScene();
}

void MainWindow::performScene()
{
	if(m_currentSceneType == GameScene::SCENE_CONVERSATION)
		enableGamePlayButtons(true);
	else
		enableGamePlayButtons(false);

	if(m_currentSceneType == GameScene::SCENE_MOVIE)
	{
		m_pixmapDisplayLabel->hide();
		m_videoWidget->show();
	}
	else
	{
		m_videoWidget->hide();
		m_pixmapDisplayLabel->show();
	}

	switch(m_currentSceneType)
	{
	case GameScene::SCENE_CONVERSATION:
		performConversation();
		break;
	case GameScene::SCENE_SPRITE:
		performSprite();
		break;
	case GameScene::SCENE_MOVIE:
		performMovie();
		break;
	case GameScene::SCENE_USER_CHOICE:
		performUserChoice();
		break;
	case GameScene::SCENE_SOUND:
		performSound();
		break;
	case GameScene::SCENE_START_SCENARIO:
		performStartScenario();
		break;
	case GameScene::SCENE_GAME_OVER:
		performGameOver();
		break;
	case GameScene::SCENE_TRIVIA:
		performTrivia();
		break;
	case GameScene::SCENE_TRIVIA_RETURN:
		performTriviaReturn();
		break;
	case GameScene::SCENE_ERROR:
		{
			string message = "I have encountered a scenario error.  This may be caused by an invalid scenario closing scene type.\nThis is an unrecoverable error.";
			fprintf(stderr,"%s",message.c_str());
			QMessageBox mb(QMessageBox::Critical,"Fatal Error",message.c_str(),QMessageBox::Close,m_centralWidget);
			mb.exec();
			enableTitleButton();
		}
		break;
	default:
		{
			string message = "I have encountered an undefined game scene type: ";
			stringstream sceneStream;
			sceneStream << m_currentSceneType;
			message += sceneStream.str();
			message += ".\nThis is an unrecoverable error.";
			fprintf(stderr,"%s",message.c_str());
			QMessageBox mb(QMessageBox::Critical,"Fatal Error",message.c_str(),QMessageBox::Close,m_centralWidget);
			mb.exec();
			enableTitleButton();
			m_currentSceneType = GameScene::SCENE_ERROR;
		}
		break;
	}
}

void MainWindow::performConversation()
{
	m_skipContainer.update(m_currentGameScenarioOffset,m_currentGameSceneOffset);
	GameScene gs = m_currentGameScenario.getGameScene();
	m_currentConversationString = gs.getConversation().c_str();
	if(m_skipping)
		m_conversationWriteSkipTimer->start(WRITE_CONVERSATION_TIMER_SKIP_TIME);
	else
	{
		m_currentConversationOffset = 0;
		m_conversationComplete = false;
		m_accelerateConversation = false;
		m_conversationWriteTimer->start(WRITE_CONVERSATION_TIMER_TIME);
#ifdef CONVERSATION_KEY_CLICK
		m_conversationTypingMediaObject->stop();
		m_conversationTypingMediaObject->play();
#endif
	}
}

int MainWindow::getCurrentSceneType()
{
	return m_currentSceneType;
}

void MainWindow::performSprite()
{
	vector<QPixmap> swapVector;
	m_slideVector.swap(swapVector);
	vector<ActiveSprite> activeSpriteVector;

	GameScene gs = m_currentGameScenario.getGameScene();
	int backgroundOffset = gs.getBackgroundOffset();
	if(backgroundOffset >= 0)
	{
		m_currentBackgroundPixmap.load(m_backgroundVector.at(backgroundOffset).c_str());
		m_currentBackgroundPixmap.scaled(IMAGE_MOVIE_MINIMUM_WIDTH,IMAGE_MOVIE_MINIMUM_HEIGHT);
	}
	int spriteCount = gs.getSpriteCount();
	if(spriteCount)
	{
		for(int i = 0;i < spriteCount;i++)
		{
			int spriteOffset,verticalPosition,horizontalStartingPosition,horizontalEndingPosition,direction;
			gs.getSpriteInfo(i,spriteOffset,verticalPosition,horizontalStartingPosition,horizontalEndingPosition,direction);
			QPixmap sprite(m_spriteVector.at(spriteOffset).c_str());
			sprite.scaledToHeight(DEFAULT_SPRITE_HEIGHT);
			ActiveSprite activeSprite(sprite,verticalPosition,horizontalStartingPosition,horizontalEndingPosition,direction,m_currentBackgroundPixmap.width());
			activeSpriteVector.push_back(activeSprite);
		}
	}
	bool done = false;
	while(!done)
	{
		QPixmap pixmap = m_currentBackgroundPixmap;
		QPainter painter(&pixmap);
		done = true;
		for(int i = 0;i < (int) activeSpriteVector.size();i++)
		{
			if(!activeSpriteVector.at(i).slideSpriteInPainter(painter))
				done = false;
		}
		m_slideVector.push_back(pixmap);
	}
	m_currentVectorImage = 0;
	m_spriteSlideTimer->start(SPRITE_SLIDE_TIMER_TIME);
}

bool MainWindow::parseMovie(QXmlStreamReader &xml,GameScenario &gameScenario)
{
	bool ret = true;
	GameScene gameScene(GameScene::SCENE_MOVIE);
	bool done = false;
	while(!done)
	{
		switch(xml.readNext())
		{
		case QXmlStreamReader::StartElement:
			{
				bool validElement = false;
				if(!xml.name().compare("movie_name"))
				{
					validElement = true;
					string movieText = xml.readElementText().toStdString();
					int movieOffset = getMovieOffsetFromText(movieText);
					if(movieOffset >= 0)
						gameScene.setMovieOffset(movieOffset);
					else
					{
						ret = false;
						done = true;
						fprintf(stderr,"error invalid movie: %s\n",movieText.c_str());
					}

				}
				else if(!xml.name().compare("movie_subtitle"))
				{
					validElement = true;
					if(!parseMovieSubtitle(xml,gameScene))
					{
						ret = false;
						done = true;
						fprintf(stderr,"error invalid movie subtitle\n");
					}
				}
				if(!validElement)
				{
					ret = false;
					done = true;
					fprintf(stderr,"error invalid movie element: %s\n",xml.name().toString().toStdString().c_str());
				}
			}
			break;
		case QXmlStreamReader::Characters:
			break;
		case QXmlStreamReader::EndElement:
			done = true;
			break;
		default:
			done = true;
			ret = false;
			fprintf(stderr,"error unexpected xml read type\n");
			break;
		}
	}
	if(ret)
		gameScenario.addGameScene(gameScene);
	return ret;
}

bool MainWindow::parseMovieSubtitle(QXmlStreamReader &xml,GameScene &gameScene)
{
	bool ret = true;
	string subtitle;
	int time = -1;
	bool done = false;
	while(!done)
	{
		switch(xml.readNext())
		{
		case QXmlStreamReader::StartElement:
			{
				bool validElement = false;
				if(!xml.name().compare("subtitle"))
				{
					validElement = true;
					subtitle = xml.readElementText().toStdString();
				}
				else if(!xml.name().compare("time"))
				{
					validElement = true;
					bool ok;
					time = xml.readElementText().toInt(&ok);
					if(!ok)
					{
						ret = false;
						done = true;
						fprintf(stderr,"error invalid movie subtitle time element: %s\n",xml.name().toString().toStdString().c_str());
					}
				}
				if(!validElement)
				{
					ret = false;
					done = true;
					fprintf(stderr,"error invalid movie subtitle element: %s\n",xml.name().toString().toStdString().c_str());
				}
			}
			break;
		case QXmlStreamReader::Characters:
			break;
		case QXmlStreamReader::EndElement:
			done = true;
			break;
		default:
			ret = false;
			done = true;
			fprintf(stderr,"error unexpected xml read type\n");
			break;
		}
	}
	if(ret)
	{
		if(time >= 0)
			gameScene.addMovieSubtitle(time,subtitle);
		else
		{
			ret = false;
			fprintf(stderr,"error invalid movie subtitle time element: %d\n",time);
		}
	}
	return ret;
}

void MainWindow::performMovie()
{
	if(m_skipping)
		signalAdvanceScenario();
	else
	{
		GameScene gs = m_currentGameScenario.getGameScene();
		vector<SceneMovieSubtitleNodeInUse> swapVector;
		m_sceneMovieSubtitleNodeInUseVector.swap(swapVector);
		QString movieFile = m_movieVector.at(gs.getMovieOffset()).c_str();
		int count = gs.getMovieSubtitleCount();
		if(count)
		{
			for(int i = 0;i < count;i++)
			{
				SceneMovieSubtitleNode smsn;
				if(gs.getMovieSubtitleInfo(i,smsn))
				{
					SceneMovieSubtitleNodeInUse smsniu(smsn);
					m_sceneMovieSubtitleNodeInUseVector.push_back(smsniu);
				}
			}
		}
		m_conversationLabel->setText("");
		if(m_sceneMovieSubtitleNodeInUseVector.size())
		{
			if(m_sceneMovieSubtitleNodeInUseVector.at(0).getTimeInSeconds() == 0)
			{
				m_conversationLabel->setText(m_sceneMovieSubtitleNodeInUseVector.at(0).getSubtitleText().c_str());
				m_sceneMovieSubtitleNodeInUseVector.at(0).setUsed(true);
			}
		}
		QPixmap blackPixmap(m_backgroundVector.at(BACKGROUND_BLACK).c_str());
		blackPixmap.scaled(IMAGE_MOVIE_MINIMUM_WIDTH,IMAGE_MOVIE_MINIMUM_HEIGHT);
		m_pixmapDisplayLabel->setPixmap(blackPixmap);
		m_movieMediaObject->setCurrentSource(movieFile);
		m_movieMediaObject->play();
	}
}

void MainWindow::movieRunTime(qint64 time)
{
	for(int i = 0;i < (int) m_sceneMovieSubtitleNodeInUseVector.size();i++)
	{
		SceneMovieSubtitleNodeInUse smsniu = m_sceneMovieSubtitleNodeInUseVector.at(i);
		if(!smsniu.getUsed())
		{
			if(smsniu.getTimeInSeconds() <= time / 1000)
			{
				m_conversationLabel->setText(smsniu.getSubtitleText().c_str());
				m_sceneMovieSubtitleNodeInUseVector.at(i).setUsed(true);
			}
		}
	}
#ifdef LINUX_BUILD
	if(m_movieMediaObject->state() == Phonon::PlayingState)
	{
		if(m_movieMediaObject->remainingTime() < MOVIE_TICK_INTERVAL)
			m_movieMediaObject->stop();
	}
#endif
}

bool MainWindow::parseUserChoice(QXmlStreamReader &xml,GameScenario &gameScenario)
{
	bool ret = true;
	GameScene gameScene(GameScene::SCENE_USER_CHOICE);
	bool done = false;
	while(!done)
	{
		switch(xml.readNext())
		{
		case QXmlStreamReader::StartElement:
			{
				bool validElement = false;
				if(!xml.name().compare("choice"))
				{
					validElement = true;
					if(!parseChoice(xml,gameScene))
					{
						ret = false;
						done = true;
						fprintf(stderr,"error invalid user choice choice\n");
					}
				}
				if(!validElement)
				{
					ret = false;
					done = true;
					fprintf(stderr,"error invalid user choice element: %s\n",xml.name().toString().toStdString().c_str());
				}
			}
			break;
		case QXmlStreamReader::Characters:
			break;
		case QXmlStreamReader::EndElement:
			done = true;
			break;
		default:
			done = true;
			ret = false;
			fprintf(stderr,"error unexpected xml read type\n");
			break;
		}
	}
	if(ret)
		gameScenario.addGameScene(gameScene);
	return ret;
}

bool MainWindow::parseChoice(QXmlStreamReader &xml,GameScene &gameScene)
{
	bool ret = true;
	string choiceText;
	int scenarioOffset = -1;
	bool done = false;
	while(!done)
	{
		switch(xml.readNext())
		{
		case QXmlStreamReader::StartElement:
			{
				bool validElement = false;
				if(!xml.name().compare("choice_text"))
				{
					validElement = true;
					choiceText = xml.readElementText().toStdString();
				}
				else if(!xml.name().compare("scenario_name"))
				{
					validElement = true;
					string scenarioName = xml.readElementText().toAscii().data();
					scenarioOffset = getScenarioOffsetFromText(scenarioName);
					if(scenarioOffset < 0)
					{
						ret = false;
						done = true;
						fprintf(stderr,"error invalid user choice choice scenario: %s\n",scenarioName.c_str());
					}
				}
				if(!validElement)
				{
					ret = false;
					done = true;
					fprintf(stderr,"error invalid user choice choice element: %s\n",xml.name().toString().toStdString().c_str());
				}
			}
			break;
		case QXmlStreamReader::Characters:
			break;
		case QXmlStreamReader::EndElement:
			done = true;
			break;
		default:
			ret = false;
			done = true;
			fprintf(stderr,"error unexpected xml read type\n");
			break;
		}
	}
	if(ret)
	{
		if(scenarioOffset >= 0 && choiceText.length())
			gameScene.addUserChoice(scenarioOffset,choiceText);
		else
		{
			ret = false;
			fprintf(stderr,"error invalid user choice choice\n");
		}
	}
	return ret;
}

void MainWindow::performUserChoice()
{
	m_skipping = false;
	GameScene gs = m_currentGameScenario.getGameScene();
	int count = gs.getUserChoiceCount();
	if(count)
	{
		vector<int> scenarioOffsetVector;
		vector<string> choicesVector;
		for(int i = 0;i < count;i++)
		{
			int scenarioOffset;
			string choice;
			gs.getUserChoiceInfo(i,scenarioOffset,choice);
			scenarioOffsetVector.push_back(scenarioOffset);
			choicesVector.push_back(choice);
		}
		UserChoiceDialog ucd(choicesVector,m_conversationLabel);
		ucd.init();
		m_conversationLabel->releaseKeyboardToChild(true);
		int dialogChoice = ucd.exec();
		m_conversationLabel->releaseKeyboardToChild(false);
		if(dialogChoice >= 0)
			signalStartScenario(scenarioOffsetVector.at(dialogChoice));
		else
			signalRewindScenario();
	}
}

bool MainWindow::parseTrivia(QXmlStreamReader &xml,GameScenario &gameScenario)
{
	bool ret = true;
	GameScene gameScene(GameScene::SCENE_TRIVIA);
	bool done = false;
	while(!done)
	{
		switch(xml.readNext())
		{
		case QXmlStreamReader::StartElement:
			{
				bool validElement = false;
				if(!xml.name().compare("question"))
				{
					validElement = true;
					string question = xml.readElementText().toAscii().data();
					if(question.size())
						gameScene.setTriviaQuestion(question);
					else
					{
						ret = false;
						done = true;
						fprintf(stderr,"error invalid trivia question: %s\n",question.c_str());
					}

				}
				else if(!xml.name().compare("choice"))
				{
					validElement = true;
					string choice = xml.readElementText().toAscii().data();
					if(choice.size())
						gameScene.addTriviaChoice(choice);
					else
					{
						ret = false;
						done = true;
						fprintf(stderr,"error invalid trivia choice: %s\n",choice.c_str());
					}
				}
				else if(!xml.name().compare("correct_answer"))
				{
					validElement = true;
					string answer = xml.readElementText().toAscii().data();
					bool valid = true;
					if(!answer.size())
						valid = false;
					else
					{
						for(int i = 0;i < (int) answer.size();i++)
						{
							if(!isdigit(answer[i]))
							{
								valid = false;
								break;
							}
						}
					}
					if(valid)
						gameScene.setTriviaCorrectAnswer(atoi(answer.c_str()));
					else
					{
						ret = false;
						done = true;
						fprintf(stderr,"error invalid trivia correct answer: %s\n",answer.c_str());
					}
				}
				else if(!xml.name().compare("error_scenario"))
				{
					validElement = true;
					string scenarioString = xml.readElementText().toStdString();
					int scenarioOffset = getScenarioOffsetFromText(scenarioString);
					if(scenarioOffset >= 0)
						gameScene.setTriviaErrorScenario(scenarioOffset);
					else
					{
						ret = false;
						done = true;
						fprintf(stderr,"error invalid trivia error scenario: %s\n",scenarioString.c_str());
					}
				}
				else if(!xml.name().compare("sprite_name"))
				{
					validElement = true;
					string spriteName = xml.readElementText().toStdString();
					int spriteOffset = getSpriteOffsetFromText(spriteName);
					if(spriteOffset >= 0)
						gameScene.setTriviaSprite(spriteOffset);
					else
					{
						ret = false;
						done = true;
						fprintf(stderr,"error invalid trivia sprite: %s\n",spriteName.c_str());
					}
				}
				if(!validElement)
				{
					ret = false;
					done = true;
					fprintf(stderr,"error invalid element: %s\n",xml.name().toString().toStdString().c_str());
				}
			}
			break;
		case QXmlStreamReader::Characters:
			break;
		case QXmlStreamReader::EndElement:
			done = true;
			break;
		default:
			done = true;
			ret = false;
			fprintf(stderr,"error unexpected xml read type\n");
			break;
		}
	}
	if(ret)
	{
		if(gameScene.getTriviaChoiceCount() > 1)
		{
			if(gameScene.getTriviaCorrectAnswer() < gameScene.getTriviaChoiceCount())
				gameScenario.addGameScene(gameScene);
			else
			{
				ret = false;
				fprintf(stderr,"error invalid trivia correct answer: %d\n",gameScene.getTriviaCorrectAnswer());
			}
		}
		else
		{
			ret = false;
			fprintf(stderr,"error not enough trivia choices: %d\n",gameScene.getTriviaChoiceCount());
		}
	}
	return ret;
}

void MainWindow::performTrivia()
{
	if(m_skipping)
		signalAdvanceScenario();
	else
	{
		m_triviaReturnScenarioOffset = m_currentGameScenarioOffset;
		m_triviaReturnSceneOffset = m_currentGameSceneOffset;
		m_triviaPixMap = *m_pixmapDisplayLabel->pixmap();
		m_triviaBackgroundPixmap = m_currentBackgroundPixmap;

		TriviaDialog td(m_currentGameScenario.getGameScene().getTriviaChoices(),
						m_currentGameScenario.getGameScene().getTriviaQuestion(),
						m_currentGameScenario.getGameScene().getTriviaSpriteOffset(),
						this);
		td.init();
		if(td.exec() != m_currentGameScenario.getGameScene().getTriviaCorrectAnswer())
			startScenario(m_currentGameScenario.getGameScene().getTriviaErrorScenario());
		else
			signalAdvanceScenario();
	}
}

void MainWindow::rewindScenario()
{
	m_currentSceneType = m_currentGameScenario.previousScene(m_currentGameSceneOffset);
	performScene();
}

void MainWindow::writeConversation()
{
	++m_currentConversationOffset;
	if((m_currentConversationOffset > (int) m_currentConversationString.length()) || m_accelerateConversation)
	{
		string finalString = m_currentConversationString;
		finalString += " >>";
		m_conversationLabel->setText(finalString.c_str());
		m_conversationComplete = true;
		m_conversationWriteTimer->stop();
#ifdef CONVERSATION_KEY_CLICK
		m_conversationTypingMediaObject->stop();
#endif
	}
	else
		m_conversationLabel->setText(m_currentConversationString.substr(0,m_currentConversationOffset).c_str());
}

void MainWindow::writeSkipConversation()
{
	string finalString = m_currentConversationString;
	finalString += " >>";
	m_conversationLabel->setText(finalString.c_str());
	m_conversationComplete = true;
	m_conversationWriteSkipTimer->stop();
	signalAdvanceScenario();
}

bool MainWindow::getConversationComplete()
{
	return m_conversationComplete;
}

void MainWindow::setAccelerateConversation()
{
	m_accelerateConversation = true;
}

bool MainWindow::parseSound(QXmlStreamReader &xml,GameScenario &gameScenario)
{
	bool ret = true;
	GameScene gameScene(GameScene::SCENE_SOUND);
	int soundOffset;
	bool done = false;
	while(!done)
	{
		switch(xml.readNext())
		{
		case QXmlStreamReader::StartElement:
			{
				bool validElement = false;
				if(!xml.name().compare("sound_name"))
				{
					validElement = true;
					string soundString = xml.readElementText().toStdString();
					soundOffset = getSoundOffsetFromText(soundString);
					if(soundOffset >= 0)
						gameScene.setSoundOffset(soundOffset);
					else
					{
						ret = false;
						done = true;
						fprintf(stderr,"error invalid sound: %s\n",soundString.c_str());
					}
				}
				if(!validElement)
				{
					ret = false;
					done = true;
					fprintf(stderr,"error invalid sound element: %s\n",xml.name().toString().toStdString().c_str());
				}
			}
			break;
		case QXmlStreamReader::Characters:
			break;
		case QXmlStreamReader::EndElement:
			done = true;
			break;
		default:
			ret = false;
			done = true;
			fprintf(stderr,"error unexpected xml read type\n");
			break;
		}
	}
	if(ret)
		gameScenario.addGameScene(gameScene);
	return ret;
}

bool MainWindow::parseTriviaReturn(QXmlStreamReader &xml,GameScenario &gameScenario)
{
	bool ret = true;
	GameScene gameScene(GameScene::SCENE_TRIVIA_RETURN);
	bool done = false;
	while(!done)
	{
		switch(xml.readNext())
		{
		case QXmlStreamReader::Characters:
			break;
		case QXmlStreamReader::EndElement:
			done = true;
			break;
		default:
			ret = false;
			done = true;
			fprintf(stderr,"error unexpected xml read type\n");
			break;
		}
	}
	if(ret)
		gameScenario.addGameScene(gameScene);
	return ret;
}

void MainWindow::performTriviaReturn()
{
	m_skipping = false;
	m_pixmapDisplayLabel->setPixmap(m_triviaPixMap);
	m_currentBackgroundPixmap = m_triviaBackgroundPixmap;
	m_currentGameScenarioOffset = m_triviaReturnScenarioOffset;
	m_currentGameScenario = m_gameScenarioVector.at(m_currentGameScenarioOffset);
	if(m_triviaReturnSceneOffset)
	{
		m_currentGameScenario.setCurrentScene(m_triviaReturnSceneOffset - 1);
		signalAdvanceScenario();
	}
	else
		signalStartScenario(m_currentGameScenarioOffset);
}

void MainWindow::soundEffectMediaStateChanged(Phonon::State newstate,Phonon::State oldstate)
 {
	if(newstate == Phonon::PlayingState)
	{
		if(!previousPlayEstablished)
		{
			m_audioVideoMonitorThreadMutex.lock();
			m_soundPlayEstablished = true;
			m_audioVideoMonitorThreadMutex.unlock();
		}
	}
	if((oldstate == Phonon::PlayingState || newstate == Phonon::ErrorState) && m_currentSceneType == GameScene::SCENE_SOUND)
	{
		m_soundEffectMediaObject->stop();
		signalAdvanceScenario();
	}
	else if(newstate == Phonon::StoppedState && oldstate != Phonon::PausedState && m_currentSceneType == GameScene::SCENE_SOUND)  // this is a kluge for an odd stop state I get in xine
	{
		if(!m_soundEffectStopStateFound)
		{
			m_soundEffectMediaObject->play();
			m_soundEffectStopStateFound = true;
		}
		else
		{
			m_soundEffectMediaObject->stop();
			signalAdvanceScenario();
		}
	}
 }

void MainWindow::performSound()
{
	if(m_skipping)
		signalAdvanceScenario();
	else
	{
		m_soundEffectStopStateFound = false;
		GameScene gs = m_currentGameScenario.getGameScene();
		QString soundFile = m_soundVector.at(gs.getSoundOffset()).c_str();
		m_soundEffectMediaObject->setCurrentSource(soundFile);
		m_soundEffectMediaObject->play();
	}
}

void MainWindow::enableGamePlayButtons(bool enabled)
{
	if(enabled)
	{
		if(m_skipContainer.isSkipAvailable(m_currentGameScenarioOffset,m_currentGameSceneOffset))
		{
			if(m_skipping)
				m_skipButton->setEnabled(false);
			else
				m_skipButton->setEnabled(true);
		}
		else
		{
			m_skipping = false;
			m_skipButton->setEnabled(false);
		}
	}
	else
		m_skipButton->setEnabled(false);
	if(enabled)
	{
		if(m_skipping)
		{
			m_saveButton->setEnabled(false);
			m_loadButton->setEnabled(false);
			m_titleButton->setEnabled(false);
			m_previousButton->setEnabled(false);
			m_nextButton->setEnabled(false);
		}
		else
		{
			m_saveButton->setEnabled(true);
			if(m_saveLoadContainer.isLoadAvailable())
				m_loadButton->setEnabled(true);
			else
				m_loadButton->setEnabled(false);
			m_titleButton->setEnabled(true);
			int dummy;
			if(isPreviousAvailable(dummy))
				m_previousButton->setEnabled(true);
			else
				m_previousButton->setEnabled(false);
			m_nextButton->setEnabled(true);
		}
	}
	else
	{
		m_saveButton->setEnabled(false);
		m_loadButton->setEnabled(false);
		m_titleButton->setEnabled(false);
		m_previousButton->setEnabled(false);
		m_nextButton->setEnabled(false);
		if(m_currentSceneType == GameScene::SCENE_CONVERSATION ||
				m_currentSceneType == GameScene::SCENE_SOUND ||
				m_currentSceneType == GameScene::SCENE_MOVIE)
		{
			int dummy;
			if(isPreviousAvailable(dummy))
				m_previousButton->setEnabled(true);
			m_nextButton->setEnabled(true);
		}
	}
}

void MainWindow::enableTitleButton()
{
	m_saveButton->setEnabled(false);
	m_loadButton->setEnabled(false);
	m_skipButton->setEnabled(false);
	m_previousButton->setEnabled(false);
	m_nextButton->setEnabled(false);
	m_titleButton->setEnabled(true);
}

bool MainWindow::parseStartScenario(QXmlStreamReader &xml,GameScenario &gameScenario)
{
	bool ret = true;
	GameScene gameScene(GameScene::SCENE_START_SCENARIO);
	int scenarioOffset;
	bool done = false;
	while(!done)
	{
		switch(xml.readNext())
		{
		case QXmlStreamReader::StartElement:
			{
				bool validElement = false;
				if(!xml.name().compare("scenario_name"))
				{
					validElement = true;
					string scenarioString = xml.readElementText().toStdString();
					scenarioOffset = getScenarioOffsetFromText(scenarioString);
					if(scenarioOffset >= 0)
						gameScene.setScenarioOffset(scenarioOffset);
					else
					{
						ret = false;
						done = true;
						fprintf(stderr,"error invalid start scenario: %s\n",scenarioString.c_str());
					}
				}
				if(!validElement)
				{
					ret = false;
					done = true;
					fprintf(stderr,"error invalid start scenario element: %s\n",xml.name().toString().toStdString().c_str());
				}
			}
			break;
		case QXmlStreamReader::Characters:
			break;
		case QXmlStreamReader::EndElement:
			done = true;
			break;
		default:
			ret = false;
			done = true;
			fprintf(stderr,"error unexpected xml read type\n");
			break;
		}
	}
	if(ret)
		gameScenario.addGameScene(gameScene);
	return ret;
}

void MainWindow::performStartScenario()
{
	signalStartScenario(m_currentGameScenario.getGameScene().getScenarioOffset());
}

bool MainWindow::parseGameOver(QXmlStreamReader &xml,GameScenario &gameScenario)
{
	bool ret = true;
	GameScene gameScene(GameScene::SCENE_GAME_OVER);
	bool done = false;
	while(!done)
	{
		switch(xml.readNext())
		{
		case QXmlStreamReader::StartElement:
			{
				bool validElement = false;
				if(!xml.name().compare("to_be_continued"))
				{
					validElement = true;
					string toBeContinuedString = xml.readElementText().toStdString();
					if(toBeContinuedString == "true")
						gameScene.setToBeContinued(true);
				}
				if(!validElement)
				{
					ret = false;
					done = true;
					fprintf(stderr,"error invalid sound element: %s\n",xml.name().toString().toStdString().c_str());
				}
			}
			break;
		case QXmlStreamReader::Characters:
			break;
		case QXmlStreamReader::EndElement:
			done = true;
			break;
		default:
			ret = false;
			done = true;
			fprintf(stderr,"error unexpected xml read type\n");
			break;
		}
	}
	if(ret)
		gameScenario.addGameScene(gameScene);
	return ret;
}

void MainWindow::performGameOver()
{
	GameScene gs = m_currentGameScenario.getGameScene();
	if(gs.getToBeContinued())
	{
		QPixmap toBeContinuedPixmap(m_backgroundVector.at(BACKGROUND_TO_BE_CONTINUED).c_str());
		toBeContinuedPixmap.scaled(IMAGE_MOVIE_MINIMUM_WIDTH,IMAGE_MOVIE_MINIMUM_HEIGHT);
		m_pixmapDisplayLabel->setPixmap(toBeContinuedPixmap);
		m_conversationLabel->setText("To Be Continued.");
	}
	else
	{
		QPixmap gameOverPixmap(m_backgroundVector.at(BACKGROUND_GAME_OVER).c_str());
		gameOverPixmap.scaled(IMAGE_MOVIE_MINIMUM_WIDTH,IMAGE_MOVIE_MINIMUM_HEIGHT);
		m_pixmapDisplayLabel->setPixmap(gameOverPixmap);
		m_conversationLabel->setText("Game Over.");
	}
	enableTitleButton();
}

#ifdef CONVERSATION_KEY_CLICK
void MainWindow::conversationTypingMediaStateChanged(Phonon::State newstate,Phonon::State)
 {
	if(newstate == Phonon::PlayingState)
	{
		if(!previousPlayEstablished)
		{
			m_audioVideoMonitorThreadMutex.lock();
			m_conversationPlayEstablished = true;
			m_audioVideoMonitorThreadMutex.unlock();
		}
	}
 }
#endif

void MainWindow::skipButtonClicked()
{
	if(m_skipContainer.isSkipAvailable(m_currentGameScenarioOffset,m_currentGameSceneOffset))
	{
		m_skipping = true;
		if(!m_conversationComplete)
			m_accelerateConversation = true;
		signalAdvanceScenario();
	}
}

void MainWindow::unsetSkipping()
{
	m_skipping = false;
}

bool MainWindow::isSkipping()
{
	return m_skipping;
}

void MainWindow::gameLoadButtonClicked()
{
	m_previousPage = PAGE_TITLE;
	showLoadGame();
}

void MainWindow::showLoadGame()
{
	m_skipping = false;
	m_currentSaveLoadMode = MODE_LOAD;
	m_currentSaveLoadPage = 0;
	enablePreviousNext();
	m_saveLoadTitleLabel->setText("Load Game");
	setSaveLoadIcons();
	m_mainStack->setCurrentWidget(m_saveLoadCentralWidget);
}

void MainWindow::saveLoadCancelButtonClicked()
{
	if(m_previousPage == PAGE_TITLE)
		m_mainStack->setCurrentWidget(m_titleCentralWidget);
	else
		m_mainStack->setCurrentWidget(m_gameCentralWidget);
}

void MainWindow::enablePreviousNext()
{
	if(m_currentSaveLoadPage > 0)
		m_saveLoadPreviousButton->setEnabled(true);
	else
		m_saveLoadPreviousButton->setEnabled(false);
	if(m_currentSaveLoadPage < SAVE_LOAD_MAX_PAGE)
		m_saveLoadNextButton->setEnabled(true);
	else
		m_saveLoadNextButton->setEnabled(false);
}

void MainWindow::setSaveLoadIcons()
{
	int base = m_currentSaveLoadPage * 6;

	m_saveLoad1Button->setIcon(m_saveLoadIconVector.at(base));
	m_saveLoad2Button->setIcon(m_saveLoadIconVector.at(base + 1));
	m_saveLoad3Button->setIcon(m_saveLoadIconVector.at(base + 2));
	m_saveLoad4Button->setIcon(m_saveLoadIconVector.at(base + 3));
	m_saveLoad5Button->setIcon(m_saveLoadIconVector.at(base + 4));
	m_saveLoad6Button->setIcon(m_saveLoadIconVector.at(base + 5));

	time_t saveNodeTime = m_saveLoadContainer.getSaveNodeTime(base);
	if(saveNodeTime)
		m_saveLoad1Label->setText(ctime(&saveNodeTime));
	else
		m_saveLoad1Label->setText("Open");
	saveNodeTime = m_saveLoadContainer.getSaveNodeTime(base + 1);
	if(saveNodeTime)
		m_saveLoad2Label->setText(ctime(&saveNodeTime));
	else
		m_saveLoad2Label->setText("Open");
	saveNodeTime = m_saveLoadContainer.getSaveNodeTime(base + 2);
	if(saveNodeTime)
		m_saveLoad3Label->setText(ctime(&saveNodeTime));
	else
		m_saveLoad3Label->setText("Open");
	saveNodeTime = m_saveLoadContainer.getSaveNodeTime(base + 3);
	if(saveNodeTime)
		m_saveLoad4Label->setText(ctime(&saveNodeTime));
	else
		m_saveLoad4Label->setText("Open");
	saveNodeTime = m_saveLoadContainer.getSaveNodeTime(base + 4);
	if(saveNodeTime)
		m_saveLoad5Label->setText(ctime(&saveNodeTime));
	else
		m_saveLoad5Label->setText("Open");
	saveNodeTime = m_saveLoadContainer.getSaveNodeTime(base + 5);
	if(saveNodeTime)
		m_saveLoad6Label->setText(ctime(&saveNodeTime));
	else
		m_saveLoad6Label->setText("Open");
}

void MainWindow::loadButtonClicked()
{
	m_accelerateConversation = true;
	m_previousPage = PAGE_GAME;
	showLoadGame();
}

void MainWindow::saveButtonClicked()
{
	m_currentSaveLoadMode = MODE_SAVE;
	m_accelerateConversation = true;
	m_previousPage = PAGE_GAME;
	m_currentSaveLoadPage = 0;
	enablePreviousNext();
	m_saveLoadTitleLabel->setText("Save Game");
	setSaveLoadIcons();
	m_mainStack->setCurrentWidget(m_saveLoadCentralWidget);
}

void MainWindow::saveLoadPreviousButtonClicked()
{
	--m_currentSaveLoadPage;
	if(m_currentSaveLoadPage < 0)
		m_currentSaveLoadPage = 0;
	enablePreviousNext();
	setSaveLoadIcons();
}

void MainWindow::saveLoadNextButtonClicked()
{
	++m_currentSaveLoadPage;
	if(m_currentSaveLoadPage > SAVE_LOAD_MAX_PAGE)
		m_currentSaveLoadPage = SAVE_LOAD_MAX_PAGE;
	enablePreviousNext();
	setSaveLoadIcons();
}

bool MainWindow::crossCheckBackgroundSprite()
{
	bool ret = true;
	for(int i = 0;i < BACKGROUND_MAX;i++)
	{
		string text;
		if(getBackgroundTextFromOffset(i,text))
		{
			if(getBackgroundOffsetFromText(text) < 0)
			{
				ret = false;
				fprintf(stderr,"error in background cross check element: %d %s\n",i,text.c_str());
				break;
			}
		}
		else
		{
			ret = false;
			fprintf(stderr,"error in background cross check element: %d\n",i);
			break;
		}
	}
	if(ret)
	{
		for(int i = 0;i < SPRITE_MAX;i++)
		{
			string text;
			if(getSpriteTextFromOffset(i,text))
			{
				if(getSpriteOffsetFromText(text) < 0)
				{
					ret = false;
					fprintf(stderr,"error in sprite cross check element: %d %s\n",i,text.c_str());
					break;
				}
			}
			else
			{
				ret = false;
				fprintf(stderr,"error in sprite cross check element: %d\n",i);
				break;
			}
		}
	}
	return ret;
}

void MainWindow::saveLoad1ButtonClicked()
{
	saveLoadButtonAction(0);
}

void MainWindow::saveLoad2ButtonClicked()
{
	saveLoadButtonAction(1);
}

void MainWindow::saveLoad3ButtonClicked()
{
	saveLoadButtonAction(2);
}

void MainWindow::saveLoad4ButtonClicked()
{
	saveLoadButtonAction(3);
}

void MainWindow::saveLoad5ButtonClicked()
{
	saveLoadButtonAction(4);
}

void MainWindow::saveLoad6ButtonClicked()
{
	saveLoadButtonAction(5);
}

void MainWindow::saveLoadButtonAction(int offset)
{
	if(m_currentSaveLoadMode == MODE_SAVE)
	{
		GameScene gameScene;
		int backgroundOffset = -1;
		if(m_currentGameScenario.getSaveNodeInfo(m_currentGameSceneOffset,backgroundOffset,gameScene))
		{
			string backgroundString;
			if(getBackgroundTextFromOffset(backgroundOffset,backgroundString))
			{
				int spriteOffset = -1;
				int verticalPosition = -1;
				int horizontalStartingPosition = -1;
				int horizontalEndingPosition = -1;
				int direction = -1;
				vector<SaveSpriteNode> saveSpriteNodeVector;
				int spriteCount = gameScene.getSpriteCount();
				bool spriteError = false;
				for(int i = 0;i < spriteCount;i++)
				{
					gameScene.getSpriteInfo(i,spriteOffset,verticalPosition,horizontalStartingPosition,horizontalEndingPosition,direction);
					if(horizontalEndingPosition != SPRITE_HORIZONTAL_OFF_SCREEN)  // if it's off screen, skip it
					{
						string spriteName;
						if(getSpriteTextFromOffset(spriteOffset,spriteName))
						{
							SaveSpriteNode saveSpriteNode(verticalPosition,horizontalEndingPosition,spriteName);
							saveSpriteNodeVector.push_back(saveSpriteNode);
						}
						else
						{
							fprintf(stderr,"error in getSpriteTextFromOffset");
							spriteError = true;
							break;
						}
					}
				}
				if(!spriteError)
				{
					time_t currentTime = time(NULL);
					SaveNode saveNode(currentTime,m_currentGameScenarioOffset,m_currentGameSceneOffset,backgroundString,saveSpriteNodeVector);
					int saveLoadOffset = (m_currentSaveLoadPage * 6) + offset;
					QPixmap pixmap(m_backgroundVector.at(backgroundOffset).c_str());
					pixmap.scaled(IMAGE_MOVIE_MINIMUM_WIDTH,IMAGE_MOVIE_MINIMUM_HEIGHT);
					QPainter painter(&pixmap);
					for(int i = 0;i < gameScene.getSpriteCount();i++)
					{
						gameScene.getSpriteInfo(i,spriteOffset,verticalPosition,horizontalStartingPosition,horizontalEndingPosition,direction);
						if(horizontalEndingPosition != SPRITE_HORIZONTAL_OFF_SCREEN)
						{
							int spriteYOffset = 0;
							switch(verticalPosition)
							{
							case MainWindow::SPRITE_VERTICAL_BOTTOM:
								spriteYOffset = DEFAULT_BOTTOM_SPRITE;
								break;
							case MainWindow::SPRITE_VERTICAL_MIDDLE:
								spriteYOffset = DEFAULT_MIDDLE_SPRITE;
								break;
							case MainWindow::SPRITE_VERTICAL_TOP:
								spriteYOffset = DEFAULT_TOP_SPRITE;
								break;
							default:
								break;
							}
							QPixmap sprite(m_spriteVector.at(spriteOffset).c_str());
							sprite.scaledToHeight(DEFAULT_SPRITE_HEIGHT);
							int spriteWidth = (sprite.width() * DEFAULT_SPRITE_HEIGHT) / sprite.height();
							int spriteXOffset = 0;
							switch(horizontalEndingPosition)
							{
							case MainWindow::SPRITE_HORIZONTAL_LEFT:
								spriteXOffset = ACTIVE_SPRITE_X_DISPLAY_BOOST;
								break;
							case MainWindow::SPRITE_HORIZONTAL_MIDDLE:
								spriteXOffset = (pixmap.width() / 2) - (spriteWidth / 2);
								break;
							case MainWindow::SPRITE_HORIZONTAL_RIGHT:
								spriteXOffset = pixmap.width() - spriteWidth - ACTIVE_SPRITE_X_DISPLAY_BOOST;
								break;
							default:
								break;
							}
							painter.drawPixmap(spriteXOffset,spriteYOffset,spriteWidth,DEFAULT_SPRITE_HEIGHT,sprite);
						}
					}
					QPixmap scalePixmap = pixmap.scaled(SAVE_LOAD_PIXMAP_WIDTH,SAVE_LOAD_PIXMAP_HEIGHT);
					bool overwrite = m_saveLoadContainer.getSaveNodeInUse(saveLoadOffset);
					SaveLoadVerificationDialog slvd(MODE_SAVE,overwrite,currentTime,scalePixmap,this);
					slvd.init();
					if(slvd.exec() == QDialog::Accepted)
					{
						if(m_saveLoadContainer.setSaveNode(saveLoadOffset,saveNode))
						{
							m_saveLoadIconVector.at(saveLoadOffset) = scalePixmap;
							m_loadButton->setEnabled(true);  // in case this is the first
							m_mainStack->setCurrentWidget(m_gameCentralWidget);
						}
					}
				}
			}
			else
				fprintf(stderr,"error in getBackgroundTextFromOffset\n");
		}
		else
			fprintf(stderr,"error in GameScenario::getSaveNodeInfo\n");
	}
	else
	{
		SaveNode saveNode;
		if(m_saveLoadContainer.getSaveNode((m_currentSaveLoadPage * 6) + offset,saveNode))
		{
			if(saveNode.getInUse())
			{
				int scenarioOffset = saveNode.getScenarioOffset();
				if(scenarioOffset >= 0 && scenarioOffset < SCENARIO_MAX)
				{
					int sceneOffset = saveNode.getSceneOffset();
					int lastScene = m_gameScenarioVector.at(scenarioOffset).getLastSceneOffset();
					if(sceneOffset >= 0 && sceneOffset <= lastScene)
					{
						string backgroundName = saveNode.getBackgroundName();
						int backgroundOffset = getBackgroundOffsetFromText(backgroundName);
						if(backgroundOffset >= 0)
						{
							vector<SaveSpriteNode> saveSpriteNodeVector = saveNode.getSaveSpriteNodeVector();
							QPixmap pixmap(m_backgroundVector.at(backgroundOffset).c_str());
							pixmap.scaled(IMAGE_MOVIE_MINIMUM_WIDTH,IMAGE_MOVIE_MINIMUM_HEIGHT);
							QPainter painter(&pixmap);
							bool spriteError = false;
							for(int i = 0;i < (int) saveSpriteNodeVector.size();i++)
							{
								SaveSpriteNode saveSpriteNode = saveSpriteNodeVector.at(i);
								string spriteName = saveSpriteNode.getSpriteName();
								int spriteOffset = getSpriteOffsetFromText(spriteName);
								if(spriteOffset >= 0)
								{
									int verticalPosition = saveSpriteNode.getVerticalPosition();
									if(verticalPosition >= SPRITE_VERTICAL_TOP && verticalPosition <= SPRITE_VERTICAL_BOTTOM)
									{
										int horizontalPosition = saveSpriteNode.getHorizontalPosition();
										if(horizontalPosition >= SPRITE_HORIZONTAL_LEFT && horizontalPosition <= SPRITE_HORIZONTAL_RIGHT)
										{
											int spriteYOffset = 0;
											switch(verticalPosition)
											{
											case MainWindow::SPRITE_VERTICAL_BOTTOM:
												spriteYOffset = DEFAULT_BOTTOM_SPRITE;
												break;
											case MainWindow::SPRITE_VERTICAL_MIDDLE:
												spriteYOffset = DEFAULT_MIDDLE_SPRITE;
												break;
											case MainWindow::SPRITE_VERTICAL_TOP:
												spriteYOffset = DEFAULT_TOP_SPRITE;
												break;
											default:
												break;
											}
											QPixmap sprite(m_spriteVector.at(spriteOffset).c_str());
											sprite.scaledToHeight(DEFAULT_SPRITE_HEIGHT);
											int spriteWidth = (sprite.width() * DEFAULT_SPRITE_HEIGHT) / sprite.height();
											int spriteXOffset = 0;
											switch(horizontalPosition)
											{
											case MainWindow::SPRITE_HORIZONTAL_LEFT:
												spriteXOffset = ACTIVE_SPRITE_X_DISPLAY_BOOST;
												break;
											case MainWindow::SPRITE_HORIZONTAL_MIDDLE:
												spriteXOffset = (pixmap.width() / 2) - (spriteWidth / 2);
												break;
											case MainWindow::SPRITE_HORIZONTAL_RIGHT:
												spriteXOffset = pixmap.width() - spriteWidth - ACTIVE_SPRITE_X_DISPLAY_BOOST;
												break;
											default:
												break;
											}
											painter.drawPixmap(spriteXOffset,spriteYOffset,spriteWidth,DEFAULT_SPRITE_HEIGHT,sprite);
										}
										else
										{
											fprintf(stderr,"error in GameScenario::getSaveNode, Invalid horizontal position: %d in SaveNode: %d not in use\n",horizontalPosition,(m_currentSaveLoadPage * 6) + offset);
											spriteError = true;
											break;
										}
									}
									else
									{
										fprintf(stderr,"error in GameScenario::getSaveNode, Invalid vertical position: %d in SaveNode: %d not in use\n",verticalPosition,(m_currentSaveLoadPage * 6) + offset);
										spriteError = true;
										break;
									}
								}
								else
								{
									fprintf(stderr,"error in GameScenario::getSaveNode, Invalid sprite name: %s in SaveNode: %d not in use\n",spriteName.c_str(),(m_currentSaveLoadPage * 6) + offset);
									spriteError = true;
									break;
								}
							}
							if(!spriteError)
							{
								SaveLoadVerificationDialog slvd(MODE_LOAD,
																false,
																saveNode.getTime(),
																m_saveLoadIconVector.at((m_currentSaveLoadPage * 6) + offset).pixmap(SAVE_LOAD_PIXMAP_WIDTH,SAVE_LOAD_PIXMAP_HEIGHT),
																this);
								slvd.init();
								if(slvd.exec() == QDialog::Accepted)
								{
									m_currentGameScenarioOffset = scenarioOffset;
									m_currentGameScenario = m_gameScenarioVector.at(m_currentGameScenarioOffset);
									m_currentBackgroundPixmap.load(m_backgroundVector.at(backgroundOffset).c_str());
									m_currentBackgroundPixmap.scaled(IMAGE_MOVIE_MINIMUM_WIDTH,IMAGE_MOVIE_MINIMUM_HEIGHT);
									if(sceneOffset)
									{
										m_currentGameScenario.setCurrentScene(sceneOffset - 1);
										signalAdvanceScenario();
									}
									else  // this should never happen because we would have no pixmap but ...
										signalStartScenario(m_currentGameScenarioOffset);
									m_pixmapDisplayLabel->setPixmap(pixmap);
									m_mainStack->setCurrentWidget(m_gameCentralWidget);
								}
							}
						}
						else
							fprintf(stderr,"error in GameScenario::getSaveNode, Invalid background name: %s in SaveNode: %d not in use\n",backgroundName.c_str(),(m_currentSaveLoadPage * 6) + offset);
					}
					else
						fprintf(stderr,"error in GameScenario::getSaveNode, Scene: %d out of range for Scenario: %d in SaveNode: %d not in use\n",sceneOffset,scenarioOffset,(m_currentSaveLoadPage * 6) + offset);
				}
				else
					fprintf(stderr,"error in GameScenario::getSaveNode, Scenario: %d out of range in SaveNode: %d not in use\n",scenarioOffset,(m_currentSaveLoadPage * 6) + offset);
			}
			else
				fprintf(stderr,"error in GameScenario::getSaveNode, SaveNode: %d not in use\n",(m_currentSaveLoadPage * 6) + offset);
		}
		else
			fprintf(stderr,"error in GameScenario::getSaveNode\n");
	}
}

void MainWindow::loadSaveIcons()
{
	QPixmap initialSaveLoadPixmap(m_backgroundVector.at(BACKGROUND_BLACK).c_str());
	initialSaveLoadPixmap.scaled(SAVE_LOAD_PIXMAP_WIDTH,SAVE_LOAD_PIXMAP_HEIGHT);
	QIcon initialSaveLoadIcon(initialSaveLoadPixmap);
	for(int i = 0;i < TOTAL_GAME_SAVES;i++)
		m_saveLoadIconVector.push_back(initialSaveLoadIcon);
	for(int i = 0;i < TOTAL_GAME_SAVES;i++)
	{
		SaveNode saveNode;
		m_saveLoadContainer.getSaveNode(i,saveNode);
		if(saveNode.getInUse())
		{
			if(!createSaveNodeIcon(i,saveNode))  // bad save node, empty it
			{
				SaveNode emptySaveNode;
				m_saveLoadContainer.setSaveNode(i,emptySaveNode);
			}
		}
	}
}

bool MainWindow::createSaveNodeIcon(int offset,const SaveNode &saveNode)
{
	bool ret = true;
	int scenarioOffset = saveNode.getScenarioOffset();
	if(scenarioOffset >= 0 && scenarioOffset < SCENARIO_MAX)
	{
		int sceneOffset = saveNode.getSceneOffset();
		int lastScene = m_gameScenarioVector.at(scenarioOffset).getLastSceneOffset();
		if(sceneOffset >= 0 && sceneOffset <= lastScene)
		{
			string backgroundName = saveNode.getBackgroundName();
			int backgroundOffset = getBackgroundOffsetFromText(backgroundName);
			if(backgroundOffset >= 0)
			{
				vector<SaveSpriteNode> saveSpriteNodeVector = saveNode.getSaveSpriteNodeVector();
				QPixmap pixmap(m_backgroundVector.at(backgroundOffset).c_str());
				pixmap.scaled(IMAGE_MOVIE_MINIMUM_WIDTH,IMAGE_MOVIE_MINIMUM_HEIGHT);
				QPainter painter(&pixmap);
				bool spriteError = false;
				for(int i = 0;i < (int) saveSpriteNodeVector.size();i++)
				{
					SaveSpriteNode saveSpriteNode = saveSpriteNodeVector.at(i);
					string spriteName = saveSpriteNode.getSpriteName();
					int spriteOffset = getSpriteOffsetFromText(spriteName);
					if(spriteOffset >= 0)
					{
						int verticalPosition = saveSpriteNode.getVerticalPosition();
						if(verticalPosition >= SPRITE_VERTICAL_TOP && verticalPosition <= SPRITE_VERTICAL_BOTTOM)
						{
							int horizontalPosition = saveSpriteNode.getHorizontalPosition();
							if(horizontalPosition >= SPRITE_HORIZONTAL_LEFT && horizontalPosition <= SPRITE_HORIZONTAL_RIGHT)
							{
								int spriteYOffset = 0;
								switch(verticalPosition)
								{
								case MainWindow::SPRITE_VERTICAL_BOTTOM:
									spriteYOffset = DEFAULT_BOTTOM_SPRITE;
									break;
								case MainWindow::SPRITE_VERTICAL_MIDDLE:
									spriteYOffset = DEFAULT_MIDDLE_SPRITE;
									break;
								case MainWindow::SPRITE_VERTICAL_TOP:
									spriteYOffset = DEFAULT_TOP_SPRITE;
									break;
								default:
									break;
								}
								QPixmap sprite(m_spriteVector.at(spriteOffset).c_str());
								sprite.scaledToHeight(DEFAULT_SPRITE_HEIGHT);
								int spriteWidth = (sprite.width() * DEFAULT_SPRITE_HEIGHT) / sprite.height();
								int spriteXOffset = 0;
								switch(horizontalPosition)
								{
								case MainWindow::SPRITE_HORIZONTAL_LEFT:
									spriteXOffset = ACTIVE_SPRITE_X_DISPLAY_BOOST;
									break;
								case MainWindow::SPRITE_HORIZONTAL_MIDDLE:
									spriteXOffset = (pixmap.width() / 2) - (spriteWidth / 2);
									break;
								case MainWindow::SPRITE_HORIZONTAL_RIGHT:
									spriteXOffset = pixmap.width() - spriteWidth - ACTIVE_SPRITE_X_DISPLAY_BOOST;
									break;
								default:
									break;
								}
								painter.drawPixmap(spriteXOffset,spriteYOffset,spriteWidth,DEFAULT_SPRITE_HEIGHT,sprite);
							}
							else
							{
								fprintf(stderr,"error in GameScenario::getSaveNode, Invalid horizontal position: %d in SaveNode: %d not in use\n",horizontalPosition,offset);
								spriteError = true;
								break;
							}
						}
						else
						{
							fprintf(stderr,"error in GameScenario::getSaveNode, Invalid vertical position: %d in SaveNode: %d not in use\n",verticalPosition,offset);
							spriteError = true;
							break;
						}
					}
					else
					{
						fprintf(stderr,"error in GameScenario::getSaveNode, Invalid sprite name: %s in SaveNode: %d not in use\n",spriteName.c_str(),offset);
						spriteError = true;
						break;
					}
				}
				if(!spriteError)
				{
					QPixmap scalePixmap = pixmap.scaled(SAVE_LOAD_PIXMAP_WIDTH,SAVE_LOAD_PIXMAP_HEIGHT);
					QIcon icon(scalePixmap);
					m_saveLoadIconVector.at(offset) = icon;
				}
				else
					ret = false;
			}
			else
			{
				fprintf(stderr,"error in GameScenario::getSaveNode, Invalid background name: %s in SaveNode: %d not in use\n",backgroundName.c_str(),offset);
				ret = false;
			}
		}
		else
		{
			fprintf(stderr,"error in GameScenario::getSaveNode, Scene: %d out of range for Scenario: %d in SaveNode: %d not in use\n",sceneOffset,scenarioOffset,offset);
			ret = false;
		}
	}
	else
	{
		fprintf(stderr,"error in GameScenario::getSaveNode, Scenario: %d out of range in SaveNode: %d not in use\n",scenarioOffset,offset);
		ret = false;
	}
	return ret;
}

void MainWindow::aboutButtonClicked()
{
	m_mainStack->setCurrentWidget(m_aboutCentralWidget);
}

void MainWindow::aboutTitleButtonClicked()
{
	m_mainStack->setCurrentWidget(m_titleCentralWidget);
}

void MainWindow::instructionsButtonClicked()
{
	m_instructionsTextBrowser->setHtml(m_instructionsText.c_str());
	m_mainStack->setCurrentWidget(m_instructionsCentralWidget);
}

void MainWindow::instructionsTitleButtonClicked()
{
	m_mainStack->setCurrentWidget(m_titleCentralWidget);
}

void MainWindow::problemsWithAudioVideoButtonClicked()
{
	m_instructionsTextBrowser->setHtml(m_problemsWithAudioVideoText.c_str());
	m_mainStack->setCurrentWidget(m_instructionsCentralWidget);
}

void MainWindow::aboutQtButtonClicked()
{
	QMessageBox::aboutQt(this);
}

#ifdef CONVERSATION_KEY_CLICK
bool MainWindow::getConversationPlayEstablished()
{
	bool ret = false;
	m_audioVideoMonitorThreadMutex.lock();
	ret = m_conversationPlayEstablished;
	m_audioVideoMonitorThreadMutex.unlock();
	return ret;
}
#endif

bool MainWindow::getSoundPlayEstablished()
{
	bool ret = false;
	m_audioVideoMonitorThreadMutex.lock();
	ret =  m_soundPlayEstablished;
	m_audioVideoMonitorThreadMutex.unlock();
	return ret;
}

bool MainWindow::getMoviePlayEstablished()
{
	bool ret = false;
	m_audioVideoMonitorThreadMutex.lock();
	ret =  m_moviePlayEstablished;
	m_audioVideoMonitorThreadMutex.unlock();
	return ret;
}

void MainWindow::audioVideoWarn()
{
	m_audioVideoMonitorThreadMutex.lock();
	m_audioVideoWarnDisplayed = true;
	m_audioVideoMonitorThreadMutex.unlock();
	string message = "I'm having problems with audio / video.\n";
	message += "This is probably because I'm missing a required component.\n";
	message += "I'll display the 'Problems With Audio / Video' page when you close this dialog.\n";
	message += "Please install the required components for your operating system and restart the game.";
	QMessageBox mb(QMessageBox::Warning,"Problems With Audio / Video",message.c_str(),QMessageBox::Close,this);
	mb.exec();
	m_accelerateConversation = true;
	m_currentConversationString = "";
	m_currentConversationOffset = 0;
	QPixmap blackPixmap(m_backgroundVector.at(BACKGROUND_BLACK).c_str());
	blackPixmap.scaled(IMAGE_MOVIE_MINIMUM_WIDTH,IMAGE_MOVIE_MINIMUM_HEIGHT);
	m_pixmapDisplayLabel->setPixmap(blackPixmap);
	m_conversationLabel->setText("");
	problemsWithAudioVideoButtonClicked();
}

bool MainWindow::isAudioVideoWarnDisplayed()
{
	bool ret = false;
	m_audioVideoMonitorThreadMutex.lock();
	ret = m_audioVideoWarnDisplayed;
	m_audioVideoMonitorThreadMutex.unlock();
	return ret;
}

bool MainWindow::isThreadQuit()
{
	bool ret = false;
	m_audioVideoMonitorThreadMutex.lock();
	ret = m_threadQuit;
	m_audioVideoMonitorThreadMutex.unlock();
	return ret;
}

void MainWindow::accelerateSound()
{
	if(getCurrentSceneType() == GameScene::SCENE_SOUND)
	{
		m_soundEffectMediaObject->stop();
		signalAdvanceScenario();
	}
}

void MainWindow::accelerateMovie()
{
	if(getCurrentSceneType() == GameScene::SCENE_MOVIE)
	{
		if(m_movieMediaObject->state() == Phonon::StoppedState)
		{
			m_currentSceneType = GameScene::SCENE_ERROR;
			QString empty;
			m_movieMediaObject->setCurrentSource(empty);
			signalAdvanceScenario();
		}
		else
		{
			// sorry to say, I could not find a common reaction between windows and linux after adding movie truncating
			// so I have to split on the build define
#ifdef LINUX_BUILD
			m_movieMediaObject->stop();
#endif
#ifdef WINDOWS_BUILD
			m_movieMediaObject->pause();
#endif
			m_conversationLabel->setText("");
		}
	}
}

void MainWindow::previousButtonClicked()
{
	previousScene();
}

void MainWindow::nextButtonClicked()
{
	switch(m_currentSceneType)
	{
	case GameScene::SCENE_CONVERSATION:
		if(m_conversationComplete == true)
			signalAdvanceScenario();
		else
			m_accelerateConversation = true;
		break;
	case GameScene::SCENE_SOUND:
		accelerateSound();
		break;
	case GameScene::SCENE_MOVIE:
		accelerateMovie();
		break;
	default:
		break;
	}
}

void MainWindow::previousScene()
{
	if(m_currentSceneType == GameScene::SCENE_CONVERSATION ||
			m_currentSceneType == GameScene::SCENE_SOUND ||
			m_currentSceneType == GameScene::SCENE_MOVIE)
	{
		int previousScene;
		if(isPreviousAvailable(previousScene))
		{
			int backgroundOffset;
			GameScene spriteGameScene;
			if(m_currentGameScenario.getSaveNodeInfo(previousScene,backgroundOffset,spriteGameScene))
			{
				if(backgroundOffset >= 0)
				{
					QPixmap pixmap(m_backgroundVector.at(backgroundOffset).c_str());
					pixmap.scaled(IMAGE_MOVIE_MINIMUM_WIDTH,IMAGE_MOVIE_MINIMUM_HEIGHT);
					QPainter painter(&pixmap);
					for(int i = 0;i < spriteGameScene.getSpriteCount();i++)
					{
						int spriteOffset;
						int verticalPosition;
						int horizontalStartingPosition;
						int horizontalEndingPosition;
						int direction;
						spriteGameScene.getSpriteInfo(i,spriteOffset,verticalPosition,horizontalStartingPosition,horizontalEndingPosition,direction);

						if(verticalPosition >= SPRITE_VERTICAL_TOP && verticalPosition <= SPRITE_VERTICAL_BOTTOM)
						{
							if(horizontalEndingPosition >= SPRITE_HORIZONTAL_LEFT && horizontalEndingPosition <= SPRITE_HORIZONTAL_RIGHT)
							{
								int spriteYOffset = 0;
								switch(verticalPosition)
								{
								case MainWindow::SPRITE_VERTICAL_BOTTOM:
									spriteYOffset = DEFAULT_BOTTOM_SPRITE;
									break;
								case MainWindow::SPRITE_VERTICAL_MIDDLE:
									spriteYOffset = DEFAULT_MIDDLE_SPRITE;
									break;
								case MainWindow::SPRITE_VERTICAL_TOP:
									spriteYOffset = DEFAULT_TOP_SPRITE;
									break;
								default:
									break;
								}
								QPixmap sprite(m_spriteVector.at(spriteOffset).c_str());
								sprite.scaledToHeight(DEFAULT_SPRITE_HEIGHT);
								int spriteWidth = (sprite.width() * DEFAULT_SPRITE_HEIGHT) / sprite.height();
								int spriteXOffset = 0;
								switch(horizontalEndingPosition)
								{
								case MainWindow::SPRITE_HORIZONTAL_LEFT:
									spriteXOffset = ACTIVE_SPRITE_X_DISPLAY_BOOST;
									break;
								case MainWindow::SPRITE_HORIZONTAL_MIDDLE:
									spriteXOffset = (pixmap.width() / 2) - (spriteWidth / 2);
									break;
								case MainWindow::SPRITE_HORIZONTAL_RIGHT:
									spriteXOffset = pixmap.width() - spriteWidth - ACTIVE_SPRITE_X_DISPLAY_BOOST;
									break;
								default:
									break;
								}
								painter.drawPixmap(spriteXOffset,spriteYOffset,spriteWidth,DEFAULT_SPRITE_HEIGHT,sprite);
							}
						}
					}
					if(m_currentSceneType == GameScene::SCENE_CONVERSATION)
						m_conversationComplete = true;
					if(m_currentSceneType == GameScene::SCENE_SOUND)
						m_soundEffectMediaObject->stop();
					if(m_currentSceneType == GameScene::SCENE_MOVIE)
					{
#ifdef LINUX_BUILD
						m_movieMediaObject->stop();
#endif
#ifdef WINDOWS_BUILD
						m_movieMediaObject->pause();
#endif
						m_currentSceneType = GameScene::SCENE_ERROR;  // stop the movie callback from setting the next scene
					}
					m_conversationLabel->setText("");
					m_currentBackgroundPixmap.load(m_backgroundVector.at(backgroundOffset).c_str());
					m_currentBackgroundPixmap.scaled(IMAGE_MOVIE_MINIMUM_WIDTH,IMAGE_MOVIE_MINIMUM_HEIGHT);
					if(previousScene)
					{
						m_currentGameScenario.setCurrentScene(previousScene - 1);
						signalAdvanceScenario();
					}
					else  // this should never happen because we would have no pixmap but ...
						signalStartScenario(m_currentGameScenarioOffset);
					m_pixmapDisplayLabel->setPixmap(pixmap);
				}
			}
		}
	}
}

bool MainWindow::isPreviousAvailable(int &previousScene)
{
	bool ret = false;
	for(int i = m_currentGameSceneOffset - 1;i > 0;i--)
	{
		if(m_currentGameScenario.getSceneType(i) == GameScene::SCENE_CONVERSATION)
		{
			previousScene = i;
			ret = true;
			break;
		}
	}
	return ret;
}

bool MainWindow::getSpriteFileString(int index,string &fileName) const
{
	bool ret = true;
	if(index >= 0 && index <= (int) m_spriteVector.size())
		fileName = m_spriteVector.at(index);
	else
		ret = false;
	return ret;
}

#ifdef LINUX_BUILD
void MainWindow::soundEffectRunTime(qint64)
{
	if(m_soundEffectMediaObject->state() == Phonon::PlayingState)
	{
		if(m_soundEffectMediaObject->remainingTime() < SOUND_EFFECT_TICK_INTERVAL)
			m_soundEffectMediaObject->stop();
	}
}
#endif

#if defined(CONVERSATION_KEY_CLICK) && defined(LINUX_BUILD)
void MainWindow::conversationTypingRunTime(qint64)
{
	if(m_conversationTypingMediaObject->state() == Phonon::PlayingState)
	{
		if(m_conversationTypingMediaObject->remainingTime() < CONVERSATION_TYPING_TICK_INTERVAL)
			m_conversationTypingMediaObject->stop();
	}
}
#endif



