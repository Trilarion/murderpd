/*
 * saveloadverificationdialog.h
 *
 * Copyright 2012 - 2013 Chris Ohmstede
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 3
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 *
 */

#ifndef SAVELOADVERIFICATIONDIALOG_H
#define SAVELOADVERIFICATIONDIALOG_H

#include "builddefs.h"
#include <QtGui/QDialog>
#include <QtGui/QPixmap>
#include <QtGui/QVBoxLayout>
#include <QtGui/QLabel>
#include <QtGui/QPushButton>
#include <stdio.h>
#include <time.h>

class SaveLoadVerificationDialog : public QDialog
{
	Q_OBJECT
public:
	SaveLoadVerificationDialog(int mode,
							   bool overwrite,
							   time_t currentTime,
							   const QPixmap &pixmap,
							   QWidget *parent = 0,
							   Qt::WindowFlags f = 0);

	void init();
private:
	QVBoxLayout *m_mainVerticalLayout;
	QHBoxLayout *m_titleHorizontalLayout;
	QHBoxLayout *m_pixmapHorizontalLayout;
	QHBoxLayout *m_timeHorizontalLayout;
	QHBoxLayout *m_horizontalButtonLayout;

	QLabel *m_titleLabel;
	QLabel *m_pixmapLabel;
	QLabel *m_timeLabel;
	QPushButton *m_okButton;
	QPushButton *m_cancelButton;

	int m_mode;
	bool m_overwrite;
	time_t m_currentTime;
	QPixmap m_pixmap;
};

#endif // SAVELOADVERIFICATIONDIALOG_H
