/*
 * triviadialog.cpp
 *
 * Copyright 2012 - 2013 Chris Ohmstede
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 3
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 *
 */

#include "triviadialog.h"
#include "mainwindow.h"
#include <QtGui/QRadioButton>

TriviaDialog::TriviaDialog(const vector<string> &choicesVector,
						 const string &question,
						 int spriteOffset,
						 QWidget *parent /*= 0*/,
						 Qt::WindowFlags f /*= 0*/) :
							QDialog(parent,f),
							m_choicesVector(choicesVector),
							m_question(question),
							m_spriteOffset(spriteOffset)
{
	m_mainWindow = (MainWindow*) parent;
	setWindowTitle(TRIVIA_DIALOG_TITLE);
}

void TriviaDialog::init()
{
	m_mainVerticalLayout = new QVBoxLayout();

	m_topLine = new QFrame();
	m_topLine->setFrameStyle(QFrame::HLine | QFrame::Raised);
	m_topLine->setLineWidth(FRAME_LINE_WIDTH);

	m_questionHorizontalLayout = new QHBoxLayout();
	m_spriteLabel = new QLabel();
	m_questionLabel = new QLabel(m_question.c_str());
	m_questionLabel->setWordWrap(true);
	QFont font = m_questionLabel->font();
	font.setPointSize(CONVERSATION_FONT_SIZE);
	m_questionLabel->setFont(font);
	m_questionLabel->setMinimumHeight(MINIMUM_QUESTION_HEIGHT);
	m_questionLabel->setMargin(CONVERSATION_MARGIN);

	string fileName;
	if(m_mainWindow->getSpriteFileString(m_spriteOffset,fileName))
	{
		QPixmap sprite(fileName.c_str());
		sprite.scaledToHeight(DEFAULT_SPRITE_HEIGHT);
		m_spriteLabel->setPixmap(sprite);
	}
	m_questionHorizontalLayout->addWidget(m_spriteLabel);
	m_questionHorizontalLayout->addWidget(m_questionLabel);
	m_questionHorizontalLayout->setStretch(0,0);
	m_questionHorizontalLayout->setStretch(1,1);

	m_questionLine = new QFrame();
	m_questionLine->setFrameStyle(QFrame::HLine | QFrame::Raised);
	m_questionLine->setLineWidth(FRAME_LINE_WIDTH);

	m_radioVerticalLayout = new QVBoxLayout();
	m_radioButtonGroup = new QButtonGroup(this);
	for(int i = 0;i < (int) m_choicesVector.size();i++)
	{
		QRadioButton *rb = new QRadioButton(m_choicesVector.at(i).c_str());
		m_radioButtonGroup->addButton(rb,i);
		m_radioVerticalLayout->addWidget(rb);
	}
	m_radioButtonGroup->button(0)->setChecked(true);

	m_radioLine = new QFrame();
	m_radioLine->setFrameStyle(QFrame::HLine | QFrame::Raised);
	m_radioLine->setLineWidth(FRAME_LINE_WIDTH);

	m_buttonHorizontalLayout = new QHBoxLayout();
	m_okButton = new QPushButton("OK");

	m_buttonHorizontalLayout->addStretch();
	m_buttonHorizontalLayout->addWidget(m_okButton);
	m_buttonHorizontalLayout->addStretch();

	m_mainVerticalLayout->addWidget(m_topLine);
	m_mainVerticalLayout->addItem(m_questionHorizontalLayout);
	m_mainVerticalLayout->addWidget(m_questionLine);
	m_mainVerticalLayout->addStretch();
	m_mainVerticalLayout->addItem(m_radioVerticalLayout);
	m_mainVerticalLayout->addStretch();
	m_mainVerticalLayout->addWidget(m_radioLine);
	m_mainVerticalLayout->addItem(m_buttonHorizontalLayout);

	setLayout(m_mainVerticalLayout);

	setMinimumWidth(IMAGE_MOVIE_MINIMUM_WIDTH - 20);
	setMinimumHeight(IMAGE_MOVIE_MINIMUM_HEIGHT - 120);

	connect(m_okButton,SIGNAL(clicked()),this,SLOT(accept()));
	connect(this,SIGNAL(rejected()),this,SLOT(userChoiceDialogRejected()));
	connect(this,SIGNAL(accepted()),this,SLOT(userChoiceDialogAccepted()));
}

void TriviaDialog::userChoiceDialogRejected()
{
	setResult(-1);
}

void TriviaDialog::userChoiceDialogAccepted()
{
	setResult(m_radioButtonGroup->checkedId());
}
