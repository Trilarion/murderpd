/*
 * gamescene.cpp
 *
 * Copyright 2012 - 2013 Chris Ohmstede
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 3
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 *
 */

#include "gamescene.h"

// class SceneSpriteNode
SceneSpriteNode::SceneSpriteNode() :
		m_spriteOffset(0),
		m_verticalPosition(0),
		m_horizontalStartingPosition(0),
		m_horizontalEndingPosition(0),
		m_direction(0)
{
}

SceneSpriteNode::SceneSpriteNode(int spriteOffset,
		int verticalPosition,
		int horizontalStartingPosition,
		int horizontalEndingPosition,
		int direction) :
		m_spriteOffset(spriteOffset),
		m_verticalPosition(verticalPosition),
		m_horizontalStartingPosition(horizontalStartingPosition),
		m_horizontalEndingPosition(horizontalEndingPosition),
		m_direction(direction)
{
}

SceneSpriteNode::SceneSpriteNode(const SceneSpriteNode &ssn) :
		m_spriteOffset(ssn.m_spriteOffset),
		m_verticalPosition(ssn.m_verticalPosition),
		m_horizontalStartingPosition(ssn.m_horizontalStartingPosition),
		m_horizontalEndingPosition(ssn.m_horizontalEndingPosition),
		m_direction(ssn.m_direction)
{
}

SceneSpriteNode &SceneSpriteNode::operator = (const SceneSpriteNode &ssn)
{
	m_spriteOffset = ssn.m_spriteOffset;
	m_verticalPosition = ssn.m_verticalPosition;
	m_horizontalStartingPosition = ssn.m_horizontalStartingPosition;
	m_horizontalEndingPosition = ssn.m_horizontalEndingPosition;
	m_direction = ssn.m_direction;
	return *this;
}

int SceneSpriteNode::getSpriteOffset() const
{
	return m_spriteOffset;
}

int SceneSpriteNode::getVerticalPosition() const
{
	return m_verticalPosition;
}

int SceneSpriteNode::getHorizontalStartingPosition() const
{
	return m_horizontalStartingPosition;
}

int SceneSpriteNode::getHorizontalEndingPosition() const
{
	return m_horizontalEndingPosition;
}

int SceneSpriteNode::getDirection() const
{
	return m_direction;
}



// class SceneMovieSubtitleNode
SceneMovieSubtitleNode::SceneMovieSubtitleNode() : m_timeInSeconds(0)
{
}

SceneMovieSubtitleNode::SceneMovieSubtitleNode(int timeInSeconds,const string &subtitleText) : m_timeInSeconds(timeInSeconds),m_subtitleText(subtitleText)
{
}

SceneMovieSubtitleNode::SceneMovieSubtitleNode(const SceneMovieSubtitleNode &smsn) : m_timeInSeconds(smsn.m_timeInSeconds),m_subtitleText(smsn.m_subtitleText)
{
}

SceneMovieSubtitleNode &SceneMovieSubtitleNode::operator = (const SceneMovieSubtitleNode &smsn)
{
	m_timeInSeconds = smsn.m_timeInSeconds;
	m_subtitleText = smsn.m_subtitleText;
	return *this;
}

int SceneMovieSubtitleNode::getTimeInSeconds() const
{
	return m_timeInSeconds;
}

const string &SceneMovieSubtitleNode::getSubtitleText() const
{
	return m_subtitleText;
}




// class SceneMovieSubtitleNodeInUse
SceneMovieSubtitleNodeInUse::SceneMovieSubtitleNodeInUse() : SceneMovieSubtitleNode(),m_used(false)
{
}

SceneMovieSubtitleNodeInUse::SceneMovieSubtitleNodeInUse(const SceneMovieSubtitleNode &smsn) : SceneMovieSubtitleNode(smsn),m_used(false)
{
}

SceneMovieSubtitleNodeInUse::SceneMovieSubtitleNodeInUse(const SceneMovieSubtitleNodeInUse &smsniu) : SceneMovieSubtitleNode(smsniu),m_used(smsniu.m_used)
{
}

SceneMovieSubtitleNodeInUse &SceneMovieSubtitleNodeInUse::operator = (const SceneMovieSubtitleNode &smsn)
{
	SceneMovieSubtitleNode::operator = (smsn);
	m_used = false;
	return *this;
}

SceneMovieSubtitleNodeInUse &SceneMovieSubtitleNodeInUse::operator = (const SceneMovieSubtitleNodeInUse &smsniu)
{
	SceneMovieSubtitleNode::operator = (smsniu);
	m_used = smsniu.m_used;
	return *this;
}

bool SceneMovieSubtitleNodeInUse::getUsed() const
{
	return m_used;
}

void SceneMovieSubtitleNodeInUse::setUsed(bool used)
{
	m_used = used;
}




// class SceneUserChoiceNode
SceneUserChoiceNode::SceneUserChoiceNode() : m_scenarioOffset(-1)
{
}

SceneUserChoiceNode::SceneUserChoiceNode(int scenarioOffset,const string &userChoiceText) : m_scenarioOffset(scenarioOffset),m_userChoiceText(userChoiceText)
{
}

SceneUserChoiceNode::SceneUserChoiceNode(const SceneUserChoiceNode &sucn) : m_scenarioOffset(sucn.m_scenarioOffset),m_userChoiceText(sucn.m_userChoiceText)
{
}

SceneUserChoiceNode &SceneUserChoiceNode::operator = (const SceneUserChoiceNode &sucn)
{
	m_scenarioOffset = sucn.m_scenarioOffset;
	m_userChoiceText = sucn.m_userChoiceText;
	return *this;
}

int SceneUserChoiceNode::getScenarioOffset() const
{
	return m_scenarioOffset;
}

const string &SceneUserChoiceNode::getUserChoiceText() const
{
	return m_userChoiceText;
}



// class GameScene
GameScene::GameScene() :
		m_sceneType(-1),
		m_movieOffset(-1),
		m_backgroundOffset(-1),
		m_soundOffset(-1),
		m_scenarioOffset(-1),
		m_toBeContinued(false),
		m_triviaCorrectAnswer(-1),
		m_triviaErrorScenario(-1),
		m_triviaSpriteOffset(-1)
{
}

GameScene::GameScene(int sceneType) :
		m_sceneType(sceneType),
		m_movieOffset(-1),
		m_backgroundOffset(-1),
		m_soundOffset(-1),
		m_scenarioOffset(-1),
		m_toBeContinued(false),
		m_triviaCorrectAnswer(-1),
		m_triviaErrorScenario(-1),
		m_triviaSpriteOffset(-1)
{
}

GameScene::GameScene(int sceneType,const string &conversation) :
		m_sceneType(sceneType),
		m_conversation(conversation),
		m_movieOffset(-1),
		m_backgroundOffset(-1),
		m_soundOffset(-1),
		m_scenarioOffset(-1),
		m_toBeContinued(false),
		m_triviaCorrectAnswer(-1),
		m_triviaErrorScenario(-1),
		m_triviaSpriteOffset(-1)
{
}

GameScene::GameScene(const GameScene &gs) :
		m_sceneType(gs.m_sceneType),
		m_conversation(gs.m_conversation),
		m_movieOffset(gs.m_movieOffset),
		m_backgroundOffset(gs.m_backgroundOffset),
		m_soundOffset(gs.m_soundOffset),
		m_scenarioOffset(gs.m_scenarioOffset),
		m_toBeContinued(gs.m_toBeContinued),
		m_triviaCorrectAnswer(gs.m_triviaCorrectAnswer),
		m_triviaErrorScenario(gs.m_triviaErrorScenario),
		m_triviaSpriteOffset(gs.m_triviaSpriteOffset),
		m_triviaQuestion(gs.m_triviaQuestion)
{
	m_sceneSpriteNodeVector = gs.m_sceneSpriteNodeVector;
	m_sceneMovieSubtitleNodeVector = gs.m_sceneMovieSubtitleNodeVector;
	m_sceneUserChoiceNodeVector = gs.m_sceneUserChoiceNodeVector;
	m_trivia_choices = gs.m_trivia_choices;
}

GameScene &GameScene::operator = (const GameScene &gs)
{
	m_sceneType = gs.m_sceneType;
	m_conversation = gs.m_conversation;
	m_movieOffset = gs.m_movieOffset;
	m_backgroundOffset = gs.m_backgroundOffset;
	m_soundOffset = gs.m_soundOffset;
	m_scenarioOffset = gs.m_scenarioOffset;
	m_toBeContinued = gs.m_toBeContinued;
	m_triviaCorrectAnswer = gs.m_triviaCorrectAnswer;
	m_triviaErrorScenario = gs.m_triviaErrorScenario;
	m_triviaSpriteOffset = gs.m_triviaSpriteOffset;
	m_triviaQuestion = gs.m_triviaQuestion;
	m_sceneSpriteNodeVector = gs.m_sceneSpriteNodeVector;
	m_sceneMovieSubtitleNodeVector = gs.m_sceneMovieSubtitleNodeVector;
	m_sceneUserChoiceNodeVector = gs.m_sceneUserChoiceNodeVector;
	m_trivia_choices = gs.m_trivia_choices;
	return *this;
}

void GameScene::addSprite(int spriteOffset,
			   int verticalPosition,
			   int horizontalStartingPosition,
			   int horizontalEndingPosition,
			   int direction)
{
	SceneSpriteNode ssn(spriteOffset,verticalPosition,horizontalStartingPosition,horizontalEndingPosition,direction);
	m_sceneSpriteNodeVector.push_back(ssn);
}

void GameScene::addMovieSubtitle(int timeInSeconds,const string &subtitleText)
{
	SceneMovieSubtitleNode smsn(timeInSeconds,subtitleText);
	m_sceneMovieSubtitleNodeVector.push_back(smsn);
}

void GameScene::addUserChoice(int scenerioOffset,const string &userChoiceText)
{
	SceneUserChoiceNode sucn(scenerioOffset,userChoiceText);
	m_sceneUserChoiceNodeVector.push_back(sucn);
}

int GameScene::getSceneType() const
{
	return m_sceneType;
}

const string &GameScene::getConversation() const
{
	return m_conversation;
}

int GameScene::getMovieOffset() const
{
	return m_movieOffset;
}

int GameScene::getBackgroundOffset() const
{
	return m_backgroundOffset;
}

int GameScene::getSpriteCount() const
{
	return (int) m_sceneSpriteNodeVector.size();
}

bool GameScene::getSpriteInfo(int index,
				   int &spriteOffset,
				   int &verticalPosition,
				   int &horizontalStartingPosition,
				   int &horizontalEndingPosition,
				   int &direction) const
{
	bool ret = true;
	if(index >= 0 && index < (int) m_sceneSpriteNodeVector.size())
	{
		SceneSpriteNode ssn = m_sceneSpriteNodeVector.at(index);
		spriteOffset = ssn.getSpriteOffset();
		verticalPosition = ssn.getVerticalPosition();
		horizontalStartingPosition = ssn.getHorizontalStartingPosition();
		horizontalEndingPosition = ssn.getHorizontalEndingPosition();
		direction = ssn.getDirection();
	}
	else
		ret = false;
	return ret;
}

int GameScene::getMovieSubtitleCount() const
{
	return (int) m_sceneMovieSubtitleNodeVector.size();
}

bool GameScene::getMovieSubtitleInfo(int index,SceneMovieSubtitleNode &smsn) const
{
	bool ret = true;
	if(index >= 0 && index < (int) m_sceneMovieSubtitleNodeVector.size())
		smsn = m_sceneMovieSubtitleNodeVector.at(index);
	else
		ret = false;
	return ret;
}

int GameScene::getUserChoiceCount() const
{
	return (int) m_sceneUserChoiceNodeVector.size();
}

bool GameScene::getUserChoiceInfo(int index,int &scenerioOffset,string &userChoiceText) const
{
	bool ret = true;
	if(index >= 0 && index < (int) m_sceneUserChoiceNodeVector.size())
	{
		SceneUserChoiceNode sucn = m_sceneUserChoiceNodeVector.at(index);
		scenerioOffset = sucn.getScenarioOffset();
		userChoiceText = sucn.getUserChoiceText();
	}
	else
		ret = false;
	return ret;
}

void GameScene::setConversation(const char *conversation)
{
	m_conversation = conversation;
}

void GameScene::setMovieOffset(int movieOffset)
{
	m_movieOffset = movieOffset;
}

void GameScene::setBackgroundOffset(int backgroundOffset)
{
	m_backgroundOffset = backgroundOffset;
}

void GameScene::setSoundOffset(int soundOffset)
{
	m_soundOffset = soundOffset;
}

int GameScene::getSoundOffset() const
{
	return m_soundOffset;
}

int GameScene::getScenarioOffset() const
{
	return m_scenarioOffset;
}

void GameScene::setScenarioOffset(int scenarioOffset)
{
	m_scenarioOffset = scenarioOffset;
}

void GameScene::setToBeContinued(bool toBeContinued)
{
	m_toBeContinued = toBeContinued;
}

bool GameScene::getToBeContinued() const
{
	return m_toBeContinued;
}

void GameScene::setTriviaQuestion(const string &question)
{
	m_triviaQuestion = question;
}

void GameScene::addTriviaChoice(const string &choice)
{
	m_trivia_choices.push_back(choice);
}

void GameScene::setTriviaCorrectAnswer(int answer)
{
	m_triviaCorrectAnswer = answer;
}

void GameScene::setTriviaErrorScenario(int scenarioOffset)
{
	m_triviaErrorScenario = scenarioOffset;
}

void GameScene::setTriviaSprite(int spriteOffset)
{
	m_triviaSpriteOffset = spriteOffset;
}

int GameScene::getTriviaCorrectAnswer() const
{
	return m_triviaCorrectAnswer;
}

int GameScene::getTriviaChoiceCount() const
{
	return (int) m_trivia_choices.size();
}

int GameScene::getTriviaSpriteOffset() const
{
	return m_triviaSpriteOffset;
}

int GameScene::getTriviaErrorScenario() const
{
	return m_triviaErrorScenario;
}

const string &GameScene::getTriviaQuestion() const
{
	return m_triviaQuestion;
}

const vector<string> &GameScene::getTriviaChoices() const
{
	return m_trivia_choices;
}

