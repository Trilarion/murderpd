/*
 * saveloadverificationdialog.cpp
 *
 * Copyright 2012 - 2013 Chris Ohmstede
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 3
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 *
 */

#include "saveloadverificationdialog.h"
#include "mainwindow.h"

SaveLoadVerificationDialog::SaveLoadVerificationDialog(int mode,
							bool overwrite,
							time_t currentTime,
							const QPixmap &pixmap,
							QWidget *parent /*= 0*/,
							Qt::WindowFlags f /*= 0*/) :
							QDialog(parent,f),
							m_mode(mode),
							m_overwrite(overwrite),
							m_currentTime(currentTime),
							m_pixmap(pixmap)
{
	if(m_mode == MainWindow::MODE_LOAD)
		setWindowTitle("Load Game");
	else
		setWindowTitle("Save Game");
}

void SaveLoadVerificationDialog::init()
{
	m_mainVerticalLayout = new QVBoxLayout();
	m_titleHorizontalLayout = new QHBoxLayout();
	m_pixmapHorizontalLayout = new QHBoxLayout();
	m_timeHorizontalLayout = new QHBoxLayout();
	m_horizontalButtonLayout = new QHBoxLayout();
	m_horizontalButtonLayout->setSpacing(16);

	m_titleLabel = new QLabel;
	if(m_mode == MainWindow::MODE_LOAD)
		m_titleLabel->setText("Load this game?");
	else
	{
		if(m_overwrite)
			m_titleLabel->setText("Overwrite with this game?");
		else
			m_titleLabel->setText("Save this game?");
	}
	QFont font = m_titleLabel->font();
	font.setPointSize(12);
	m_titleLabel->setFont(font);

	m_titleHorizontalLayout->addStretch();
	m_titleHorizontalLayout->addWidget(m_titleLabel);
	m_titleHorizontalLayout->addStretch();

	m_pixmapLabel = new QLabel;
	m_pixmapLabel->setPixmap(m_pixmap);

	m_pixmapHorizontalLayout->addStretch();
	m_pixmapHorizontalLayout->addWidget(m_pixmapLabel);
	m_pixmapHorizontalLayout->addStretch();

	m_timeLabel =  new QLabel(ctime(&m_currentTime));

	m_timeHorizontalLayout->addStretch();
	m_timeHorizontalLayout->addWidget(m_timeLabel);
	m_timeHorizontalLayout->addStretch();

	m_okButton = new QPushButton("OK");
	m_cancelButton = new QPushButton("Cancel");

	m_horizontalButtonLayout->addStretch();
	m_horizontalButtonLayout->addWidget(m_okButton);
	m_horizontalButtonLayout->addStretch();
	m_horizontalButtonLayout->addWidget(m_cancelButton);
	m_horizontalButtonLayout->addStretch();

	m_mainVerticalLayout->addItem(m_titleHorizontalLayout);
	m_mainVerticalLayout->addStretch();
	m_mainVerticalLayout->addItem(m_pixmapHorizontalLayout);
	m_mainVerticalLayout->addItem(m_timeHorizontalLayout);
	m_mainVerticalLayout->addStretch();
	m_mainVerticalLayout->addItem(m_horizontalButtonLayout);

	setLayout(m_mainVerticalLayout);

	connect(m_okButton,SIGNAL(clicked()),this,SLOT(accept()));
	connect(m_cancelButton,SIGNAL(clicked()),this,SLOT(reject()));
}
