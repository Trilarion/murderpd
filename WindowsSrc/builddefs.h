/*
 * builddefs.h
 *
 * Copyright 2012 - 2013 Chris Ohmstede
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 3
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 *
 */

#ifndef BUILDDEFS_H
#define BUILDDEFS_H

// build for Linux
//#define LINUX_BUILD

// build for Windows
#define WINDOWS_BUILD

// Set this define if you want a key click sound at the beginning of conversations
#define CONVERSATION_KEY_CLICK


#ifdef LINUX_BUILD
#undef WINDOWS_BUILD
#else
#define WINDOWS_BUILD
#endif

#endif // BUILDDEFS_H
