/*
 * gamescenario.cpp
 *
 * Copyright 2012 - 2013 Chris Ohmstede
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 3
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 *
 */

#include "gamescenario.h"

GameScenario::GameScenario() : m_currentScene(0)
{
}

GameScenario::GameScenario(const GameScenario &gs) : m_currentScene(gs.m_currentScene)
{
	m_gameSceneVector = gs.m_gameSceneVector;
}

GameScenario &GameScenario::operator = (const GameScenario &gs)
{
	m_currentScene = gs.m_currentScene;
	m_gameSceneVector = gs.m_gameSceneVector;
	return *this;
}

void GameScenario::addGameScene(const GameScene &gs)
{
	m_gameSceneVector.push_back(gs);
}

int GameScenario::firstScene(int &currentScene)
{
	int sceneType = GameScene::SCENE_ERROR;
	m_currentScene = currentScene = 0;
	if(m_gameSceneVector.size())
		sceneType = m_gameSceneVector.at(m_currentScene).getSceneType();
	return sceneType;
}

int GameScenario::nextScene(int &currentScene)
{
	int sceneType = GameScene::SCENE_ERROR;
	++m_currentScene;
	currentScene = m_currentScene;
	if(m_currentScene < (int) m_gameSceneVector.size())
		sceneType = m_gameSceneVector.at(m_currentScene).getSceneType();
	return sceneType;
}

int GameScenario::previousScene(int &currentScene)
{
	int sceneType = GameScene::SCENE_ERROR;
	--m_currentScene;
	currentScene = m_currentScene;
	if(m_currentScene >= 0 && m_currentScene < (int) m_gameSceneVector.size())
		sceneType = m_gameSceneVector.at(m_currentScene).getSceneType();
	return sceneType;
}

const GameScene &GameScenario::getGameScene() const
{
	return m_gameSceneVector.at(m_currentScene);
}

void GameScenario::setCurrentScene(int scene)
{
	m_currentScene = scene;
}

bool GameScenario::getSaveNodeInfo(int scene,int &backgroundOffset,GameScene &spriteGameScene)
{
	bool ret = false;
	backgroundOffset = -1;
	bool spriteFound = false;
	for(int i = scene;i >= 0;i--)
	{
		if(m_gameSceneVector.at(i).getSceneType() == GameScene::SCENE_SPRITE)
		{
			if(!spriteFound)
			{
				spriteGameScene = m_gameSceneVector.at(i);
				spriteFound = true;
			}
			backgroundOffset = m_gameSceneVector.at(i).getBackgroundOffset();
			if(backgroundOffset >= 0)
				break;
		}
	}
	if(spriteFound && backgroundOffset >= 0)
		ret = true;
	return ret;
}

int GameScenario::getLastSceneOffset()
{
	return (int) m_gameSceneVector.size() - 1;
}

int GameScenario::getSceneType(int offset)
{
	int ret = GameScene::SCENE_ERROR;
	if(offset >= 0 && offset <= (int) m_gameSceneVector.size())
		ret = m_gameSceneVector.at(offset).getSceneType();
	return ret;
}
