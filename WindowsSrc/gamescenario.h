/*
 * gamescenario.h
 *
 * Copyright 2012 - 2013 Chris Ohmstede
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 3
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 *
 */

#ifndef GAMESCENARIO_H
#define GAMESCENARIO_H

#include "builddefs.h"
#include "gamescene.h"
#include <vector>

using namespace std;

class GameScenario
{
public:
    GameScenario();
	GameScenario(const GameScenario &gs);
	GameScenario &operator = (const GameScenario &gs);

	void addGameScene(const GameScene &gs);

	int firstScene(int &currentScene);
	int nextScene(int &currentScene);
	int previousScene(int &currentScene);

	const GameScene &getGameScene() const;
	void setCurrentScene(int scene);

	bool getSaveNodeInfo(int scene,int &backgroundOffset,GameScene &spriteGameScene);

	int getLastSceneOffset();

	int getSceneType(int offset);
private:
	int m_currentScene;
	vector<GameScene> m_gameSceneVector;
};

#endif // GAMESCENARIO_H
