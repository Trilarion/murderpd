/*
 * gamescene.h
 *
 * Copyright 2012 - 2013 Chris Ohmstede
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 3
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 *
 */

#ifndef GAMESCENE_H
#define GAMESCENE_H

#include "builddefs.h"
#include <string>
#include <vector>

using namespace std;

class SceneSpriteNode
{
public:
	SceneSpriteNode();
	SceneSpriteNode(int spriteOffset,
					int verticalPosition,
					int horizontalStartingPosition,
					int horizontalEndingPosition,
					int direction);
	SceneSpriteNode(const SceneSpriteNode &ssn);
	SceneSpriteNode &operator = (const SceneSpriteNode &ssn);

	int getSpriteOffset() const;
	int getVerticalPosition() const;
	int getHorizontalStartingPosition() const;
	int getHorizontalEndingPosition() const;
	int getDirection() const;
private:
	int m_spriteOffset;
	int m_verticalPosition;
	int m_horizontalStartingPosition;
	int m_horizontalEndingPosition;
	int m_direction;
};

class SceneMovieSubtitleNode
{
public:
	SceneMovieSubtitleNode();
	SceneMovieSubtitleNode(int timeInSeconds,const string &subtitleText);
	SceneMovieSubtitleNode(const SceneMovieSubtitleNode &smsn);
	SceneMovieSubtitleNode &operator = (const SceneMovieSubtitleNode &smsn);

	int getTimeInSeconds() const;
	const string &getSubtitleText() const;
private:
	int m_timeInSeconds;
	string m_subtitleText;
};

class SceneUserChoiceNode
{
public:
	SceneUserChoiceNode();
	SceneUserChoiceNode(int scenerioOffset,const string &userChoiceText);
	SceneUserChoiceNode(const SceneUserChoiceNode &sucn);
	SceneUserChoiceNode &operator = (const SceneUserChoiceNode &sucn);

	int getScenarioOffset() const;
	const string &getUserChoiceText() const;
private:
	int m_scenarioOffset;
	string m_userChoiceText;
};

class SceneMovieSubtitleNodeInUse : public SceneMovieSubtitleNode
{
public:
	SceneMovieSubtitleNodeInUse();
	SceneMovieSubtitleNodeInUse(const SceneMovieSubtitleNode &smsn);
	SceneMovieSubtitleNodeInUse(const SceneMovieSubtitleNodeInUse &smsniu);
	SceneMovieSubtitleNodeInUse &operator = (const SceneMovieSubtitleNode &smsn);
	SceneMovieSubtitleNodeInUse &operator = (const SceneMovieSubtitleNodeInUse &smsniu);

	bool getUsed() const;
	void setUsed(bool used);
private:
	bool m_used;
};

class GameScene
{
public:
	enum
	{
		SCENE_CONVERSATION,
		SCENE_MOVIE,
		SCENE_SPRITE,
		SCENE_USER_CHOICE,
		SCENE_SOUND,
		SCENE_START_SCENARIO,
		SCENE_GAME_OVER,
		SCENE_TRIVIA,
		SCENE_TRIVIA_RETURN,
		SCENE_ERROR
	};

    GameScene();
	GameScene(int sceneType);
	GameScene(int sceneType,const string &conversation);
	GameScene(const GameScene &gs);
	GameScene &operator = (const GameScene &gs);

	void setConversation(const char *conversation);
	void setMovieOffset(int movieOffset);
	void setBackgroundOffset(int backgroundOffset);
	void setSoundOffset(int soundOffset);
	void setScenarioOffset(int scenarioOffset);
	void setToBeContinued(bool toBeContinued);
	void addSprite(int spriteOffset,
				   int verticalPosition,
				   int horizontalStartingPosition,
				   int horizontalEndingPosition,
				   int direction);
	void addMovieSubtitle(int timeInSeconds,const string &subtitleText);
	void addUserChoice(int scenerioOffset,const string &userChoiceText);

	int getSceneType() const;
	const string &getConversation() const;
	int getMovieOffset() const;
	int getBackgroundOffset() const;
	int getSoundOffset() const;
	int getScenarioOffset() const;
	bool getToBeContinued() const;
	int getSpriteCount() const;
	bool getSpriteInfo(int index,
					   int &spriteOffset,
					   int &verticalPosition,
					   int &horizontalStartingPosition,
					   int &horizontalEndingPosition,
					   int &direction) const;
	int getMovieSubtitleCount() const;
	bool getMovieSubtitleInfo(int index,SceneMovieSubtitleNode &smsn) const;
	int getUserChoiceCount() const;
	bool getUserChoiceInfo(int index,int &scenerioOffset,string &userChoiceText) const;
	void setTriviaQuestion(const string &question);
	void addTriviaChoice(const string &choice);
	void setTriviaCorrectAnswer(int answer);
	void setTriviaErrorScenario(int scenarioOffset);
	void setTriviaSprite(int spriteOffset);
	int getTriviaCorrectAnswer() const;
	int getTriviaChoiceCount() const;
	int getTriviaSpriteOffset() const;
	int getTriviaErrorScenario() const;
	const string &getTriviaQuestion() const;
	const vector<string> &getTriviaChoices() const;
private:
	int m_sceneType;

	string m_conversation;
	int m_movieOffset;
	int m_backgroundOffset;
	int m_soundOffset;
	int m_scenarioOffset;
	bool m_toBeContinued;
	int m_triviaCorrectAnswer;
	int m_triviaErrorScenario;
	int m_triviaSpriteOffset;
	string m_triviaQuestion;
	vector<SceneSpriteNode> m_sceneSpriteNodeVector;
	vector<SceneMovieSubtitleNode> m_sceneMovieSubtitleNodeVector;
	vector<SceneUserChoiceNode> m_sceneUserChoiceNodeVector;
	vector<string> m_trivia_choices;
};

#endif // GAMESCENE_H
