/*
 * userchoicedialog.h
 *
 * Copyright 2012 - 2013 Chris Ohmstede
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 3
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 *
 */

#ifndef USERCHOICEDIALOG_H
#define USERCHOICEDIALOG_H

#include "builddefs.h"
#include <QtGui/QDialog>
#include <QtGui/QVBoxLayout>
#include <QtGui/QListWidget>
#include <QtGui/QPushButton>
#include <vector>
#include <string>

using namespace std;

#define USER_CHOICE_DIALOG_TITLE			"What Will I Do?"

class UserChoiceDialog : public QDialog
{
	Q_OBJECT
public:
	UserChoiceDialog(const vector<string> &choicesVector,QWidget *parent = 0,Qt::WindowFlags f = 0);

	void init();
private:
	QVBoxLayout *m_mainVerticalLayout;
	QHBoxLayout *m_buttonHorizontalLayout;

	QListWidget *m_choiceList;
	QPushButton *m_okButton;
	QPushButton *m_cancelButton;

	vector<string> m_choicesVector;
private slots:
	void userChoiceDialogRejected();
	void userChoiceDialogAccepted();
	void listDoubleClicked(QListWidgetItem*);
};

#endif // USERCHOICEDIALOG_H
