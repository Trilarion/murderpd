/*
 * activesprite.cpp
 *
 * Copyright 2012 - 2013 Chris Ohmstede
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 3
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 *
 */

#include "activesprite.h"
#include "mainwindow.h"

ActiveSprite::ActiveSprite() :
				m_spriteVertical(-1),
				m_spriteHorizontal(-1),
				m_spriteDirection(-1),
				m_currentOffset(0),
				m_finalOffset(0),
				m_spriteWidth(0),
				m_completePending(false),
				m_backgroundWidth(0),
				m_randVertical(-1),
				m_randHorizontal(-1),
				m_currentVerticalOffset(-1),
				m_slideToRandComplete(false),
				m_randVerticalDirection(-1),
				m_randHorizontalDirection(-1),
				m_finalRandOffset(0),
				m_verticalSlideToRandComplete(false),
				m_horizontalSlideToRandComplete(false)
{
}

ActiveSprite::ActiveSprite(const QPixmap &sprite,
						int spriteVertical,
						int spriteStartingHorizontal,
						int spriteEndingHorizontal,
						int spriteDirection,
						int backgroundWidth) :
						m_sprite(sprite),
						m_spriteVertical(spriteVertical),
						m_spriteHorizontal(spriteEndingHorizontal),
						m_spriteDirection(spriteDirection),
						m_completePending(false),
						m_backgroundWidth(backgroundWidth),
						m_slideToRandComplete(false),
						m_verticalSlideToRandComplete(false),
						m_horizontalSlideToRandComplete(false)
{
	m_spriteWidth = (m_sprite.width() * DEFAULT_SPRITE_HEIGHT) / m_sprite.height();
	switch(m_spriteHorizontal)
	{
	case MainWindow::SPRITE_HORIZONTAL_LEFT:
		m_finalOffset = ACTIVE_SPRITE_X_DISPLAY_BOOST;
		break;
	case MainWindow::SPRITE_HORIZONTAL_MIDDLE:
		m_finalOffset = (backgroundWidth / 2) - (m_spriteWidth / 2);
		break;
	case MainWindow::SPRITE_HORIZONTAL_RIGHT:
		m_finalOffset = backgroundWidth - m_spriteWidth - ACTIVE_SPRITE_X_DISPLAY_BOOST;
		break;
	default:
		m_finalOffset = 0;
		break;
	}
	switch(spriteStartingHorizontal)
	{
	case MainWindow::SPRITE_HORIZONTAL_LEFT:
		m_currentOffset = ACTIVE_SPRITE_X_DISPLAY_BOOST;
		break;
	case MainWindow::SPRITE_HORIZONTAL_MIDDLE:
		m_currentOffset = (backgroundWidth / 2) - (m_spriteWidth / 2);
		break;
	case MainWindow::SPRITE_HORIZONTAL_RIGHT:
		m_currentOffset = backgroundWidth - m_spriteWidth - ACTIVE_SPRITE_X_DISPLAY_BOOST;
		break;
	default:  // off screen
		if(m_spriteDirection == MainWindow::SPRITE_DIRECTION_FROM_LEFT)
			m_currentOffset = 0;
		else
			m_currentOffset = backgroundWidth - m_spriteWidth - 1;
		break;
	}
	// reset the direction for start finish both on screen
	if(m_finalOffset != 0)  // off screen, maintain the direction
	{
		if(m_currentOffset <= m_finalOffset)
			m_spriteDirection = MainWindow::SPRITE_DIRECTION_FROM_LEFT;
		else
			m_spriteDirection = MainWindow::SPRITE_DIRECTION_FROM_RIGHT;
	}
	m_randVertical = rand() % 3;
	m_randHorizontal = (rand() % 3) + 1;
	switch(spriteVertical)
	{
	case MainWindow::SPRITE_VERTICAL_TOP:
		m_currentVerticalOffset = DEFAULT_TOP_SPRITE;
		break;
	case MainWindow::SPRITE_VERTICAL_MIDDLE:
		m_currentVerticalOffset = DEFAULT_MIDDLE_SPRITE;
		break;
	case MainWindow::SPRITE_VERTICAL_BOTTOM:
		m_currentVerticalOffset = DEFAULT_BOTTOM_SPRITE;
		break;
	default:
		m_currentVerticalOffset = 0;
		break;
	}
	if(m_randVertical <= spriteVertical)
		m_randVerticalDirection = ACTIVE_SPRITE_UP;
	else
		m_randVerticalDirection = ACTIVE_SPRITE_DOWN;
	if(spriteStartingHorizontal == MainWindow::SPRITE_HORIZONTAL_OFF_SCREEN)
	{
		if(spriteDirection == MainWindow::SPRITE_DIRECTION_FROM_LEFT)
			m_randHorizontalDirection = ACTIVE_SPRITE_LEFT;
		else
			m_randHorizontalDirection = ACTIVE_SPRITE_RIGHT;
	}
	else
	{
		if(m_randHorizontal <= spriteStartingHorizontal)
			m_randHorizontalDirection = ACTIVE_SPRITE_LEFT;
		else
			m_randHorizontalDirection = ACTIVE_SPRITE_RIGHT;
	}
	switch(m_randHorizontal)
	{
	case MainWindow::SPRITE_HORIZONTAL_LEFT:
		m_finalRandOffset = ACTIVE_SPRITE_X_DISPLAY_BOOST;
		break;
	case MainWindow::SPRITE_HORIZONTAL_MIDDLE:
		m_finalRandOffset = (backgroundWidth / 2) - (m_spriteWidth / 2);
		break;
	case MainWindow::SPRITE_HORIZONTAL_RIGHT:
		m_finalRandOffset = backgroundWidth - m_spriteWidth - ACTIVE_SPRITE_X_DISPLAY_BOOST;
		break;
	default:
		m_finalRandOffset = 0;
		break;
	}
}

ActiveSprite::ActiveSprite(const ActiveSprite &as) :
						m_sprite(as.m_sprite),
						m_spriteVertical(as.m_spriteVertical),
						m_spriteHorizontal(as.m_spriteHorizontal),
						m_spriteDirection(as.m_spriteDirection),
						m_currentOffset(as.m_currentOffset),
						m_finalOffset(as.m_finalOffset),
						m_spriteWidth(as.m_spriteWidth),
						m_completePending(as.m_completePending),
						m_backgroundWidth(as.m_backgroundWidth),
						m_randVertical(as.m_randVertical),
						m_randHorizontal(as.m_randHorizontal),
						m_currentVerticalOffset(as.m_currentVerticalOffset),
						m_slideToRandComplete(as.m_slideToRandComplete),
						m_randVerticalDirection(as.m_randVerticalDirection),
						m_randHorizontalDirection(as.m_randHorizontalDirection),
						m_finalRandOffset(as.m_finalRandOffset),
						m_verticalSlideToRandComplete(as.m_verticalSlideToRandComplete),
						m_horizontalSlideToRandComplete(as.m_horizontalSlideToRandComplete)
{
}

ActiveSprite &ActiveSprite::operator = (const ActiveSprite &as)
{
	m_sprite = as.m_sprite;
	m_spriteVertical = as.m_spriteVertical;
	m_spriteHorizontal = as.m_spriteHorizontal;
	m_spriteDirection = as.m_spriteDirection;
	m_currentOffset = as.m_currentOffset;
	m_finalOffset = as.m_finalOffset;
	m_spriteWidth = as.m_spriteWidth;
	m_completePending = as.m_completePending;
	m_backgroundWidth = as.m_backgroundWidth;
	m_randVertical = as.m_randVertical;
	m_randHorizontal = as.m_randHorizontal;
	m_currentVerticalOffset = as.m_currentVerticalOffset;
	m_slideToRandComplete = as.m_slideToRandComplete;
	m_randVerticalDirection = as.m_randVerticalDirection;
	m_randHorizontalDirection = as.m_randHorizontalDirection;
	m_finalRandOffset = as.m_finalRandOffset;
	m_verticalSlideToRandComplete = as.m_verticalSlideToRandComplete;
	m_horizontalSlideToRandComplete = as.m_horizontalSlideToRandComplete;
	return *this;
}

bool ActiveSprite::slideSpriteInPainter(QPainter &painter)
{
	bool complete = false;
	if(m_currentOffset == m_finalOffset)  // the sprite isn't moving, leave it alone
		m_slideToRandComplete = true;
	if(!m_slideToRandComplete)
	{
		if(m_randVerticalDirection == ACTIVE_SPRITE_UP)
		{
			m_currentVerticalOffset -= SPRITE_SLIDE_Y_STEP;
			switch(m_randVertical)
			{
			case MainWindow::SPRITE_VERTICAL_BOTTOM:
				if(m_currentVerticalOffset <= DEFAULT_BOTTOM_SPRITE)
				{
					m_currentVerticalOffset = DEFAULT_BOTTOM_SPRITE;
					m_verticalSlideToRandComplete = true;
				}
				break;
			case MainWindow::SPRITE_VERTICAL_MIDDLE:
				if(m_currentVerticalOffset <= DEFAULT_MIDDLE_SPRITE)
				{
					m_currentVerticalOffset = DEFAULT_MIDDLE_SPRITE;
					m_verticalSlideToRandComplete = true;
				}
				break;
			case MainWindow::SPRITE_VERTICAL_TOP:
				if(m_currentVerticalOffset <= DEFAULT_TOP_SPRITE)
				{
					m_currentVerticalOffset = DEFAULT_TOP_SPRITE;
					m_verticalSlideToRandComplete =- true;
				}
				break;
			default:
				break;
			}
		}
		else
		{
			m_currentVerticalOffset += SPRITE_SLIDE_Y_STEP;
			switch(m_randVertical)
			{
			case MainWindow::SPRITE_VERTICAL_BOTTOM:
				if(m_currentVerticalOffset >= DEFAULT_BOTTOM_SPRITE)
				{
					m_currentVerticalOffset = DEFAULT_BOTTOM_SPRITE;
					m_verticalSlideToRandComplete = true;
				}
				break;
			case MainWindow::SPRITE_VERTICAL_MIDDLE:
				if(m_currentVerticalOffset >= DEFAULT_MIDDLE_SPRITE)
				{
					m_currentVerticalOffset = DEFAULT_MIDDLE_SPRITE;
					m_verticalSlideToRandComplete = true;
				}
				break;
			case MainWindow::SPRITE_VERTICAL_TOP:
				if(m_currentVerticalOffset >= DEFAULT_TOP_SPRITE)
				{
					m_currentVerticalOffset = DEFAULT_TOP_SPRITE;
					m_verticalSlideToRandComplete = true;
				}
				break;
			default:
				break;
			}
		}
		if(m_randHorizontalDirection == ACTIVE_SPRITE_LEFT)
		{
			m_currentOffset += SPRITE_SLIDE_X_STEP;
			if(m_currentOffset >= m_finalRandOffset)
			{
				m_currentOffset = m_finalRandOffset;
				m_horizontalSlideToRandComplete = true;
			}
		}
		else
		{
			m_currentOffset -= SPRITE_SLIDE_X_STEP;
			if(m_currentOffset <= m_finalRandOffset)
			{
				m_currentOffset = m_finalRandOffset;
				m_horizontalSlideToRandComplete = true;
			}
		}
		if(m_verticalSlideToRandComplete && m_horizontalSlideToRandComplete)
		{
			m_slideToRandComplete = true;
			m_verticalSlideToRandComplete = m_horizontalSlideToRandComplete = false;  // reset for the next slide
		}
		painter.drawPixmap(m_currentOffset,m_currentVerticalOffset,m_spriteWidth,DEFAULT_SPRITE_HEIGHT,m_sprite);
	}
	else
	{
		complete = m_completePending; // last iteration set complete pending, keep painting the final offset until we are no longer called
		int spriteYOffset = 0;
		switch(m_spriteVertical)
		{
		case MainWindow::SPRITE_VERTICAL_BOTTOM:
			spriteYOffset = DEFAULT_BOTTOM_SPRITE;
			break;
		case MainWindow::SPRITE_VERTICAL_MIDDLE:
			spriteYOffset = DEFAULT_MIDDLE_SPRITE;
			break;
		case MainWindow::SPRITE_VERTICAL_TOP:
			spriteYOffset = DEFAULT_TOP_SPRITE;
			break;
		default:
			break;
		}

		if(m_currentOffset == m_finalOffset)
			m_currentVerticalOffset = spriteYOffset;  // the sprite isn't moving, leave it alone

		if(m_finalOffset != -1)  // sprite is still on screen
		{
			painter.drawPixmap(m_currentOffset,m_currentVerticalOffset,m_spriteWidth,DEFAULT_SPRITE_HEIGHT,m_sprite);
			if(m_currentVerticalOffset <= spriteYOffset)
			{
				m_currentVerticalOffset += SPRITE_SLIDE_Y_STEP;
				if(m_currentVerticalOffset >= spriteYOffset)
				{
					m_currentVerticalOffset = spriteYOffset;
					m_verticalSlideToRandComplete = true;
				}
			}
			else
			{
				m_currentVerticalOffset -= SPRITE_SLIDE_Y_STEP;
				if(m_currentVerticalOffset <= spriteYOffset)
				{
					m_currentVerticalOffset = spriteYOffset;
					m_verticalSlideToRandComplete = true;
				}
			}
			if(m_finalOffset == 0)  // sprite is moving off the screen, move in the authored direction
			{
				if(m_spriteDirection == MainWindow::SPRITE_DIRECTION_FROM_LEFT)
				{
					m_currentOffset += SPRITE_SLIDE_X_STEP;
					if(m_finalOffset > 0)
					{
						if(m_currentOffset >= m_finalOffset)
						{
							m_currentOffset = m_finalOffset;
							m_horizontalSlideToRandComplete = true;
						}
					}
					else
					{
						if(m_currentOffset >= m_backgroundWidth - m_spriteWidth - 1)
							m_finalOffset = -1;
					}
				}
				else
				{
					m_currentOffset -= SPRITE_SLIDE_X_STEP;
					if(m_finalOffset > 0)
					{
						if(m_currentOffset <= m_finalOffset)
						{
							m_currentOffset = m_finalOffset;
							m_horizontalSlideToRandComplete = true;
						}
					}
					else
					{
						if(m_currentOffset <= 0)
							m_finalOffset = -1;
					}
				}
			}
			else  // the sprite stays on the screen, move from the curent horizontal offset
			{
				if(m_currentOffset <= m_finalOffset)
				{
					m_currentOffset += SPRITE_SLIDE_X_STEP;
					if(m_currentOffset >= m_finalOffset)
					{
						m_currentOffset = m_finalOffset;
						m_horizontalSlideToRandComplete = true;
					}
				}
				else
				{
					m_currentOffset -= SPRITE_SLIDE_X_STEP;
					if(m_currentOffset <= m_finalOffset)
					{
						m_currentOffset = m_finalOffset;
						m_horizontalSlideToRandComplete = true;
					}
				}
			}
			if(m_verticalSlideToRandComplete && m_horizontalSlideToRandComplete)
				m_completePending = true;
		}
		else // m_finalOffset == -1, off screen, let the painter go through with no sprite
			m_completePending = true;
	}
	return complete;
}
