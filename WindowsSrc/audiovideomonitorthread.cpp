/*
 * audiovideomonitorthread.cpp
 *
 * Copyright 2012 - 2013 Chris Ohmstede
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 3
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 *
 */

#include "audiovideomonitorthread.h"
#include "mainwindow.h"

AudioVideoMonitorThread::AudioVideoMonitorThread() : QThread(),m_monitoring(false)
{
}

void AudioVideoMonitorThread::setParentAndType(MainWindow *parent,int audioVideoType)
{
	m_parent = parent;
	m_audioVideoType = audioVideoType;
}

void AudioVideoMonitorThread::run()
{
	bool done = false;
	while(!done)
	{
		if(m_parent->isThreadQuit())
		{
			done = true;
			continue;
		}
		switch(m_audioVideoType)  // if we establish play, no need to monitor anymore, let the thread end
		{
#ifdef CONVERSATION_KEY_CLICK
		case AUDIO_VIDEO_CONVERSATION:
			done = m_parent->getConversationPlayEstablished();
			break;
#endif
		case AUDIO_VIDEO_SOUND:
			done = m_parent->getSoundPlayEstablished();
			break;
		case AUDIO_VIDEO_MOVIE:
			done = m_parent->getMoviePlayEstablished();
			break;
		default:  // should never happen, but if it does, no reason to keep the thread alive
			done = true;
			break;
		}
		if(!done)
		{
			bool sceneTypeEstablished = false;
			int sceneType = m_parent->getCurrentSceneType();
			switch(m_audioVideoType)
			{
			case AUDIO_VIDEO_CONVERSATION:
				if(sceneType == GameScene::SCENE_CONVERSATION)
					sceneTypeEstablished = true;
				break;
			case AUDIO_VIDEO_SOUND:
				if(sceneType == GameScene::SCENE_SOUND)
					sceneTypeEstablished = true;
				break;
			case AUDIO_VIDEO_MOVIE:
				if(sceneType == GameScene::SCENE_MOVIE)
					sceneTypeEstablished = true;
				break;
			default:
				break;
			}
			if(sceneTypeEstablished)
			{
				if(!m_monitoring)
				{
					m_monitoring = true;
					m_startTime = time(NULL);
				}
				else
				{
					time_t endTime = time(NULL);
					if(difftime(endTime,m_startTime) >= ESTABLISH_PLAY_TIMEOUT)  // we should have established play by now, move to the next scene, the scene change will throw the warn
						signalAdvanceScenario();
				}
			}
			else
			{
				if(m_monitoring)
				{
					m_monitoring = false;
					bool playEstablished = false;
					switch(m_audioVideoType)
					{
#ifdef CONVERSATION_KEY_CLICK
					case AUDIO_VIDEO_CONVERSATION:
						playEstablished = m_parent->getConversationPlayEstablished();
						break;
#endif
					case AUDIO_VIDEO_SOUND:
						playEstablished = m_parent->getSoundPlayEstablished();
						break;
					case AUDIO_VIDEO_MOVIE:
						playEstablished = m_parent->getMoviePlayEstablished();
						break;
					default:  // should never happen, but if it does, no reason to keep the thread alive
						break;
					}
					if(playEstablished)
						done = true;
					else
					{
						if(!m_parent->isAudioVideoWarnDisplayed())
							signalAudioVideoWarn();
					}
				}
			}
			if(!done)
				msleep(200);
		}
	}
}

